package com.digi.soc;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Adapters.BottomAdapter;
import com.digi.soc.Fragments.HomeFragment;
import com.digi.soc.Helpers.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.rvBottomMain)
     RecyclerView rvBottomMain;

    public static RecyclerView rvBottom;
    public static final int RUNTIME_PERMISSION_CODE = 7;
    private static final int REQUEST_PHONE_CALL = 1;

    public static MainActivity instance = null;

    public MainActivity() {
        instance = this;
    }

    public static synchronized MainActivity getInstance() {
        if (instance == null) {
            instance = new MainActivity();
        }
        return MainActivity.instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        AndroidRuntimePermission();

        rvBottomMain.setHasFixedSize(true);
        rvBottomMain.setLayoutManager(new GridLayoutManager(this, 5));
        rvBottomMain.setNestedScrollingEnabled(false);

        rvBottom = rvBottomMain;

        changeFragment(this, new HomeFragment(), false);
    }

    public static void changeFragment(Context context, Fragment fragment, boolean doAdd) {
        FragmentManager manager = ((MainActivity) context).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.containerFrame, fragment);
        if (doAdd)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    public static void changeAppartmentFragment(Context context, Fragment fragment, boolean doAdd,Bundle bundle) {
        FragmentManager manager = ((MainActivity) context).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.containerFrame, fragment);
        fragment.setArguments(bundle);
        if (doAdd)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();

    }
    public static void setBottomPos(Context context, int selPos) {
        if (selPos==-1)
            rvBottom.setVisibility(View.GONE);
        else
            rvBottom.setVisibility(View.VISIBLE);
        rvBottom.setAdapter(new BottomAdapter(context, selPos));
    }
    // Creating Runtime permission function.
    public void AndroidRuntimePermission() {

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    android.app.AlertDialog.Builder alert_builder = new android.app.AlertDialog.Builder(MainActivity.this);
                    alert_builder.setMessage("External Storage Permission is Required.");
                    alert_builder.setTitle("Please Grant Permission.");
                    alert_builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            ActivityCompat.requestPermissions(
                                    MainActivity.this,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    RUNTIME_PERMISSION_CODE

                            );
                        }
                    });

                    alert_builder.setNeutralButton("Cancel", null);

                    android.app.AlertDialog dialog = alert_builder.create();

                    dialog.show();

                } else {

                    ActivityCompat.requestPermissions(
                            MainActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            RUNTIME_PERMISSION_CODE
                    );
                }
            } else {

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case RUNTIME_PERMISSION_CODE: {

                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Utils.showToast( this,"Please grant permission to prvoide us better services");
                }
            }
        }
    }

}
