package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.Models.ComplainModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;
import com.thekhaeng.pushdownanim.PushDownAnim;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ComplainAdapter extends RecyclerView.Adapter<ComplainAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ComplainModel> complainList;
    private Animation animFadein, animFadeOut;

    public ComplainAdapter(Context context, ArrayList<ComplainModel> complainList) {
        this.context = context;
        this.complainList = complainList;
        animFadein = AnimationUtils.loadAnimation(context, R.anim.fade_in2);
        animFadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out2);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.complain_item, parent, false);
        return new ComplainAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ComplainModel complainModel = complainList.get(position);
        holder.tvPost.setText(complainModel.getDescription());
        holder.tvPostTitle.setText(complainModel.getTitle());
        holder.tvName.setText(complainModel.getCreated_by().getFirstName() + " " + complainModel.getCreated_by().getLastName());
        holder.tvTimeAgo.setText(Utility.agoTimeShow(complainModel.getCreatedAt()));

        Glide.with(context)
                .load(complainList.get(position).getImage())
                .thumbnail(Glide.with(context).load(R.drawable.rect))
                .into(holder.ivPost);

        Glide.with(context)
                .load(complainModel.getCreated_by().getProfilePic())
                .thumbnail(Glide.with(context).load(R.drawable.rect))
                .into(holder.ivUser);

        holder.tvSeeMore.setOnClickListener(view -> {
            if (holder.tvPost.getVisibility() == View.VISIBLE) {
                holder.tvPost.setVisibility(View.GONE);
                holder.tvPostFull.setVisibility(View.VISIBLE);
                holder.tvSeeMore.setText(context.getResources().getString(R.string.see_less));


            } else {

                holder.tvPostFull.setVisibility(View.GONE);
                holder.tvPost.setVisibility(View.VISIBLE);
                holder.tvSeeMore.setText(context.getResources().getString(R.string.see_more));

            }

        });

        holder.tvChangeStatus.setOnClickListener(view -> {

            holder.tvChangeStatus.setVisibility(View.GONE);
            holder.llChangeStatus.setVisibility(View.VISIBLE);

            holder.llChangeStatus.startAnimation(animFadein);

            animFadein.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    holder.llChangeStatus.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        });

        holder.btnCancel.setOnClickListener(view -> {
            holder.llChangeStatus.startAnimation(animFadeOut);

            animFadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    holder.cbProgress.setChecked(false);
                    holder.cbProgress.setClickable(true);
                    holder.btnResolved.setVisibility(View.VISIBLE);
                    holder.cbResolved.setVisibility(View.GONE);
                    holder.llChangeStatus.setVisibility(View.GONE);
                    holder.tvChangeStatus.setVisibility(View.VISIBLE);
                    holder.llChangeStatus.clearAnimation();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        });

        PushDownAnim.setPushDownAnimTo(holder.cbProgress)
                .setScale(PushDownAnim.MODE_STATIC_DP, 7)
                .setDurationPush(70)
                .setDurationRelease(50)
                .setInterpolatorPush(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setInterpolatorRelease(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        holder.cbProgress.setClickable(false);
                        holder.btnResolved.setVisibility(View.GONE);
                        holder.cbResolved.setVisibility(View.VISIBLE);
                        holder.cbResolved.setChecked(false);

                    }
                });

        PushDownAnim.setPushDownAnimTo(holder.cbResolved)
                .setScale(PushDownAnim.MODE_STATIC_DP, 7)
                .setDurationPush(70)
                .setDurationRelease(50)
                .setInterpolatorPush(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setInterpolatorRelease(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.cbProgress.setChecked(true);
                        holder.cbProgress.setClickable(false);
                    }
                });

        holder.btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.cbProgress.isChecked()) {
                    ChangeStatus(1, holder,complainModel.getId(),position);
                } else if (holder.cbResolved.isChecked()) {
                    ChangeStatus(2, holder, complainModel.getId(),position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return complainList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivUser)
        CircleImageView ivUser;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvFlatNo)
        TextView tvFlatNo;
        @BindView(R.id.ivMore)
        ImageView ivMore;
        @BindView(R.id.ivPost)
        ImageView ivPost;
        @BindView(R.id.tvPost)
        TextView tvPost;
        @BindView(R.id.tvPostFull)
        TextView tvPostFull;
        @BindView(R.id.tvSeeMore)
        TextView tvSeeMore;
        @BindView(R.id.tvTimeAgo)
        TextView tvTimeAgo;
        @BindView(R.id.tvPostTitle)
        TextView tvPostTitle;
        @BindView(R.id.llChangeStatus)
        LinearLayout llChangeStatus;
        @BindView(R.id.tvChangeStatus)
        TextView tvChangeStatus;
        @BindView(R.id.btnCancel)
        Button btnCancel;
        @BindView(R.id.btnDone)
        Button btnDone;

        @BindView(R.id.cbProgress)
        CheckBox cbProgress;
        @BindView(R.id.btnResolved)
        Button btnResolved;
        @BindView(R.id.cbResolved)
        CheckBox cbResolved;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void ChangeStatus(int state, MyViewHolder holder, int complainId,int position) {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("status", state);

        new NetworkCall().callPostJSON(context, Services.COMPLAIN + "/" + complainId + "/status",
                jObj, true, NetworkCall.REQ_POST,
                true, (response, requestCode) -> {
                    String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                    Log.e("code-->", requestCode);
                    if (requestCode.equals("200")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
                            alertDialog.setTitle("Success!");
                            alertDialog.setMessage(message);
                            alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                complainList.remove(position);
                                notifyDataSetChanged();
                            });

                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (!returnResult.isEmpty()) {
                        return;
                    }
                });
    }
}
