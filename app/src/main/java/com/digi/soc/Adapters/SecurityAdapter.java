package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Fragments.EditStaffFragment;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.SecurityModel;
import com.digi.soc.Models.StaffModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("SetTextI18n")
public class SecurityAdapter extends RecyclerView.Adapter<SecurityAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<SecurityModel> dataList;
    private Boolean isEdit = true;

    public SecurityAdapter(Context context, ArrayList<SecurityModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public SecurityAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_security, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SecurityAdapter.MyViewHolder holder, int position) {
        SecurityModel securityModel = dataList.get(position);
        holder.tvname.setText(securityModel.getFirst_name()+" "+securityModel.getLast_name());
        holder.tvMob.setText(securityModel.getContact_no());

        holder.ivMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  showMenu(v , holder, position,securityModel.getId(),staffModel.getName(),staffModel.getContact_no(),staffModel.getWing_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvname)
        TextView tvname;

        @BindView(R.id.tvMob)
        TextView tvMob;


        @BindView(R.id.ivMore)
        ImageView ivMore;

        @BindView(R.id.layoutDisp)
        LinearLayout layoutDisp;

        @BindView(R.id.layoutEdit)
        LinearLayout layoutEdit;

        @BindView(R.id.edtGate)
        EditText edtGate;

        @BindView(R.id.ivClose)
        ImageView ivClose;

        @BindView(R.id.ivUpdate)
        ImageView ivUpdate;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void showMenu(View v, MyViewHolder holder, int position, int staffId, String staffName, String contact_no,String wingId) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_edit:
                        Bundle bundle = new Bundle();
                        bundle.putString("staffId", String.valueOf(staffId) );
                        bundle.putString("staffName", staffName );
                        bundle.putString("contact_no", contact_no );
                        bundle.putString("wingId", wingId );
                        MainActivity.changeAppartmentFragment(context, new EditStaffFragment(), true,bundle);
                        break;
                    case  R.id.action_delete:
                        deleteVendor(holder,position,staffId);
                        break;

                }
                return false;
            }

        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.rules_more_menu, popup.getMenu());
        popup.show();
    }
    private void deleteVendor(SecurityAdapter.MyViewHolder holder, int position, int staffId) {

        MyConstants.isResponceJsonOBJ = true;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.blank_warning_delete));

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall().callDeleteJSON(context, Services.DELETE_SOCIETY_PERSON + "/"+staffId,
                    true, NetworkCall.REQ_DELETE,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {

                            String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject error = jsonObject.getJSONObject("error");
                                    String message = error.getString("message");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        dataList.remove(position);
                                        notifyDataSetChanged();
                                    });
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }


}
