package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Models.BookingModel;
import com.digi.soc.Models.HomeModel;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<BookingModel> datalist;
    public BookingAdapter(Context context, ArrayList<BookingModel> datalist) {
        this.context = context;
        this.datalist = datalist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.booking_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        addData(holder);
    }

    private void addData(MyViewHolder holder) {

        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.rvBookinglist.setHasFixedSize(true);
        holder.rvBookinglist.setLayoutManager(layoutManager);
        holder.rvBookinglist.setNestedScrollingEnabled(false);
        BookingAdapter2 bookingAdapter2 = new BookingAdapter2(context, datalist);
        holder.rvBookinglist.setAdapter(bookingAdapter2);
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rvBookinglist)
        RecyclerView rvBookinglist;
        @BindView(R.id.btnAccept)
        Button btnAccept;
        @BindView(R.id.btnReject)
        Button btnReject;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}