package com.digi.soc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Models.BookingModel;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingAdapter2 extends RecyclerView.Adapter<BookingAdapter2.MyViewHolder> {
    private Context context;
    private ArrayList<BookingModel> datalist;

    public BookingAdapter2(Context context, ArrayList<BookingModel> datalist) {
        this.context = context;
        this.datalist = datalist;
    }

    @NonNull
    @Override
    public BookingAdapter2.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.booking_item2, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BookingAdapter2.MyViewHolder holder, int position) {
        BookingModel bookingModel = datalist.get(position);
        holder.tvBookingItem.setText(bookingModel.getChair());
        holder.tvBookingQnt.setText(bookingModel.getQnt());

    }


    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvBookingItem)
        TextView tvBookingItem;
        @BindView(R.id.tvBookingQnt)
        TextView tvBookingQnt;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}