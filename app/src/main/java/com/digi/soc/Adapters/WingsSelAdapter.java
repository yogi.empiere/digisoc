package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Helpers.OnWingDeleted;
import com.digi.soc.Models.AssetsModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WingsSelAdapter extends RecyclerView.Adapter<WingsSelAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<WingsModel> dataList;
    private OnWingDeleted onWingDeleted;
    private WingsSelAdapter wingsSelAdapter = this;

    public WingsSelAdapter(Context context, ArrayList<WingsModel> dataList,OnWingDeleted onWingDeleted) {
        super();
        this.context = context;
        this.dataList = dataList;
        this.onWingDeleted = onWingDeleted;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WingsSelAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.wing_selection_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        WingsModel wingsModel = dataList.get(position);

        if(wingsModel.isSelect()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.checkWing.setTextColor(context.getColor(R.color.white));
            }else{
                holder.checkWing.setTextColor(ContextCompat.getColor(context,R.color.white));
            }
            holder.checkWing.setChecked(true);
        }else{
            holder.checkWing.setChecked(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.checkWing.setTextColor(context.getColor(R.color.colorPrimary));
            }else{
                holder.checkWing.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
            }
        }

        holder.checkWing.setText(wingsModel.getName());
        //holder.checkWing.setText(wingsModel.getWings_Name());
        holder.checkWing.setOnCheckedChangeListener((compoundButton, b) -> {

            if(b){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.checkWing.setTextColor(context.getColor(R.color.white));
                }else{
                    holder.checkWing.setTextColor(ContextCompat.getColor(context,R.color.white));
                }
            }else{
                onWingDeleted.onWingDeleteListner();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.checkWing.setTextColor(context.getColor(R.color.colorPrimary));
                }else{
                    holder.checkWing.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                }
            }
            dataList.get(position).setSelect(b);
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.checkWing)
        CheckBox checkWing;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
