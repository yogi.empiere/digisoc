package com.digi.soc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.digi.soc.Models.VisitorModel;
import com.digi.soc.R;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VisitorAdapter extends RecyclerView.Adapter<VisitorAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<VisitorModel> dataList;

    public VisitorAdapter(Context context, ArrayList<VisitorModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public VisitorAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.visitor_wing_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VisitorAdapter.MyViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return 8;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvWingFlat)
        TextView tvWingFlat;
        @BindView(R.id.tvDateTime)
        TextView tvDateTime;
        @BindView(R.id.tvVisitor)
        TextView tvVisitor;
        @BindView(R.id.ivProfile)
        ImageView ivProfile;
        @BindView(R.id.ivCall)
        ImageView ivCall;
        @BindView(R.id.layoutDisp)
        LinearLayout layoutDisp;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
