package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Fragments.AddAppartmentFragment;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("SetTextI18n")
public class WingsAdapter extends RecyclerView.Adapter<WingsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<WingsModel> dataList;
    private Animation animFadein, animFadeOut;

    public WingsAdapter(Context context, ArrayList<WingsModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;

        animFadein = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out);
    }

    @NonNull
    @Override
    public WingsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.wings_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WingsAdapter.MyViewHolder holder, int position) {
        WingsModel model = dataList.get(position);
        if (dataList.size() > 0) {
            holder.tvWing.setText(model.getName());

            if(model.getFloors() != null){
                holder.layoutWings.removeAllViews();

                for(int i=0;i<model.getFloors().size();i++){
                    LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = vi.inflate(R.layout.row_apartment_list, null);

                    LinearLayout mainView = (LinearLayout) view.findViewById(R.id.llApartmentList);

                    setApartMents(model.getFloors().get(i).getApartments(), mainView,position,i);

                    holder.layoutWings.addView(view);
                }
            }
            if (dataList.get(position).isSelect()) {
                holder.layoutWings.setVisibility(View.VISIBLE);
              //  holder.itemView.startAnimation(animFadeOut);

              /*  animFadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        holder.itemView.clearAnimation();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });*/
            } else {
                holder.layoutWings.setVisibility(View.GONE);
               /* holder.itemView.startAnimation(animFadein);

                animFadein.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        holder.itemView.clearAnimation();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });*/
            }
           holder.ivMore.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   showMenu(v , holder, position,model.getId());
               }
           });
        }
        holder.ivClose.setOnClickListener(view -> {
            holder.layoutDisp.setVisibility(View.VISIBLE);
            holder.layoutEdit.setVisibility(View.GONE);
        });

       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               for (int i = 0; i < dataList.size(); i++) {
                   if (i == position) {
                       if (dataList.get(position).isSelect())
                           dataList.get(position).setSelect(false);
                       else
                           dataList.get(position).setSelect(true);
                   }else{
                       dataList.get(i).setSelect(false);
                   }
                   if(i == dataList.size()-1){
                       notifyDataSetChanged();
                   }
               }
           }
       });
    }

    private void setApartMents(ArrayList<WingsModel.ApartmentModel> arrdata, LinearLayout mainView, int wingsposition, int floorNo) {
        for (int i = 0; i < arrdata.size(); i++) {
            LayoutInflater v2 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = v2.inflate(R.layout.row_apartment_detail, null);

            TextView tvBlockName = (TextView) v.findViewById(R.id.tvBlockName);
            TextView tvBlockType = (TextView) v.findViewById(R.id.tvBlockType);
            ImageView ivmore     = (ImageView) v.findViewById(R.id.ivMore);

            int appartmentId = arrdata.get(i).getId();

            int finalI = i;
            ivmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenuForAppartment(v,appartmentId,wingsposition,finalI,floorNo);
                }
            });

            tvBlockName.setText(arrdata.get(i).getApartment_no());

            mainView.addView(v);
        }
    }

    @Override
    public int getItemCount() {
        if (dataList.size() == 0)
            return 8;
        else
            return dataList.size();
    }

    public void showMenu(View v, MyViewHolder holder, int position,int wingId ) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_edit:
                        holder.layoutDisp.setVisibility(View.GONE);
                        holder.layoutEdit.setVisibility(View.VISIBLE);
                        break;
                        case R.id.add_appartment:
                        addApprtment(wingId);
                        break;
                }
                return false;
            }


        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.wing_more_menu, popup.getMenu());
        popup.show();
    }

    public void showMenuForAppartment(View v,int appartmentId,int wingPosition, int appartmentPosition,int floorNo) {
        PopupMenu popup = new PopupMenu(context, v);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_delete:
                       deleteApprtment(appartmentId,wingPosition,appartmentPosition,floorNo);
                        break;
                }
                return false;
            }


        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.more_menu, popup.getMenu());
        popup.show();
    }
    private void addApprtment(int wingId) {
        Bundle bundle = new Bundle();
        int wing = wingId;
        bundle.putInt("WingId", wing );
        MainActivity.changeAppartmentFragment(context, new AddAppartmentFragment(), true,bundle);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvWing)
        TextView tvWing;
        @BindView(R.id.tvFlatRent)
        TextView tvFlatRent;
        @BindView(R.id.tvFlatRented)
        TextView tvFlatRented;
        @BindView(R.id.ivMore)
        ImageView ivMore;
        @BindView(R.id.layoutWings)
        LinearLayout layoutWings;

        @BindView(R.id.layoutDisp)
        LinearLayout layoutDisp;

        @BindView(R.id.layoutEdit)
        LinearLayout layoutEdit;

        @BindView(R.id.edtWing)
        EditText edtWing;

        @BindView(R.id.ivClose)
        ImageView ivClose;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void deleteApprtment(int appartmentId,int wingPosition,int appartmentPosition,int floorNo) {

        MyConstants.isResponceJsonOBJ = true;

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.blank_warning_delete));

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall().callDeleteJSON(context, Services.DELETE_APPARTMENT+ appartmentId,
                    true, NetworkCall.REQ_DELETE,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {
                            String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject error = jsonObject.getJSONObject("error");
                                    String message = error.getString("message");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        dataList.get(wingPosition).getFloors().get(floorNo).getApartments().remove(appartmentPosition);
                                        notifyDataSetChanged();
                                    });
                                    alertDialog.show();


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }
}
