package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.Models.HomeModel;
import com.digi.soc.Models.NoticeModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

@SuppressLint("SetTextI18n")
public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<NoticeModel> noticeDataList;

    private Animation animFadein, animFadeOut;

    public NoticeAdapter(Context context, ArrayList<NoticeModel> dataList) {
        super();
        this.context = context;
        this.noticeDataList = dataList;

        animFadein = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out);
    }

    @NonNull
    @Override
    public NoticeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.home_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoticeAdapter.MyViewHolder holder, int position) {
        NoticeModel noticeModel = noticeDataList.get(position);

        holder.tvNoticeTitle.setText(noticeModel.getTitle());
        holder.tvPost.setText(noticeModel.getDescription());
        holder.tvFlatNo.setText(noticeModel.getTitle());
        holder.tvName.setText(noticeModel.getCreated_by().getFirstName()+" "+noticeModel.getCreated_by().getLastName());
        holder.tvTimeAgo.setText(Utility.agoTimeShow(noticeModel.getCreatedAt()));

        Glide.with(context)
                .load(noticeDataList.get(position).getImage())
                .thumbnail(Glide.with(context).load(R.drawable.rect))
                .into(holder.ivPost);

        Glide.with(context)
                .load(noticeModel.getCreated_by().getProfilePic())
                .thumbnail(Glide.with(context).load(R.drawable.rect))
                .into(holder.ivUser);

        holder.tvSeeMore.setOnClickListener(view -> {
            if (holder.tvPost.getVisibility()==View.VISIBLE) {
                holder.tvPost.setVisibility(View.GONE);
                holder.tvPostFull.setVisibility(View.VISIBLE);
                holder.tvSeeMore.setText(context.getResources().getString(R.string.see_less));


            } else {
                holder.tvPostFull.setVisibility(View.GONE);
                holder.tvPost.setVisibility(View.VISIBLE);
                holder.tvSeeMore.setText(context.getResources().getString(R.string.see_more));

            }
        });

    }

    @Override
    public int getItemCount() {
        return noticeDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
         TextView tvName;
        @BindView(R.id.ivUser)
        CircleImageView ivUser;
        @BindView(R.id.tvFlatNo)
         TextView tvFlatNo;
        @BindView(R.id.ivMore)
         ImageView ivMore;
        @BindView(R.id.ivPost)
         ImageView ivPost;
        @BindView(R.id.tvPost)
         TextView tvPost;
        @BindView(R.id.tvPostFull)
         TextView tvPostFull;
        @BindView(R.id.tvSeeMore)
          TextView tvSeeMore;
        @BindView(R.id.tvTimeAgo)
         TextView tvTimeAgo;

        @BindView(R.id.tvNoticeTitle)
        TextView tvNoticeTitle;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
