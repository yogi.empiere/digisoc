package com.digi.soc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Models.MemberModel;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<MemberModel> dataList;

    private Animation animFadein, animFadeOut;

    public MemberAdapter(Context context, ArrayList<MemberModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;

        animFadein = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out);
    }

    @NonNull
    @Override
    public MemberAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.member_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MemberAdapter.MyViewHolder holder, int position) {

        holder.rvWing.setHasFixedSize(true);
        holder.rvWing.setLayoutManager(new LinearLayoutManager(context));
        holder.rvWing.setNestedScrollingEnabled(false);

        holder.rvWing.setAdapter(new ContactAdapter(context, new ArrayList<>()));


        holder.itemView.setOnClickListener(view -> {
            if (holder.rvWing.getVisibility()==View.VISIBLE) {
                holder.rvWing.startAnimation(animFadein);

                animFadein.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        holder.rvWing.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            } else {
                holder.itemView.startAnimation(animFadeOut);

                animFadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        holder.rvWing.setVisibility(View.VISIBLE);

                        holder.itemView.clearAnimation();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvWing)
        TextView tvWing;
        @BindView(R.id.rvWing)
        RecyclerView rvWing;
        @BindView(R.id.layoutWings)
        LinearLayout layoutWings;
        @BindView(R.id.layoutDisp)
        LinearLayout layoutDisp;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
