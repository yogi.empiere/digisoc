package com.digi.soc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Models.WingsSubModel;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WingsSubSubAdapter extends RecyclerView.Adapter<WingsSubSubAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<WingsSubModel> dataList;

    WingsSubSubAdapter(Context context, ArrayList<WingsSubModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public WingsSubSubAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.wings_sub_sub_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WingsSubSubAdapter.MyViewHolder holder, int position) {
        holder.tvWingNo.setText("10" + "" + position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvWingNo)
        TextView tvWingNo;
        @BindView(R.id.tvWingRent)
        TextView tvWingRent;
        @BindView(R.id.ivMore)
        ImageView ivMore;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
