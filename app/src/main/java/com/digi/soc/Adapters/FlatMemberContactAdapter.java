package com.digi.soc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.digi.soc.Models.MemberContactModel;
import com.digi.soc.R;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FlatMemberContactAdapter extends RecyclerView.Adapter<FlatMemberContactAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<MemberContactModel> dataList;

    public FlatMemberContactAdapter(Context context, ArrayList<MemberContactModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.flat_member_contact_item, parent, false);

        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvContact)
        TextView tvContact;
        @BindView(R.id.tvMob)
        TextView tvMob;
        @BindView(R.id.ivCall)
        ImageView ivCall;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

