package com.digi.soc.Adapters;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.Models.AlarmModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<AlarmModel>alarmlist;
    private int isclose ;

    public AlarmAdapter(Context context, ArrayList<AlarmModel> alarmlist,int isclose) {
        this.context = context;
        this.alarmlist = alarmlist;
        this.isclose = isclose;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.alarm_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AlarmModel model = alarmlist.get(position);

        holder.tvWingFlat.setText(model.getApartment().getWing().getName()+" - "+model.getApartment().getApartment_no());
        holder.tvVisitor.setText(model.getApartment().getCurrent_tenant().getFirst_name()+" "+model.getApartment().getCurrent_tenant().getLast_name());
        Glide.with(context)
                .load(model.getApartment().getCurrent_tenant().getProfile_pic())
                .circleCrop()
                .thumbnail(Glide.with(context).load(R.drawable.rect))
                .into(holder.ivProfile);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(model.getCreated_at());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            holder.tvDateTime.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeCall(model.getApartment().getCurrent_tenant().getContact_no());
            }
        });
        holder.ivMore.setOnClickListener(view -> showMenu(view, holder, position,model.getId()));

        holder.btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAlarm(model.getId(),position);
            }
        });
        if(isclose == 0){holder.btnClose.setVisibility(View.GONE);}
    }

    private void makeCall(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+phone));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }else{
                context.startActivity(callIntent);
            }
        }else {
            context.startActivity(callIntent);
        }
    }

    @Override
    public int getItemCount() {
        return alarmlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivProfile)
        ImageView ivProfile;

        @BindView(R.id.tvWingFlat)
        TextView tvWingFlat;

        @BindView(R.id.tvVisitor)
        TextView tvVisitor;

        @BindView(R.id.ivCall)
        ImageView ivCall;

        @BindView(R.id.tvDateTime)
        TextView tvDateTime;

        @BindView(R.id.tvNotes)
        TextView tvNotes;

        @BindView(R.id.edtNote)
        EditText edtNote;

        @BindView(R.id.btnClose)
        Button btnClose;

        @BindView(R.id.ivMore)
        ImageView ivMore;

        @BindView(R.id.layoutDisp)
        LinearLayout layoutDisp;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public void showMenu(View v, AlarmAdapter.MyViewHolder holder, int position,int alarmId) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_delete:
                        deleteAlarm(position,alarmId);
                        break;
                }
                return false;
            }
        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.more_menu, popup.getMenu());
        popup.show();
    }

    private void deleteAlarm(int position, int alarmId) {

        MyConstants.isResponceJsonOBJ = true;
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.blank_warning_delete));

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall().callDeleteJSON(context, Services.DELETE_ALARM + alarmId,
                    true, NetworkCall.REQ_DELETE,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {
                            String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject error = jsonObject.getJSONObject("error");
                                    String message = error.getString("message");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        alarmlist.remove(position);
                                        notifyDataSetChanged();
                                    });
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }

    private void closeAlarm(int alarmId, int position) {
        MyConstants.isResponceJsonOBJ = true;
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.blank_warning_close));

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall().callPutJSON(context, Services.CLOSE_ALARM + alarmId,
                    true, NetworkCall.REQ_PUT,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {

                            String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject error = jsonObject.getJSONObject("error");
                                    String message = error.getString("message");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        alarmlist.remove(position);
                                        notifyDataSetChanged();
                                    });
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }
}
