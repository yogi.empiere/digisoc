package com.digi.soc.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Fragments.FlatFragment;
import com.digi.soc.Fragments.HomeFragment;
import com.digi.soc.Fragments.PeopleFragment;
import com.digi.soc.Fragments.SocietyFragment;
import com.digi.soc.Fragments.SocietyTabFragment;
import com.digi.soc.MainActivity;
import com.digi.soc.R;
import com.digi.soc.SocietyTabActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BottomAdapter extends RecyclerView.Adapter<BottomAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<String> nameList;
    private ArrayList<Integer> iconList;
    private int selPos;

    public BottomAdapter(Context context, int selPos) {
        super();
        this.context = context;
        this.selPos = selPos;

        nameList = new ArrayList<>();
        iconList = new ArrayList<>();

        nameList.add("Home");
        iconList.add(selPos == 0 ?
                R.drawable.ic_bottom_home :
                R.drawable.ic_bottom_home_);

        nameList.add("Society");
        iconList.add(selPos == 1 ?
                R.drawable.ic_bottom_sociaty :
                R.drawable.ic_bottom_sociaty_);

        nameList.add("People");
        iconList.add(selPos == 2 ?
                R.drawable.ic_bottom_people :
                R.drawable.ic_bottom_people_);

        nameList.add("Visitor");
        iconList.add(selPos == 3 ?
                R.drawable.ic_bottom_visitor :
                R.drawable.ic_bottom_visitor_);

        nameList.add("My Flat");
        iconList.add(selPos == 4 ?
                R.drawable.ic_bottom_my_flat :
                R.drawable.ic_bottom_my_flat_);
    }

    @NonNull
    @Override
    public BottomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.bottom_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BottomAdapter.MyViewHolder holder, int position) {
        holder.ivBottom.setImageResource(iconList.get(position));
        holder.tvBottom.setText(nameList.get(position));

        if (selPos==position)
            holder.tvBottom.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        else
            holder.tvBottom.setTextColor(context.getResources().getColor(R.color.gray));

        holder.itemView.setOnClickListener(view -> {
            if (selPos!=position) {
                switch (position) {
                    case 0:
                        MainActivity.changeFragment(context, new HomeFragment(), true);
                        break;
                    case 1:
                        MainActivity.changeFragment(context, new SocietyFragment(), true);
                        break;
                    case 2:
                        MainActivity.changeFragment(context, new PeopleFragment(), true);
                        break;
                    case 3: {
                        Bundle bundle = new Bundle();
                        bundle.putInt("selTabPos", 0);
                        bundle.putString("type", "Visitor");
                        Fragment fragment = new SocietyTabFragment();
                        fragment.setArguments(bundle);
                        MainActivity.changeFragment(context, fragment,true);
                        break;
                    }
                    case 4:
                        MainActivity.changeFragment(context, new FlatFragment(), true);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvBottom)
        TextView tvBottom;
        @BindView(R.id.ivBottom)
        ImageView ivBottom;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
