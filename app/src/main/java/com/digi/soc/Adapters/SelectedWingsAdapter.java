package com.digi.soc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Helpers.OnWingDeleted;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectedWingsAdapter extends RecyclerView.Adapter<SelectedWingsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<WingsModel> dataList;
    private SelectedWingsAdapter wingsSelAdapter = this;
    private OnWingDeleted onWingDeleted;

    public SelectedWingsAdapter(Context context, ArrayList<WingsModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;
    }

    public SelectedWingsAdapter(Context context, ArrayList<WingsModel> dataList, OnWingDeleted onWingDeleted) {
        super();
        this.context = context;
        this.dataList = dataList;
        this.onWingDeleted = onWingDeleted;
    }


    @NonNull
    @Override
    public SelectedWingsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.selected_wing_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedWingsAdapter.MyViewHolder holder, int position) {
       WingsModel wingsModel = dataList.get(position);

        holder.tvWing.setText(wingsModel.getName());
        if (onWingDeleted != null) {
            holder.ivCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataList.remove(position);
                    notifyDataSetChanged();
                    onWingDeleted.onWingDeleteListner();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvWing)
        TextView tvWing;

        @BindView(R.id.ivCancel)
        ImageView ivCancel;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}