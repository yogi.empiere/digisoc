package com.digi.soc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Models.WingsSubModel;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WingsSubAdapter extends RecyclerView.Adapter<WingsSubAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<WingsSubModel> dataList;

    WingsSubAdapter(Context context, ArrayList<WingsSubModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public WingsSubAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.wings_sub_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WingsSubAdapter.MyViewHolder holder, int position) {
        holder.rvSubWing.setHasFixedSize(true);
        holder.rvSubWing.setLayoutManager(new LinearLayoutManager(context,
                RecyclerView.HORIZONTAL, false));
        holder.rvSubWing.setNestedScrollingEnabled(false);

        holder.rvSubWing.setAdapter(new WingsSubSubAdapter(context, new ArrayList<>()));
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rvSubWing)
        RecyclerView rvSubWing;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
