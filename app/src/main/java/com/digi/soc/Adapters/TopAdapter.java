package com.digi.soc.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Fragments.SocietyTabFragment;
import com.digi.soc.MainActivity;
import com.digi.soc.R;
import com.digi.soc.SocietyTabActivity;
import com.skyfishjy.library.RippleBackground;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopAdapter extends RecyclerView.Adapter<TopAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<String> nameList;
    private ArrayList<Integer> iconList;
    private int selPos;
    private boolean isGrid;
    private String type;

    public TopAdapter(Context context, ArrayList<String> nameList,
                      ArrayList<Integer> iconList, int selPos) {
        super();
        this.context = context;
        this.nameList = nameList;
        this.iconList = iconList;
        this.selPos = selPos;
        this.isGrid = false;
        this.type = "";
    }

    public TopAdapter(Context context, ArrayList<String> nameList,
                      ArrayList<Integer> iconList, int selPos, String type) {
        super();
        this.context = context;
        this.nameList = nameList;
        this.iconList = iconList;
        this.selPos = selPos;
        this.isGrid = true;
        this.type = type;
    }

    @NonNull
    @Override
    public TopAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(isGrid ? R.layout.top_item_1 : R.layout.top_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TopAdapter.MyViewHolder holder, int position) {
        holder.ivTop.setImageResource(iconList.get(position));
        holder.tvTop.setText(nameList.get(position));

        if (position == 0) {
            holder.mRipplePulseLayout.startRippleAnimation();
        }

        holder.itemView.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            Intent i = new Intent(context, SocietyTabActivity.class);
            boolean isCall = false;
            if (isGrid) {
                bundle.putInt("selTabPos", position);
                if (type.equals("Society")) {
                    bundle.putString("type", "Society");
                    isCall = true;
                }
                if (type.equals("People")) {
                    bundle.putString("type", "People");
                    isCall = true;
                }
            } else {
                if (nameList.get(position).equals("Notice")) {
                    bundle.putString("type", "Home_Notice");
                    bundle.putInt("selTabPos", 0);
                   /* i.putExtra("type", "Home_Notice");
                    i.putExtra("selTabPos",0);*/
                     isCall = true;
                }
               else if (nameList.get(position).equals("Complain")) {
                    bundle.putString("type", "Home_Complain");
                    bundle.putInt("selTabPos", 0);
                    /*  i.putExtra("type", "Home_Complain");
                      i.putExtra("selTabPos",0);*/
                    isCall = true;
                }
               else if (nameList.get(position).equals("Event")) {
                    bundle.putString("type", "Home_Event");
                    bundle.putInt("selTabPos", 0);
                    /* i.putExtra("type", "Home_Event");
                     i.putExtra("selTabPos",0);*/

                    isCall = true;
                }
               else if (nameList.get(position).equals("Member Req.")) {
                    bundle.putString("type", "Home_Member_Request");
                    bundle.putInt("selTabPos", 0);
                   /* i.putExtra("type", "Home_Member_Request");
                    i.putExtra("selTabPos",0);*/
                    isCall = true;
                }
               else if (nameList.get(position).equals("Polls")) {
                    bundle.putString("type", "Home_Polls");
                    bundle.putInt("selTabPos", 0);
                   /*  i.putExtra("type", "Home_Polls");
                     i.putExtra("selTabPos",0);*/
                    isCall = true;
                }

            }
            Fragment fragment = new SocietyTabFragment();
            fragment.setArguments(bundle);
            if (isCall)
               // context.startActivity(i);
            MainActivity.changeFragment(context, fragment, true);
        });
    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTop)
        TextView tvTop;
        @BindView(R.id.ivTop)
        ImageView ivTop;

        @BindView(R.id.content)
        RippleBackground mRipplePulseLayout;


        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
