package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Models.AssetsModel;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("SetTextI18n")
public class AssetsAdapter extends RecyclerView.Adapter<AssetsAdapter.MyViewHolder> {

    private Context context;

    public AssetsAdapter(Context context, ArrayList<AssetsModel> dataList) {
        super();
        this.context = context;
    }

    @NonNull
    @Override
    public AssetsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.assets_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetsAdapter.MyViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return 8;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvAssets)
        TextView tvAssets;
        @BindView(R.id.tvQnt)
        TextView tvQnt;
        @BindView(R.id.ivMore)
        ImageView ivMore;
        @BindView(R.id.layoutDisp)
        LinearLayout layoutDisp;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
