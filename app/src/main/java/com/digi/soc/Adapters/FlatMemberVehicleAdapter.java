package com.digi.soc.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Fragments.EditFlatVehicleFragment;
import com.digi.soc.Fragments.EditSocietyRulesFragment;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.VehicleModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;

import org.json.JSONObject;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FlatMemberVehicleAdapter extends RecyclerView.Adapter<FlatMemberVehicleAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<VehicleModel> dataList;

    public FlatMemberVehicleAdapter(Context context, ArrayList<VehicleModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.flat_member_vehicle_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        VehicleModel vehicleModel = dataList.get(position);
        holder.tvContact.setText(vehicleModel.getType());
        holder.tvMob.setText(vehicleModel.getNumber());
        holder.ivMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(v, holder, position, vehicleModel.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvContact)
        TextView tvContact;

        @BindView(R.id.tvMob)
        TextView tvMob;

        @BindView(R.id.ivUser)
        ImageView ivUser;

        @BindView(R.id.ivMore)
        ImageView ivMore;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void showMenu(View v, FlatMemberVehicleAdapter.MyViewHolder holder, int position, int vehicleId) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_edit:
                        Bundle bundle = new Bundle();
                        bundle.putInt("vehicleId", vehicleId );
                        MainActivity.changeAppartmentFragment(context, new EditFlatVehicleFragment(), true,bundle);
                        break;

                    case R.id.action_delete:
                        deleteVehicleNumber(position, vehicleId);
                        break;
                }
                return false;
            }
        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.rules_more_menu, popup.getMenu());
        popup.show();
    }

    private void deleteVehicleNumber(int position, int vehicleId) {
        MyConstants.isResponceJsonOBJ = true;
            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
            alertDialog.setTitle(context.getString(R.string.blank_warning_delete));

            alertDialog.setPositiveButton("Yes", (dialog, which) -> {
                new NetworkCall().callDeleteJSON(context, Services.GET_MEMBER_VEHICLE + "/" + vehicleId,
                        true, NetworkCall.REQ_DELETE,
                        true, new NetworkCall.MultiPartRequest() {
                            @Override
                            public void myResponseMethod(String response, String requestCode) {
                                String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                                if (requestCode.equals("200")) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject error = jsonObject.getJSONObject("error");
                                        String message = error.getString("message");

                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                        alertDialog.setTitle("Success");
                                        alertDialog.setMessage(message);
                                        alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                            dataList.remove(position);
                                            notifyDataSetChanged();
                                        });
                                        alertDialog.show();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (!returnResult.isEmpty()) {
                                    return;
                                }
                            }
                        });
            });
            alertDialog.setNegativeButton("No", (dialog, which) -> {
                dialog.cancel();
            });
            alertDialog.show();
        }



}
