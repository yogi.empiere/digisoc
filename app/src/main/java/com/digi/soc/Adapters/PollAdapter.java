package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Controls.ButtonMedium;
import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Controls.TextLight;
import com.digi.soc.Helpers.CustomProgress;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.Models.EventModel;
import com.digi.soc.Models.PollModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.gson.JsonObject;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

public class PollAdapter extends RecyclerView.Adapter<PollAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<PollModel> pollDataList;
    int isclose;

    public PollAdapter(Context context, ArrayList<PollModel> pollData, int isclose) {
        this.context = context;
        this.pollDataList = pollData;
        this.isclose = isclose;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.poll_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        PollModel pollModel = pollDataList.get(position);

        holder.tvTitle.setText(pollModel.getTitle());
        holder.ivMore.setOnClickListener(view -> showMenu(view, holder, position, pollModel.getId()));

        int totalVote =0,totalVoteCount=0;
        for(int i=0; i<pollModel.getOptions().size();i++)
        {
            totalVote = pollModel.getOptions().get(i).getNo_of_votes();
            totalVoteCount = totalVoteCount + totalVote;
        }
        holder.tvTotalVotes.setText(String.valueOf(totalVoteCount)+"Vote(s)");

        if (holder.radioGroup.getChildCount() == 0) {

            for (int i = 0; i < pollModel.getOptions().size(); i++) {

                LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = vi.inflate(R.layout.radiobutton, null);

                RadioButton rbOption = (RadioButton) view;
                rbOption.setId(i);
                if (i == 0) {
                    rbOption.setChecked(true);
                } else {
                    rbOption.setChecked(false);
                }
                final int pos;
                pos = i;
                rbOption.setText(pollModel.getOptions().get(i).getTitle());

                holder.btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // if (isChecked)
                            votePoll(pollModel.getOptions().get(pos).getId());
                    }
                });
                holder.radioGroup.addView(view);
            }
        }


    }

    private void votePoll(int id) {
        MyConstants.isResponceJsonOBJ = true;
        new NetworkCall().callPutJSON(context, Services.SOCIETY_POLL_VOTE_BY_ID + id,
                true, NetworkCall.REQ_POST, true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                        Log.e("code-->", requestCode);
                        Log.e("code-->", Services.SOCIETY_POLL_VOTE_BY_ID + id);
                        if (requestCode.equals("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean success = jsonObject.getBoolean("success");
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");
                                if(success)
                                { android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);
                                alertDialog.show();}
                                else {
                                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
                                    alertDialog.setTitle("Opps!");
                                    alertDialog.setMessage(message);
                                    alertDialog.show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });

    }

    @Override
    public int getItemCount() {
        return pollDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        TextLight tvTitle;

        @BindView(R.id.ivMore)
        ImageView ivMore;

        @BindView(R.id.radioGroup)
        RadioGroup radioGroup;

        @BindView(R.id.btnSubmit)
        ButtonMedium btnSubmit;

        @BindView(R.id.tvTotalVotes)
        TextView tvTotalVotes;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void showMenu(View v, PollAdapter.MyViewHolder holder, int position, int pollId) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_close:
                        closePoll(position, pollId);
                        break;

                    case R.id.action_delete:
                        deletePoll(position, pollId);
                        break;
                }
                return false;
            }
        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.poll_more_menu, popup.getMenu());
        popup.show();
    }

    private void closePoll(int position, int pollId) {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("poll_id", pollId);
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.blank_warning_close));

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall().callPostJSON(context, Services.SOCIETY_POLL_ClOSE_BY_ID,
                    jObj, true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {
                            String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject error = jsonObject.getJSONObject("error");
                                    String message = error.getString("message");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        pollDataList.remove(position);
                                        notifyDataSetChanged();
                                    });
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }

    private void deletePoll(int position, int pollId) {
        MyConstants.isResponceJsonOBJ = true;
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.blank_warning_delete));

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall().callDeleteJSON(context, Services.SOCIETY_POLL + "/" + pollId,
                    true, NetworkCall.REQ_DELETE,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {
                            String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject error = jsonObject.getJSONObject("error");
                                    String message = error.getString("message");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        pollDataList.remove(position);
                                        notifyDataSetChanged();
                                    });
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }
}
