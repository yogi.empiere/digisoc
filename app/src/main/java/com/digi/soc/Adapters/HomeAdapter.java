package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.Models.HomeModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

@SuppressLint("SetTextI18n")
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<HomeModel> homeDataList;

    private Animation animFadein, animFadeOut;

    public HomeAdapter(Context context, ArrayList<HomeModel> dataList) {
        super();
        this.context = context;
        this.homeDataList = dataList;

        animFadein = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out);
    }

    @NonNull
    @Override
    public HomeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.home_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapter.MyViewHolder holder, int position) {
        HomeModel homeModel=homeDataList.get(position);
        holder.tvPost.setText(homeModel.getDescription());
        holder.tvName.setText(homeModel.getCreated_by().getFirstName()+" "+homeModel.getCreated_by().getLastName());
        holder.tvTimeAgo.setText(Utility.agoTimeShow(homeModel.getCreatedAt()));

        Glide.with(context)
                .load(homeDataList.get(position).getImage())
                .thumbnail(Glide.with(context).load(R.drawable.rect))
                .into(holder.ivPost);

        Glide.with(context)
                .load(homeModel.getCreated_by().getProfilePic())
                .thumbnail(Glide.with(context).load(R.drawable.rect))
                .error(R.drawable.rect)
                .into(holder.ivUser);

        holder.tvSeeMore.setOnClickListener(view -> {
            if (holder.tvPost.getVisibility()==View.VISIBLE) {
                holder.tvPost.setVisibility(View.GONE);
                holder.tvPostFull.setVisibility(View.VISIBLE);
                holder.tvSeeMore.setText(context.getResources().getString(R.string.see_less));

            } else {
                holder.tvPostFull.setVisibility(View.GONE);
                holder.tvPost.setVisibility(View.VISIBLE);
                holder.tvSeeMore.setText(context.getResources().getString(R.string.see_more));

            }
        });
        holder.ivMore.setOnClickListener(view -> showMenu(view, holder, position,homeModel.getId())
        );
        holder.tvNoticeTitle.setVisibility(View.GONE);
    }
    @Override
    public int getItemCount() {
        return homeDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
         TextView tvName;

        @BindView(R.id.tvFlatNo)
         TextView tvFlatNo;

        @BindView(R.id.ivMore)
         ImageView ivMore;

        @BindView(R.id.ivPost)
         ImageView ivPost;

        @BindView(R.id.tvPost)
         TextView tvPost;

        @BindView(R.id.tvPostFull)
         TextView tvPostFull;

        @BindView(R.id.tvSeeMore)
          TextView tvSeeMore;

        @BindView(R.id.tvTimeAgo)
         TextView tvTimeAgo;

        @BindView(R.id.ivUser)
        CircleImageView ivUser;

        @BindView(R.id.layoutDisp)
        LinearLayout layoutDisp;

        @BindView(R.id.tvNoticeTitle)
        TextView tvNoticeTitle;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public void showMenu(View v, HomeAdapter.MyViewHolder holder, int position,int feedId) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(item -> {

            switch (item.getItemId()) {
                case R.id.action_delete:
                    deleteFeed(holder,position,feedId);
                    break;
            }
            return false;
        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.more_menu, popup.getMenu());
        popup.show();
    }
    private void deleteFeed(MyViewHolder holder, int position, int feedId) {

        MyConstants.isResponceJsonOBJ = true;

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.blank_warning_delete));

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall().callDeleteJSON(context, Services.DELETE_FEED + feedId,
                    true, NetworkCall.REQ_DELETE,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {

                            String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject error = jsonObject.getJSONObject("error");
                                    String message = error.getString("message");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        homeDataList.remove(position);
                                        notifyDataSetChanged();
                                    });
                                    alertDialog.show();


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }
}
