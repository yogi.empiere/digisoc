package com.digi.soc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Models.BookingModel;
import com.digi.soc.Models.PollModel;
import com.digi.soc.R;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PollAdapter2 extends RecyclerView.Adapter<PollAdapter2.MyViewHolder> {

    private Context context;
    private ArrayList<PollModel> datalist;

    public PollAdapter2(Context context, ArrayList<PollModel> datalist) {
        this.context = context;
        this.datalist = datalist;
    }

    @NonNull
    @Override
    public PollAdapter2.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.poll_item2, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PollAdapter2.MyViewHolder holder, int position) {
        PollModel pollModel = datalist.get(position);



       holder.rb_poll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.rb_poll.isSelected()) {
                    holder.rb_poll.setChecked(true);
                    holder.rb_poll.setSelected(true);
                } else {
                    holder.rb_poll.setChecked(false);
                    holder.rb_poll.setSelected(false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvPollReview)
        TextView tvPollReview;
        @BindView(R.id.tvPollPercentage)
        TextView tvPollPercentage;
        @BindView(R.id.progress_bar)
        RoundedHorizontalProgressBar progressBar;
        @BindView(R.id.rb_poll)
        RadioButton rb_poll;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
