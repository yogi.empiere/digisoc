package com.digi.soc.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Fragments.BottomSheetDialog.AttendeesDialogFragment;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.Models.EventModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<EventModel> eventModels;
    private FragmentManager manager;
    private Animation animFadein, animFadeOut;
    private Typeface typeface;

    public EventAdapter(FragmentManager manager,Context context, ArrayList<EventModel> eventModels) {
        super();
        this.manager = manager;
        this.context = context;
        this.eventModels = eventModels;
        animFadein = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out);
    }

    @NonNull
    @Override
    public EventAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.event_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EventAdapter.MyViewHolder holder, int position) {
        EventModel eventModel = eventModels.get(position);

        holder.tvDate.setText(Utility.eventDateFormator(eventModel.getStartTime()));
        System.out.println(Utility.eventDateFormator(eventModel.getStartTime())+"getTime");
        holder.tvPlace.setText(context.getResources().getString(R.string.place_collon)+eventModel.getPlace());
        holder.tvNotice.setText(eventModel.getTitle());
        holder.tvPost.setText(eventModel.getDescription());
        holder.tvPostFull.setText(eventModel.getDescription());
        if(holder.tvPostFull.getLineCount()>4) {
            Utility.makeTextViewResizable(4, context, holder.tvPostFull, 4, context.getResources().getString(R.string.see_more), true);
        }
        holder.tvTimeAgo.setText(Utility.agoTimeShow(eventModel.getCreatedAt()));

        holder.tvAttendence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AttendeesDialogFragment attendeesBottomSheetFragment = new AttendeesDialogFragment(eventModel.getAttendees());
                attendeesBottomSheetFragment.show(manager,attendeesBottomSheetFragment.getTag());
            }
        });

        holder.tvSeeMore.setOnClickListener(view -> {
            if (holder.tvPost.getVisibility() == View.VISIBLE) {
                holder.tvPost.setVisibility(View.GONE);
                holder.tvPostFull.setVisibility(View.VISIBLE);
                holder.tvSeeMore.setText(context.getResources().getString(R.string.see_less));


            } else {
                holder.tvPostFull.setVisibility(View.GONE);
                holder.tvPost.setVisibility(View.VISIBLE);
                holder.tvSeeMore.setText(context.getResources().getString(R.string.see_more));
            }

        });

        holder.ivMore.setOnClickListener(view -> showMenu(view, holder, position,eventModel.getId()));
    }

    @Override
    public int getItemCount() {
        return eventModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.ivMore)
        ImageView ivMore;
        @BindView(R.id.tvPlace)
        TextView tvPlace;
        @BindView(R.id.tvNotice)
        TextView tvNotice;
        @BindView(R.id.tvPost)
        TextView tvPost;
        @BindView(R.id.tvPostFull)
        TextView tvPostFull;
        @BindView(R.id.tvSeeMore)
        TextView tvSeeMore;
        @BindView(R.id.tvTimeAgo)
        TextView tvTimeAgo;
        @BindView(R.id.tvAttendence)
        TextView tvAttendence;

        @BindView(R.id.layoutDisp)
        LinearLayout layoutDisp;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void showMenu(View v, EventAdapter.MyViewHolder holder, int position,int eventId) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_delete:
                      deleteEvent(holder,position,eventId);
                        break;
                }
                return false;
            }
        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.more_menu, popup.getMenu());
        popup.show();
    }

    private void deleteEvent(MyViewHolder holder, int position, int eventId) {

        MyConstants.isResponceJsonOBJ = true;

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.blank_warning_delete));

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall().callDeleteJSON(context, Services.DELETE_EVENT + eventId,
                    true, NetworkCall.REQ_DELETE,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {

                            String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject error = jsonObject.getJSONObject("error");
                                    String message = error.getString("message");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        eventModels.remove(position);
                                        notifyDataSetChanged();
                                    });
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }
}
