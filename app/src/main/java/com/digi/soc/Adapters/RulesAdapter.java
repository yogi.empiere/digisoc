package com.digi.soc.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Fragments.EditSocietyRulesFragment;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.RulesModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("SetTextI18n")
public class RulesAdapter extends RecyclerView.Adapter<RulesAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<RulesModel> dataList;

    public RulesAdapter(Context context, ArrayList<RulesModel> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RulesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.rules_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RulesAdapter.MyViewHolder holder, int position) {
        RulesModel rulesModel = dataList.get(position);
        holder.tvRules.setText(rulesModel.getDescription());

        holder.ivMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(v , holder, position,rulesModel.getId());
            }
        });
    }
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvRules)
        TextView tvRules;

        @BindView(R.id.ivMore)
        ImageView ivMore;

        @BindView(R.id.ivClose)
        ImageView ivClose;

        @BindView(R.id.ivUpdate)
        ImageView ivUpdate;

        @BindView(R.id.layoutDisp)
        LinearLayout layoutDisp;

        @BindView(R.id.layoutEdit)
        LinearLayout layoutEdit;

        @BindView(R.id.edtRule)
        EditText edtRule;


        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public void showMenu(View v, RulesAdapter.MyViewHolder holder, int position, int ruleId ) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_edit:
                        Bundle bundle = new Bundle();
                        bundle.putInt("ruleId", ruleId );
                        MainActivity.changeAppartmentFragment(context, new EditSocietyRulesFragment(), true,bundle);
                        break;
                    case  R.id.action_delete:
                        deleteRule(holder,position,ruleId);
                        break;

                }
                return false;
            }

        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.rules_more_menu, popup.getMenu());
        popup.show();
    }

    private void deleteRule(RulesAdapter.MyViewHolder holder, int position, int ruleId) {

        MyConstants.isResponceJsonOBJ = true;

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.blank_warning_delete));

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall().callDeleteJSON(context, Services.SOCIETY_RULES + "/"+ruleId,
                    true, NetworkCall.REQ_DELETE,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {

                            String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject error = jsonObject.getJSONObject("error");
                                    String message = error.getString("message");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        dataList.remove(position);
                                        notifyDataSetChanged();
                                    });
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }
}
