package com.digi.soc.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.digi.soc.Controls.TextLight;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.Models.EventModel;
import com.digi.soc.Models.MemberRequestModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import androidx.fragment.app.FragmentManager;

import org.json.JSONObject;

public class MemberRequestAdapter extends RecyclerView.Adapter<MemberRequestAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<MemberRequestModel> MemberReqdatalist;
    private FragmentManager manager;

    public MemberRequestAdapter(FragmentManager manager,Context context, ArrayList<MemberRequestModel> memberRequestModels) {
        super();
        this.manager = manager;
        this.context = context;
        this.MemberReqdatalist = memberRequestModels;

    }

    @NonNull
    @Override
    public MemberRequestAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.member_request_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MemberRequestAdapter.MyViewHolder holder, int position) {
        MemberRequestModel memberRequestModel = MemberReqdatalist.get(position);
        holder.tvMemberName.setText(memberRequestModel.getUser().getFirstName() + " " + memberRequestModel.getUser().getLastName());
        Glide.with(context)
                .load(MemberReqdatalist.get(position).getUser().getProfilePic())
                .thumbnail(Glide.with(context).load(R.drawable.rect))
                .into(holder.ivProfile);
        holder.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptMemberReq(holder,position,memberRequestModel.getId());
            }
        });
        holder.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectMemberReq(holder,position,memberRequestModel.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return MemberReqdatalist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvMemberName)
        TextLight tvMemberName;
        @BindView(R.id.tvMemberId)
        TextView tvMemberId;
        @BindView(R.id.btnAccept)
        Button btnAccept;
        @BindView(R.id.btnReject)
        Button btnReject;
        @BindView(R.id.ivProfile)
        CircleImageView ivProfile;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void acceptMemberReq(MyViewHolder holder, int position, int memberId) {
        JsonObject jObj = new JsonObject();
        jObj.addProperty("id", memberId);
        MyConstants.isResponceJsonOBJ = true;

        new NetworkCall().callPostJSON(context, Services.MEMBER_ACCEPT,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                        Log.e("code-->", returnResult);
                        if (requestCode.equals("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");

                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);
                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                    MemberReqdatalist.remove(position);
                                    notifyDataSetChanged();
                                });
                                alertDialog.show();

                            }catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        }
                });
    }

    private void rejectMemberReq(MyViewHolder holder, int position, int memberId) {
        JsonObject jObj = new JsonObject();
        jObj.addProperty("id", memberId);
        MyConstants.isResponceJsonOBJ = true;

        new NetworkCall().callPostJSON(context, Services.MEMBER_REJECT,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(context, requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");

                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle("Alert!");
                                alertDialog.setMessage(message);
                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                    MemberReqdatalist.remove(position);
                                    notifyDataSetChanged();
                                });
                                alertDialog.show();

                            }catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }
}
