package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class NoticeModel implements Serializable {

    public NoticeModel(int id, String title, String description, String image, CreatedBy created_by, String createdAt, String updatedAt, boolean isRead, ArrayList<WingsModel> wings) {
        Id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.created_by = created_by;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.isRead = isRead;
        this.wings = wings;
    }

    @SerializedName("id")
    private int Id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("created_by")
    private CreatedBy created_by;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("isRead")
    private boolean isRead;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public CreatedBy getCreated_by() {
        return created_by;
    }

    public void setCreated_by(CreatedBy created_by) {
        this.created_by = created_by;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public static class CreatedBy {

        public CreatedBy(ArrayList<String> roles, int id, String firstName, String lastName, String email, String contactNo, String profilePic) {
            this.roles = roles;
            Id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.contactNo = contactNo;
            this.profilePic = profilePic;
        }

        ArrayList<String> roles;

        @SerializedName("id")
        int Id;

        @SerializedName("first_name")
        String firstName;

        @SerializedName("last_name")
        String lastName;

        @SerializedName("email")
        String email;

        @SerializedName("contact_no")
        String contactNo;

        @SerializedName("profile_pic")
        String profilePic;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public ArrayList<String> getRoles() {
            return roles;
        }

        public void setRoles(ArrayList<String> roles) {
            this.roles = roles;
        }
    }
    ArrayList<WingsModel> wings;

    public ArrayList<WingsModel> getWings() {
        return wings;
    }

    public void setWings(ArrayList<WingsModel> wings) {
        this.wings = wings;
    }
}
