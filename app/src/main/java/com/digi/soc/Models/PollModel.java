package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PollModel implements Serializable {

    @SerializedName("id")
    private int Id;

    @SerializedName("title")
    private String title;

    @SerializedName("isClosed")
    private boolean isClosed;

    @SerializedName("options")
    private ArrayList<Options> options;
    
    @SerializedName("wing")
    private WingDetail wing;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public ArrayList<Options> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<Options> options) {
        this.options = options;
    }


    public WingDetail getWing() {
        return wing;
    }

    public void setWing(WingDetail wing) {
        this.wing = wing;
    }

    public static class Options {

        @SerializedName("id")
        private int Id;

        @SerializedName("title")
        private String title;

        @SerializedName("no_of_votes")
        private int no_of_votes;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getNo_of_votes() {
            return no_of_votes;
        }

        public void setNo_of_votes(int no_of_votes) {
            this.no_of_votes = no_of_votes;
        }
    }

    public static class WingDetail {
        public WingDetail(int id, String name, String no_of_floors) {
            this.id = id;
            this.name = name;
            this.no_of_floors = no_of_floors;
        }

        int id;
        String name,no_of_floors;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNo_of_floors() {
            return no_of_floors;
        }

        public void setNo_of_floors(String no_of_floors) {
            this.no_of_floors = no_of_floors;
        }
    }

}
  /*"society_poll": {
          "id": 7,
          "title": "This is a poll title",
          "isClosed": false,
          "options": [
          {
          "id": 20,
          "title": "Option 1",
          "no_of_votes": 0
          },
          {
          "id": 21,
          "title": "Option 2",
          "no_of_votes": 0
          },
          {
          "id": 22,
          "title": "Option 3",
          "no_of_votes": 0
          },
          {
          "id": 23,
          "title": "Option 4",
          "no_of_votes": 0
          }
          ],
          "wings": [
          {
          "id": 9,
          "name": "Wing A",
          "no_of_floors": 4
          }
          ]
          },*/
