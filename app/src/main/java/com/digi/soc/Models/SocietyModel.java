package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SocietyModel implements Serializable {
    /*  "id": 1,
          "code": "624016",
          "name": "Roshni Apartments",
          "address": "Talav Gate, Street No , Junagadh",
          "pincode": 362001,
          "city": "Junagadh",
          "state": "Gujarat",
          "country": "India",
          "office_contact_no": "9737459777",
          "alt_contact_no": "",
          "manager_no": "9662661777",
          "chairman_no": "",
          "email": "bhavik.memento@gmail.com",
          "website": "https =>//mementotech.in",
          "sales_contact_no": "1234567890",
          "sales_address": "3rd Floor,  Sahkar Bhavan, Junagadh",
          "lattitude": 0,
          "longitude": 0*/

    @SerializedName("id")
    public int Id;

    @SerializedName("code")
    public String code;

    @SerializedName("name")
    public String name;

    @SerializedName("address")
    public String address;

    @SerializedName("pincode")
    public int pincode;

    @SerializedName("city")
    public String city;

    @SerializedName("state")
    public String state;

    @SerializedName("country")
    public String country;

    @SerializedName("office_contact_no")
    public String office_contact_no;

    @SerializedName("alt_contact_no")
    public String alt_contact_no;

    @SerializedName("manager_no")
    public String manager_no;

    @SerializedName("chairman_no")
    public String chairman_no;

    @SerializedName("email")
    public String email;

    @SerializedName("website")
    public String website;

    @SerializedName("sales_contact_no")
    public String sales_contact_no;

    @SerializedName("sales_address")
    public String sales_address;

    @SerializedName("lattitude")
    public int lattitude;

    @SerializedName("longitude")
    public int longitude;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOffice_contact_no() {
        return office_contact_no;
    }

    public void setOffice_contact_no(String office_contact_no) {
        this.office_contact_no = office_contact_no;
    }

    public String getAlt_contact_no() {
        return alt_contact_no;
    }

    public void setAlt_contact_no(String alt_contact_no) {
        this.alt_contact_no = alt_contact_no;
    }

    public String getManager_no() {
        return manager_no;
    }

    public void setManager_no(String manager_no) {
        this.manager_no = manager_no;
    }

    public String getChairman_no() {
        return chairman_no;
    }

    public void setChairman_no(String chairman_no) {
        this.chairman_no = chairman_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getSales_contact_no() {
        return sales_contact_no;
    }

    public void setSales_contact_no(String sales_contact_no) {
        this.sales_contact_no = sales_contact_no;
    }

    public String getSales_address() {
        return sales_address;
    }

    public void setSales_address(String sales_address) {
        this.sales_address = sales_address;
    }

    public int getLattitude() {
        return lattitude;
    }

    public void setLattitude(int lattitude) {
        this.lattitude = lattitude;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }
}
