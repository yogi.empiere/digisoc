package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

public class GatesModel {
    @SerializedName("id")
    public int Id;

    @SerializedName("name")
    public String name;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
