package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VehicleTypeModel implements Serializable {

    @SerializedName("TWO_WHEELS")
    public String TWO_WHEELS;

    @SerializedName("FOUR_WHEELS")
    public String FOUR_WHEELS;

    public String getTWO_WHEELS() {
        return TWO_WHEELS;
    }

    public void setTWO_WHEELS(String TWO_WHEELS) {
        this.TWO_WHEELS = TWO_WHEELS;
    }

    public String getFOUR_WHEELS() {
        return FOUR_WHEELS;
    }

    public void setFOUR_WHEELS(String FOUR_WHEELS) {
        this.FOUR_WHEELS = FOUR_WHEELS;
    }
}


/*
 "vehicle_types": {
        "TWO_WHEELS": "2 Wheeler",
        "FOUR_WHEELS": "4 Wheeler"
    }*/
