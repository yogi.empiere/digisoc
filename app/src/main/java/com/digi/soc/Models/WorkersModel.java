package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

public class WorkersModel {
    @SerializedName("id")
    public int Id;

    @SerializedName("type")
    public String type;

    @SerializedName("name")
    public String name;

    @SerializedName("contact_no")
    public String contact_no;

    @SerializedName("wing_id")
    public String wing_id;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getWing_id() {
        return wing_id;
    }

    public void setWing_id(String wing_id) {
        this.wing_id = wing_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
