package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

public class EmergencyModel {
    @SerializedName("id")
    public int Id;

    @SerializedName("name")
    public String name;

    @SerializedName("contact_no")
    public String contact_no;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }
}
