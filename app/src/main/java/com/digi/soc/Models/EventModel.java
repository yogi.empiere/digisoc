package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class EventModel implements Serializable {

    public EventModel(int id, String title, String description, String place, String startTime, String endTime, String updatedAt, String createdAt, CreatedBy createdBy, ArrayList<Attendees> wings) {
        Id = id;
        this.title = title;
        this.description = description;
        this.place = place;
        this.startTime = startTime;
        this.endTime = endTime;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.attendees = wings;
    }

    @SerializedName("id")
    private int Id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("place")
    private String place;

    @SerializedName("start_time")
    private String startTime;

    @SerializedName("end_time")
    public String endTime;

    @SerializedName("updated_at")
    public String updatedAt;

    @SerializedName("created_at")
    public String createdAt;

    @SerializedName("society")
    private Society society;

    @SerializedName("created_by")
    private CreatedBy createdBy;

    @SerializedName("attendees")
    private ArrayList<Attendees> attendees;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Society getSociety() {
        return society;
    }

    public void setSociety(Society society) {
        this.society = society;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public ArrayList<Attendees> getAttendees() {
        return attendees;
    }

    public void setAttendees(ArrayList<Attendees> attendees) {
        this.attendees = attendees;
    }

    public class Society {

        @SerializedName("id")
        private int id;

        @SerializedName("code")
        private String code;

        @SerializedName("name")
        private String name;

        @SerializedName("address")
        private String address;

        @SerializedName("pincode")
        private int pincode;

        @SerializedName("city")
        private String city;

        @SerializedName("state")
        private String state;

        @SerializedName("country")
        private String country;

        @SerializedName("office_contact_no")
        private String officeContactNo;

        @SerializedName("alt_contact_no")
        private String altContactNo;

        @SerializedName("manager_no")
        private String managerNo;

        @SerializedName("chairman_no")
        private String chairmanNo;

        @SerializedName("email")
        private String email;

        @SerializedName("website")
        private String website;

        @SerializedName("sales_contact_no")
        private String salesContactNo;

        @SerializedName("sales_address")
        private String salesAddress;

        @SerializedName("lattitude")
        private double lattitude;

        @SerializedName("longitude")
        private double longitude;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getPincode() {
            return pincode;
        }

        public void setPincode(int pincode) {
            this.pincode = pincode;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getOfficeContactNo() {
            return officeContactNo;
        }

        public void setOfficeContactNo(String officeContactNo) {
            this.officeContactNo = officeContactNo;
        }

        public String getAltContactNo() {
            return altContactNo;
        }

        public void setAltContactNo(String altContactNo) {
            this.altContactNo = altContactNo;
        }

        public String getManagerNo() {
            return managerNo;
        }

        public void setManagerNo(String managerNo) {
            this.managerNo = managerNo;
        }

        public String getChairmanNo() {
            return chairmanNo;
        }

        public void setChairmanNo(String chairmanNo) {
            this.chairmanNo = chairmanNo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getSalesContactNo() {
            return salesContactNo;
        }

        public void setSalesContactNo(String salesContactNo) {
            this.salesContactNo = salesContactNo;
        }

        public String getSalesAddress() {
            return salesAddress;
        }

        public void setSalesAddress(String salesAddress) {
            this.salesAddress = salesAddress;
        }

        public double getLattitude() {
            return lattitude;
        }

        public void setLattitude(double lattitude) {
            this.lattitude = lattitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }

    public static class CreatedBy {

        public CreatedBy(int id, String firstName, String lastName, String email, String contactNo, String profilePic, ArrayList<String> roles) {
            Id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.contactNo = contactNo;
            this.profilePic = profilePic;
            this.roles = roles;
        }

        @SerializedName("id")
        int Id;

        @SerializedName("first_name")
        String firstName;

        @SerializedName("last_name")
        String lastName;

        @SerializedName("email")
        String email;

        @SerializedName("contact_no")
        String contactNo;

        @SerializedName("profile_pic")
        String profilePic;

        ArrayList<String> roles;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }
    }

    public class Attendees{
        @SerializedName("id")
        int Id;

        @SerializedName("name")
        String name;
        @SerializedName("no_of_floors")
        String no_of_floors;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNo_of_floors() {
            return no_of_floors;
        }

        public void setNo_of_floors(String no_of_floors) {
            this.no_of_floors = no_of_floors;
        }
    }

}
