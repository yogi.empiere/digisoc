package com.digi.soc.Models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WingsModel {

    public String  Wings_Name;
    public int Wings_id,no_of_floors;
    private boolean isSelect;
    ArrayList<FloorsModel> floors;
    ArrayList<ApartmentModel> apartments;
    private ArrayList<WingsSubModel> wingsSubList;

    public ArrayList<FloorsModel> getFloors() {
        return floors;
    }

    public void setFloors(ArrayList<FloorsModel> floors) {
        this.floors = floors;
    }

    public ArrayList<WingsSubModel> getWingsSubList() {
        return wingsSubList;
    }

    public void setWingsSubList(ArrayList<WingsSubModel> wingsSubList) {
        this.wingsSubList = wingsSubList;
    }

    public ArrayList<ApartmentModel> getApartmentList() {
        return apartments;
    }

    public int getWings_id() {
        return Wings_id;
    }

    public void setWings_id(int wings_id) {
        Wings_id = wings_id;
    }

    public String getWings_Name() {
        return Wings_Name;
    }

    public void setWings_Name(String wings_Name) {
        Wings_Name = wings_Name;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public int getNo_of_floors() {
        return no_of_floors;
    }

    public void setNo_of_floors(int no_of_floors) {
        this.no_of_floors = no_of_floors;
    }

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public class FloorsModel{
        int floor_no;
        ArrayList<ApartmentModel> apartments;

        public int getFloor_no() {
            return floor_no;
        }

        public void setFloor_no(int floor_no) {
            this.floor_no = floor_no;
        }

        public ArrayList<ApartmentModel> getApartments() {
            return apartments;
        }

        public void setApartments(ArrayList<ApartmentModel> apartments) {
            this.apartments = apartments;
        }

    }

    public class ApartmentModel{
        int id;
        int floor_no;
        String apartment_no;
        boolean isRented;
        OwnerDetail current_tenant;
        OwnerDetail owned_by;

        public int getFloor_no() {
            return floor_no;
        }

        public void setFloor_no(int floor_no) {
            this.floor_no = floor_no;
        }

        public int getId() {
            return id;
        }

        public String getApartment_no() {
            return apartment_no;
        }

        public boolean isRented() {
            return isRented;
        }

        public OwnerDetail getCurrent_tenant() {
            return current_tenant;
        }

        public OwnerDetail getOwned_by() {
            return owned_by;
        }


        @NonNull
        @Override
        public String toString() {
            return apartment_no;
        }
    }

    public class OwnerDetail{
        int id;
        String first_name,
                last_name,
                email,
                contact_no,
                profile_pic;
        ArrayList<String> roles;

        public int getId() {
            return id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getEmail() {
            return email;
        }

        public String getContact_no() {
            return contact_no;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public ArrayList<String> getRoles() {
            return roles;
        }
    }
}
