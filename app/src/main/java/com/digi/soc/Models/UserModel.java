package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserModel implements Serializable {

    @SerializedName("wings")
    public Wings wings;

    @SerializedName("feed")
    public Feeds feed;

    @SerializedName("notices")
    public Notices notices;

    @SerializedName("complaints")
    public Complaints complaints;


    public static class Wings {

        @SerializedName("list")
        public List<WingsModel> wingsModels = new ArrayList<>();

        public List<WingsModel> getWingsModels() {
            return wingsModels;
        }

        public void setWingsModels(List<WingsModel> wingsModels) {
            this.wingsModels = wingsModels;
        }
    }

    public static class Feeds {

        @SerializedName("list")
        public List<HomeModel> homeModels = new ArrayList<>();

        public List<HomeModel> getHomeModels() {
            return homeModels;
        }

        public void setHomeModels(List<HomeModel> homeModels) {
            this.homeModels = homeModels;
        }
    }
    public static class Notices {
        @SerializedName("list")
        public List<NoticeModel> noticeModels = new ArrayList<>();

        public List<NoticeModel> getNoticeModels() {
            return noticeModels;
        }

        public void setNoticeModels(List<NoticeModel> noticeModels) {
            this.noticeModels = noticeModels;
        }
    }

    public static class Complaints {
        @SerializedName("list")
        public List<ComplainModel> complainModels = new ArrayList<>();

        public List<ComplainModel> getComplainModels() {
            return complainModels;
        }
        public void setComplainModels(List<ComplainModel> complainModels) {
            this.complainModels = complainModels;
        }
    }
}
