package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VehicleModel implements Serializable {
    @SerializedName("id")
    public int Id;

    @SerializedName("type")
    public String type;

    @SerializedName("number")
    public String number;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}


/*
"id": 1,
        "type": "TWO_WHEELER",
        "number": "GJ-11-AB-4383"*/
