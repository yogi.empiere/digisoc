package com.digi.soc.Models;

import java.io.Serializable;

public class WingsSubModel implements Serializable {
    public String Wing_Id, Wing_Name;

    public String getWing_Id() {
        return Wing_Id;
    }

    public void setWing_Id(String wing_Id) {
        Wing_Id = wing_Id;
    }

    public String getWing_Name() {
        return Wing_Name;
    }

    public void setWing_Name(String wing_Name) {
        Wing_Name = wing_Name;
    }
}
