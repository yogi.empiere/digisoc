package com.digi.soc.Models;

import java.io.Serializable;

public class BookingModel implements Serializable {
    public String Chair, Qnt;

    public String getChair() {
        return Chair;
    }

    public void setChair(String chair) {
        Chair = chair;
    }

    public String getQnt() {
        return Qnt;
    }

    public void setQnt(String qnt) {
        Qnt = qnt;
    }
}
