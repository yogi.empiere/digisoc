package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HomeModel implements Serializable {

    @SerializedName("id")
    private int Id;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("isRead")
    private boolean isRead;

    @SerializedName("created_by")
    private CreatedBy created_by;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public CreatedBy getCreated_by() {
        return created_by;
    }

    public void setCreated_by(CreatedBy created_by) {
        this.created_by = created_by;
    }

    public static class CreatedBy {

        @SerializedName("id")
        int Id;

        @SerializedName("first_name")
        String firstName;

        @SerializedName("last_name")
        String lastName;

        @SerializedName("email")
        String email;

        @SerializedName("contact_no")
        public String contactNo;

        @SerializedName("profile_pic")
        String profilePic;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }
    }
}


/*
"created_by": {
        "id": 2,
        "first_name": "Vivek",
        "last_name": "Sadariya",
        "email": "",
        "contact_no": "8141297933",
        "profile_pic": "https://lifeatbrio.com/wp-content/uploads/2016/11/user-placeholder.jpg",*/
