package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AlarmModel implements Serializable { //"created_at":"2020-04-07 11:32:09"

    public AlarmModel(int id, String type, String status, Apartment apartment, String created_at, OwnedDetail created_by) {
        Id = id;
        this.type = type;
        this.status = status;
        this.apartment = apartment;
        this.created_at = created_at;
        this.created_by = created_by;

    }

    @SerializedName("id")
    private int Id;

    @SerializedName("type")
    private String type;

    @SerializedName("status")
    private String status;

    @SerializedName("apartment")
    private Apartment apartment;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("created_by")
    private OwnedDetail created_by;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public OwnedDetail getCreated_by() {
        return created_by;
    }

    public void setCreated_by(OwnedDetail created_by) {
        this.created_by = created_by;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Apartment {
        public Apartment(boolean isRented, int id, int floor_no, String apartment_no, OwnedDetail current_tenant, OwnedDetail owned_by, WingDetail wing) {
            this.isRented = isRented;
            Id = id;
            this.floor_no = floor_no;
            this.apartment_no = apartment_no;
            this.current_tenant = current_tenant;
            this.owned_by = owned_by;
            this.wing = wing;
        }

        boolean isRented;
        @SerializedName("id")
        private int Id;

        @SerializedName("floor_no")
        private int floor_no;

        @SerializedName("apartment_no")
        private String apartment_no;

        @SerializedName("current_tenant")
        private OwnedDetail current_tenant;

        @SerializedName("owned_by")
        private OwnedDetail owned_by;

        @SerializedName("wing")
        private WingDetail wing;

        public int getFloor_no() {
            return floor_no;
        }

        public void setFloor_no(int floor_no) {
            this.floor_no = floor_no;
        }

        public void setRented(boolean rented) {
            isRented = rented;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public void setApartment_no(String apartment_no) {
            this.apartment_no = apartment_no;
        }

        public void setCurrent_tenant(OwnedDetail current_tenant) {
            this.current_tenant = current_tenant;
        }

        public void setOwned_by(OwnedDetail owned_by) {
            this.owned_by = owned_by;
        }

        public void setWing(WingDetail wing) {
            this.wing = wing;
        }

        public String getApartment_no() {
            return apartment_no;
        }

        public boolean isRented() {
            return isRented;
        }

        public OwnedDetail getCurrent_tenant() {
            return current_tenant;
        }

        public OwnedDetail getOwned_by() {
            return owned_by;
        }

        public WingDetail getWing() {
            return wing;
        }
    }

    public static class  OwnedDetail {
        public OwnedDetail(int id, String first_name, String last_name, String email, String contact_no, String profile_pic, ArrayList<String> roles) {
            this.id = id;
            this.first_name = first_name;
            this.last_name = last_name;
            this.email = email;
            this.contact_no = contact_no;
            this.profile_pic = profile_pic;
            this.roles = roles;
        }

        @SerializedName("id")
        private int id;

        @SerializedName("first_name")
        private String first_name;

        @SerializedName("last_name")
        private String last_name;

        @SerializedName("email")
        private String email;

        @SerializedName("contact_no")
        private String contact_no;

        @SerializedName("profile_pic")
        private String profile_pic;

        ArrayList<String> roles;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContact_no() {
            return contact_no;
        }

        public void setContact_no(String contact_no) {
            this.contact_no = contact_no;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public ArrayList<String> getRoles() {
            return roles;
        }

        public void setRoles(ArrayList<String> roles) {
            this.roles = roles;
        }
    }

    public static class WingDetail {
        public WingDetail(int id, String name, String no_of_floors) {
            this.id = id;
            this.name = name;
            this.no_of_floors = no_of_floors;
        }

        int id;
        String name,no_of_floors;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNo_of_floors() {
            return no_of_floors;
        }

        public void setNo_of_floors(String no_of_floors) {
            this.no_of_floors = no_of_floors;
        }
    }


}



/*
* "alarms": {
        "active": {
            "list": [
                {
                    "id": 1,
                    "type": "This is a alarm type",
                    "status": "active",
                    "apartment": {
                        "id": 2,
                        "apartment_no": "101",
                        "isRented": false,
                        "current_tenant": {
                            "id": 2,
                            "first_name": "Vivek",
                            "last_name": "Sadariya",
                            "email": "",
                            "contact_no": "8141297933",
                            "profile_pic": "https://lifeatbrio.com/wp-content/uploads/2016/11/user-placeholder.jpg",
                            "roles": [
                                "member",
                                "secretary"
                            ]
                        },
                        "owned_by": {
                            "id": 2,
                            "first_name": "Vivek",
                            "last_name": "Sadariya",
                            "email": "",
                            "contact_no": "8141297933",
                            "profile_pic": "https://lifeatbrio.com/wp-content/uploads/2016/11/user-placeholder.jpg",
                            "roles": [
                                "member",
                                "secretary"
                            ]
                        },
                        "wing": {
                            "id": 1,
                            "name": "Wing A"
                        }
                    },
                    "created_at": "2020-04-07 11:32:09",
                    "created_by": {
                        "id": 2,
                        "first_name": "Vivek",
                        "last_name": "Sadariya",
                        "email": "",
                        "contact_no": "8141297933",
                        "profile_pic": "https://lifeatbrio.com/wp-content/uploads/2016/11/user-placeholder.jpg",
                        "roles": [
                            "member",
                            "secretary"
                        ]
                    }
                }
            ],
            "meta": {
                "current_page": 1,
                "next_page_url": "",
                "per_page": 10,
                "has_more_pages": false
            }
        },
        "closed": {
            "list": [],
            "meta": {
                "current_page": 1,
                "next_page_url": "",
                "per_page": 10,
                "has_more_pages": false
            }
        }
    },
* */