package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

public class RulesModel {

    @SerializedName("id")
    public int Id;

    @SerializedName("description")
    public String description;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
/*
"id": 1,
        "description": "This is a rule"*/
