package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

public class SecurityModel {
    @SerializedName("id")
    public int Id;

    @SerializedName("first_name")
    public String first_name;

    @SerializedName("last_name")
    public String last_name;

    @SerializedName("contact_no")
    public String contact_no;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return first_name;
    }

    public void setName(String name) {
        this.first_name = name;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }


 /* "security_guard": {
        "id": 21,
                "first_name": "Karim",
                "last_name": "Raju",
                "contact_no": "7405769522",
                "roles": [
        "security-guard"
        ],
        "security_guard_profile": {
            "id": 3,
                    "society_id": 1,
                    "secretary_id": 2,
                    "created_at": "2020-07-21T13:04:55.000000Z"
        }
    }*/
}
