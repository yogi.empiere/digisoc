package com.digi.soc.Models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApartmentModel {
    int id;
    int floor_no;
    String apartment_no;
    boolean isRented;

    public void setId(int id) {
        this.id = id;
    }

    public void setApartment_no(String apartment_no) {
        this.apartment_no = apartment_no;
    }

    OwnerDetail current_tenant;
    OwnerDetail owned_by;
    Wing wing;

    public int getFloor_no() {
        return floor_no;
    }

    public void setFloor_no(int floor_no) {
        this.floor_no = floor_no;
    }

    public int getId() {
        return id;
    }

    public String getApartment_no() {
        return apartment_no;
    }

    public boolean isRented() {
        return isRented;
    }

    public OwnerDetail getCurrent_tenant() {
        return current_tenant;
    }

    public OwnerDetail getOwned_by() {
        return owned_by;
    }

    public class OwnerDetail{
        int id;
        String first_name,
                last_name,
                email,
                contact_no,
                profile_pic;
        ArrayList<String> roles;

        public int getId() {
            return id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getEmail() {
            return email;
        }

        public String getContact_no() {
            return contact_no;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public ArrayList<String> getRoles() {
            return roles;
        }
    }
    public class Wing
    {
        int id,no_of_floors;
        String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getNo_of_floors() {
            return no_of_floors;
        }

        public void setNo_of_floors(int no_of_floors) {
            this.no_of_floors = no_of_floors;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return apartment_no ;
    }
}
