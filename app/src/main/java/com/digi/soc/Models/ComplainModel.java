package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ComplainModel implements Serializable {

    public ComplainModel(int id, String title, String description, int current_status, String image, CreatedBy created_by, String createdAt, String updatedAt, ArrayList<ComplainModel.complaint_status> complaint_status, WingDetail wing) {
        Id = id;
        this.title = title;
        this.description = description;
        this.current_status = current_status;
        this.image = image;
        this.created_by = created_by;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.complaint_status = complaint_status;
        this.wingDetail = wing;
    }

    @SerializedName("id")
    private int Id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("apartment_no")
    private String apartment_no;

    @SerializedName("current_status")
    private int current_status;

    @SerializedName("image")
    private String image;

    @SerializedName("created_by")
    private CreatedBy created_by;

    @SerializedName("created_at")
    public String createdAt;

    @SerializedName("updated_at")
    public String updatedAt;

    @SerializedName("complaint_status")
    private ArrayList<complaint_status> complaint_status;

    @SerializedName("wing")
    private WingDetail wingDetail;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public CreatedBy getCreated_by() {
        return created_by;
    }

    public void setCreated_by(CreatedBy created_by) {
        this.created_by = created_by;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getApartment_no() {
        return apartment_no;
    }

    public int getCurrent_status() {
        return current_status;
    }

    public void setCurrent_status(int current_status) {
        this.current_status = current_status;
    }

    public WingDetail getWingDetail() {
        return wingDetail;
    }

    public void setWingDetail(WingDetail wingDetail) {
        this.wingDetail = wingDetail;
    }

    public void setApartment_no(String apartment_no) {
        this.apartment_no = apartment_no;
    }

    public ArrayList<complaint_status> getComplaint_status() {
        return complaint_status;
    }

    public void setComplaint_status(ArrayList<complaint_status> complaint_status) {
        this.complaint_status = complaint_status;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public static class CreatedBy {
        public CreatedBy(int id, String firstName, String lastName, String email, String contactNo, String profilePic, ArrayList<String> roles) {
            Id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.contactNo = contactNo;
            this.profilePic = profilePic;
            this.roles = roles;
        }

        @SerializedName("id")
        int Id;

        @SerializedName("first_name")
        String firstName;

        @SerializedName("last_name")
        String lastName;

        @SerializedName("email")
        String email;

        @SerializedName("contact_no")
        String contactNo;

        @SerializedName("profile_pic")
        String profilePic;
        ArrayList<String> roles;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public ArrayList<String> getRoles() {
            return roles;
        }

        public void setRoles(ArrayList<String> roles) {
            this.roles = roles;
        }
    }

    public static class complaint_status {

        public complaint_status(int status, String note, Updated_user updated_by_user, String created_at, String updated_at) {
            this.status = status;
            this.note = note;
            this.updated_by_user = updated_by_user;
            this.created_at = created_at;
            this.updated_at = updated_at;
        }

        @SerializedName("status")
        int status;

        @SerializedName("note")
        String note;

        @SerializedName("updated_by_user")
        private Updated_user updated_by_user;

        public static class Updated_user {
            public Updated_user(int id, String first_name, String last_name, String email, String contact_no, String profile_pic, ArrayList<String> roles) {
                this.id = id;
                this.first_name = first_name;
                this.last_name = last_name;
                this.email = email;
                this.contact_no = contact_no;
                this.profile_pic = profile_pic;
                this.roles = roles;
            }

            @SerializedName("id")
            int id;
            @SerializedName("first_name")
            String first_name;

            @SerializedName("last_name")
            String last_name;

            @SerializedName("email")
            String email;
            @SerializedName("contact_no")
            String contact_no;
            @SerializedName("profile_pic")
            String profile_pic;

            ArrayList<String> roles;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getContact_no() {
                return contact_no;
            }

            public void setContact_no(String contact_no) {
                this.contact_no = contact_no;
            }

            public String getProfile_pic() {
                return profile_pic;
            }

            public void setProfile_pic(String profile_pic) {
                this.profile_pic = profile_pic;
            }

            public ArrayList<String> getRoles() {
                return roles;
            }

            public void setRoles(ArrayList<String> roles) {
                this.roles = roles;
            }
        }
        @SerializedName("created_at")
        String created_at;
        @SerializedName("updated_at")
        String updated_at;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public Updated_user getUpdated_by_user() {
            return updated_by_user;
        }

        public void setUpdated_by_user(Updated_user updated_by_user) {
            this.updated_by_user = updated_by_user;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

    }
    public static class WingDetail {
        public WingDetail(int id, String name, String no_of_floors) {
            this.id = id;
            this.name = name;
            this.no_of_floors = no_of_floors;
        }

        int id;
        String name,no_of_floors;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNo_of_floors() {
            return no_of_floors;
        }

        public void setNo_of_floors(String no_of_floors) {
            this.no_of_floors = no_of_floors;
        }
    }
}

