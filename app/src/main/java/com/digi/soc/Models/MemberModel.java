package com.digi.soc.Models;

import java.util.ArrayList;

public class MemberModel {

    private ArrayList<MemberContactModel> contactModelArrayList;

    public ArrayList<MemberContactModel> getContactModelArrayList() {
        return contactModelArrayList;
    }

    public void setContactModelArrayList(ArrayList<MemberContactModel> contactModelArrayList) {
        this.contactModelArrayList = contactModelArrayList;
    }
}
