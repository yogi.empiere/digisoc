package com.digi.soc.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MemberRequestModel implements Serializable {

    @SerializedName("id")
    private int Id;

    @SerializedName("type")
    private String Type;

    @SerializedName("user")
    private User user;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static class User {

        @SerializedName("id")
        int Id;

        @SerializedName("first_name")
        String firstName;

        @SerializedName("last_name")
        String lastName;

        @SerializedName("email")
        String email;

        @SerializedName("contact_no")
        String contactNo;

        @SerializedName("profile_pic")
        String profilePic;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }
    }
  /*   "id": 1,
      "type": "member",
             "user": {
        "id": 3,
                "first_name": "Demo",
                "last_name": "Demo",
                "email": "",
                "contact_no": "1234567890",
                "profile_pic": "https://lifeatbrio.com/wp-content/uploads/2016/11/user-placeholder.jpg",
                "roles": []
    }*/
}
