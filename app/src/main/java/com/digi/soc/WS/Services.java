package com.digi.soc.WS;

import java.util.ArrayList;

public class Services {
    public static String DEVICE_ID = "";
    public static String Authorization = "Basic YWRtaW46QVBJQEtSVVBBQ0FCUyEjJFdFQiQ=";
    static String X_API_KEY = "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss";//X-API-KEY

    private static String BASE_URL = "http://18.223.123.136/";

    private static String MAIN_URL = BASE_URL + "api/v1/";

    public static String REGISTER = MAIN_URL + "register";
    public static String LOGIN = MAIN_URL + "login";
    public static String DASHBOARD = MAIN_URL + "dashboard";
    public static String WINGS = MAIN_URL + "wing/";
    public static String GET_WINGS = MAIN_URL + "wing";
    public static String NOTICE = MAIN_URL + "notice";
    public static String COMPLAIN = MAIN_URL + "complaint";
    public static String ALARM = MAIN_URL + "alarm";
    public static String DELETE_ALARM = MAIN_URL + "alarm/";
    public static String CLOSE_ALARM = MAIN_URL + "alarm/";
    public static String EVENT = MAIN_URL + "event";
    public static String DELETE_EVENT = MAIN_URL + "event/";
    public static String MEMBER_ACCEPT = MAIN_URL + "society/members/requests/accept";
    public static String MEMBER_REJECT = MAIN_URL + "society/members/requests/reject";
    public static String CREATE_FEED = MAIN_URL + "feed";
    public static String DELETE_FEED = MAIN_URL + "feed/";
    public static String CREATE_WING = MAIN_URL + "wing";
    public static String APARTMENT = MAIN_URL + "apartment";
    public static String DELETE_APPARTMENT = MAIN_URL + "apartment/";
    public static String SOCIETY_PERSON = MAIN_URL + "society/person";
    public static String GET_SOCIETY_PERSON_BY = MAIN_URL + "society/person/search?person_type=";
    public static String DELETE_SOCIETY_PERSON = MAIN_URL + "society/person";
    public static String SOCIETY_RULES = MAIN_URL + "society/rule";   // for Edit And DELETE SOCIETY_RULES also
    public static String SOCIETY_GATES = MAIN_URL + "society/entry-gate";
    public static String SOCIETY_EMERGENCY = MAIN_URL + "society/emergency-contact";
    public static String ADD_SOCIETY_GUARD = MAIN_URL + "security-guard/register";
    public static String GET_SOCIETY_GUARD = MAIN_URL + "security-guard";
    public static String SOCIETY_POLL = MAIN_URL + "society/poll";    // for  DELETE SOCIETY_POLL also
    public static String SOCIETY_POLL_ClOSED = MAIN_URL +"society/poll/closed";
    public static String SEATCH_SOCIETY_POLL_BYWING = MAIN_URL +"society/poll/search?wing_id=";
    public static String SOCIETY_POLL_VOTE_BY_ID = MAIN_URL +"society/poll/vote/";
    public static String SOCIETY_POLL_ClOSE_BY_ID = MAIN_URL +"society/poll/close";
    public static String GET_MEMBER_VEHICLE= MAIN_URL +"society/vehicle";  // for Edit And DELETE Vehicle also
    public static String CREATE_MEMBER_VEHICLE= MAIN_URL +"society/vehicle";
    public static String GET_MEMBER_VEHICLE_TYPE= MAIN_URL +"society/vehicle-types";



}
