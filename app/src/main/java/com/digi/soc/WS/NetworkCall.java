package com.digi.soc.WS;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.MySharedPref;
import com.digi.soc.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class NetworkCall {

    public Context context;
    public AppCompatActivity mActivity;
    public String url, req_type;
    public HashMap<String, File> postFiles;
    public JsonObject postDataInJsonObject;
    public MultiPartRequest request_obj;
    public Boolean isShow, isHeaderAuthForPic, isAuth;
    public ProgressDialog mProgressPopup;

    public static String REQ_GET = "GET";
    public static String REQ_POST = "POST";
    public static String REQ_PUT = "PUT";
    public static String REQ_DELETE = "DELETE";

    public NetworkCall() {
    }
    public NetworkCall(Context context, String url,
                       JsonObject postData,
                       Boolean isShow, String req_type,
                       Boolean isAuth, MultiPartRequest request) {
        this.context = context;
        mActivity = (AppCompatActivity) context;
        this.url = url;
        this.postFiles = new HashMap<>();
        this.postDataInJsonObject = postData;
        this.request_obj = request;
        this.isShow = isShow;
        this.req_type = req_type;
        this.isAuth = isAuth;

        if (isNetworkAvailable()) {
            showPopup();
            setDataJsonObject();
        } else
            Toast.makeText(context, context.getResources().getString(R.string.internet_not_connected), Toast.LENGTH_SHORT).show();
    }

    public NetworkCall(Context context, String url,
                       JsonObject postData,
                       HashMap<String, File> postFiles,
                       Boolean isShow, String req_type,
                       Boolean isAuth, MultiPartRequest request) {
        this.context = context;
        mActivity = (AppCompatActivity) context;
        this.url = url;
        this.postFiles = new HashMap<>();
        this.postDataInJsonObject = postData;
        this.request_obj = request;
        this.isShow = isShow;
        this.postFiles = postFiles;
        this.req_type = req_type;
        this.isAuth = isAuth;

        if (isNetworkAvailable()) {
            showPopup();
            setDataJsonObject();
        } else
            Toast.makeText(context, context.getResources().getString(R.string.internet_not_connected), Toast.LENGTH_SHORT).show();
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null)
            return false;
        else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (NetworkInfo anInfo : info)
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED)
                        return true;
        }
        return false;
    }

    private void setDataJsonObject() {

        Builders.Any.B ion = Ion.with(context).load(req_type, url);
        String auth = new MySharedPref(context).getString(MySharedPref.USER_AUTH, "");

        if (isAuth) {
            Log.i("Authorization===>", auth);
            Log.i("URL===>", url);

            ion.addHeader("Accept", "application/json");
            ion.addHeader("Content-Type", "application/json");
            if (!auth.equals(""))
                ion.addHeader("Authorization", "Bearer " + auth);

        } else {
            ion.addHeader("Accept", "application/json");
            ion.addHeader("Content-Type", "application/json");
        }

        List<Part> files = new ArrayList<>();
        if (postFiles != null && postFiles.size() > 0) {
            for (String key : postFiles.keySet()) {
                File val = postFiles.get(key);
                if (val != null) {
                    files.add(new FilePart(key, val));
                }
            }

            if (files.size() != 0)
                ion.addMultipartParts(files);
        }

        if (MyConstants.isResponceJsonOBJ) {
            ion.setJsonObjectBody(postDataInJsonObject)
                    .asJsonObject()
                    .withResponse()
                    .setCallback((e, result) -> {

                        Log.i("Result -->", result.toString());
                        try {

                            if (result != null) {
                                Log.e("response ==>", result.getResult() + "_");
                                Log.e("code ==>", result.getHeaders().code() + "_");
                                request_obj.myResponseMethod(result.getResult().toString(), String.valueOf(result.getHeaders().code()));
                            }

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            request_obj.myResponseMethod("", String.valueOf(result.getHeaders().code()));

                        } finally {
                            if (isShow)
                                mProgressPopup.dismiss();
                        }
                    });

        } else {
            ion.setJsonObjectBody(postDataInJsonObject)
                    .asString().setCallback((new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            Log.e("response ==>", result + "_");
                            MyConstants.isResponceJsonOBJ = true;
                        }
                    })
            );
        }
    }


    private void setDataJsonObjectWithPic() {

        Builders.Any.B ion = Ion.with(context).load(req_type, url);
        String auth = new MySharedPref(context).getString(MySharedPref.USER_AUTH, "");
        Log.e("URL===>", url);

        if (isHeaderAuthForPic) {
            ion.addHeader("Content-Type", "application/x-www-form-urlencoded");
            ion.addHeader("X-Requested-With", "XMLHttpRequest");
            if (!auth.equals(""))
                ion.addHeader("Authorization", "Bearer " + auth);
        }

        List<Part> files = new ArrayList<>();
        if (postFiles != null && postFiles.size() > 0) {
            for (String key : postFiles.keySet()) {
                File val = postFiles.get(key);
                if (val != null) {
                    files.add(new FilePart(key, val));
                    Log.e("postFiles", key + "==>" + val.getAbsolutePath());
                }
            }
            if (files.size() != 0)
                ion.addMultipartParts(files);
        }


        ion.setJsonObjectBody(postDataInJsonObject)
                .asJsonObject()
                .withResponse()
                .setCallback((e, result) -> {
                    try {

                        if (result != null) {
                            Log.e("response ==>", result.getResult() + "_");
                            Log.e("code ==>", result.getHeaders().code() + "_");
                            request_obj.myResponseMethod(result.getResult().toString(), String.valueOf(result.getHeaders().code()));
                        }

                    } catch (Exception e1) {
                        e1.printStackTrace();
                        request_obj.myResponseMethod("", String.valueOf(result.getHeaders().code()));

                    } finally {
                        if (isShow)
                            mProgressPopup.dismiss();
                    }
                });
    }

    //for Post Data
    public void callPostJSON(Context context, String url,
                             JsonObject postData,
                             Boolean isShow, String req_type,
                             Boolean isAuth, MultiPartRequest request) {

        this.context = context;
        mActivity = (AppCompatActivity) context;
        this.url = url;
        this.postFiles = new HashMap<>();
        this.postDataInJsonObject = postData;
        this.request_obj = request;
        this.isShow = isShow;
        this.req_type = req_type;
        this.isAuth = isAuth;

        if (isNetworkAvailable()) {
            showPopup();
            new AsynchPostJSON().execute("");
        } else
            Toast.makeText(context, context.getResources().getString(R.string.internet_not_connected), Toast.LENGTH_SHORT).show();


    }
   //For delete Data
   public void callDeleteJSON(Context context, String url,
                              Boolean isShow, String req_type,
                              Boolean isAuth, MultiPartRequest request) {
        this.context = context;
        mActivity = (AppCompatActivity) context;
        this.url = url;
        this.request_obj = request;
        this.isShow = isShow;
        this.req_type = req_type;
        this.isAuth = isAuth;

        if (isNetworkAvailable()) {
            showPopup();
            new AsynchDeleteJSON().execute("");
        } else
            Toast.makeText(context, context.getResources().getString(R.string.internet_not_connected), Toast.LENGTH_SHORT).show();
    }
// For PUT Data
    public void callPutJSON(Context context, String url,
                               Boolean isShow, String req_type,
                               Boolean isAuth, MultiPartRequest request) {
        this.context = context;
        mActivity = (AppCompatActivity) context;
        this.url = url;
        this.request_obj = request;
        this.isShow = isShow;
        this.req_type = req_type;
        this.isAuth = isAuth;

        if (isNetworkAvailable()) {
            showPopup();
            new AsynchPutJSON().execute("");
        } else
            Toast.makeText(context, context.getResources().getString(R.string.internet_not_connected), Toast.LENGTH_SHORT).show();
    }
    private void showPopup() {
        mProgressPopup = new ProgressDialog(mActivity);
        mProgressPopup.setCancelable(false);
        if (isShow) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressPopup.show();
                }
            });
        }
    }

    public interface MultiPartRequest {
        void myResponseMethod(String response, String requestCode);
    }

    class AsynchPostJSON extends AsyncTask<String, Void, Response> {
        protected Response doInBackground(String... urls) {
            Response response = null;

            try {
                String auth = new MySharedPref(context).getString(MySharedPref.USER_AUTH, "");
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, postDataInJsonObject.toString());
                Request request;

                if (isAuth && !auth.isEmpty()) {
                    request = new Request.Builder()
                            .url(url)
                            .method(req_type, body)
                            .addHeader("Accept", "application/json")
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Authorization", "Bearer " + auth)
                            .build();
                } else {
                    request = new Request.Builder()
                            .url(url)
                            .method(req_type, body)
                            .addHeader("Accept", "application/json")
                            .addHeader("Content-Type", "application/json")
                            .build();
                }
                response = client.newCall(request).execute();


                return response;
            } catch (IOException e) {
                e.printStackTrace();

                return response;

            }

        }
        protected void onPostExecute(Response response) {
            if(response == null){
                if (isShow)
                    mProgressPopup.dismiss();
                request_obj.myResponseMethod("", "400");
                return;
            }
            try {
                if (response.isSuccessful()) {
                    String result = response.body().string();
                    Log.i(TAG, "AsynchPostJSON Responce: " + result);
                    if (isShow)
                        mProgressPopup.dismiss();
                    request_obj.myResponseMethod(result, String.valueOf(response.code()));
                } else {
                    if (isShow)
                        mProgressPopup.dismiss();
                    request_obj.myResponseMethod("", String.valueOf(response.code()));
                }
            } catch (IOException e) {
                e.printStackTrace();

                if (isShow)
                    mProgressPopup.dismiss();
                request_obj.myResponseMethod("", String.valueOf(response.code()));

            }
        }
    }

    class AsynchDeleteJSON extends AsyncTask<String, Void, Response> {
        protected Response doInBackground(String... urls) {
            Response response = null;

            try {
                String auth = new MySharedPref(context).getString(MySharedPref.USER_AUTH, "");
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType,"deleted");
                Request request;

                if (isAuth && !auth.isEmpty()) {
                    request = new Request.Builder()
                            .url(url)
                            .method(req_type, null)
                            .addHeader("Accept", "application/json")
                            .addHeader("Authorization", "Bearer " + auth)
                            .build();
                } else {
                    request = new Request.Builder()
                            .url(url)
                            .method(req_type, null)
                            .addHeader("Accept", "application/json")
                            .build();
                }

                response = client.newCall(request).execute();


                return response;
            } catch (IOException e) {
                e.printStackTrace();

                return response;

            }

        }
        protected void onPostExecute(Response response) {
            if(response == null){
                if (isShow)
                    mProgressPopup.dismiss();
                request_obj.myResponseMethod("", "400");
                return;
            }
            try {
                if (response.isSuccessful()) {
                    String result = response.body().string();
                    Log.i(TAG, "AsynchPostJSON Responce: " + result);
                    if (isShow)
                        mProgressPopup.dismiss();
                    request_obj.myResponseMethod(result, String.valueOf(response.code()));
                } else {
                    if (isShow)
                        mProgressPopup.dismiss();
                    request_obj.myResponseMethod("", String.valueOf(response.code()));
                }
            } catch (IOException e) {
                e.printStackTrace();

                if (isShow)
                    mProgressPopup.dismiss();
                request_obj.myResponseMethod("", String.valueOf(response.code()));

            }
        }
    }

    class AsynchPutJSON extends AsyncTask<String, Void, Response> {
        protected Response doInBackground(String... urls) {
            Response response = null;

            try {
                String auth = new MySharedPref(context).getString(MySharedPref.USER_AUTH, "");
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("application/json");
                MediaType ContentType = MediaType.parse("application/x-www-form-urlencoded");
                RequestBody body = RequestBody.create(mediaType,"deleted");
                Request request;

                if (isAuth && !auth.isEmpty()) {
                    request = new Request.Builder()
                            .url(url)
                            .method(req_type, body)
                            .addHeader("Accept", "application/json")
                            .addHeader("Accept", "application/x-www-form-urlencoded")
                            .addHeader("Authorization", "Bearer " + auth)
                            .build();
                } else {
                    request = new Request.Builder()
                            .url(url)
                            .method(req_type, body)
                            .addHeader("Accept", "application/json")
                            .addHeader("Accept", "application/x-www-form-urlencoded")
                            .build();
                }

                response = client.newCall(request).execute();


                return response;
            } catch (IOException e) {
                e.printStackTrace();

                return response;

            }

        }
        protected void onPostExecute(Response response) {
            if(response == null){
                if (isShow)
                    mProgressPopup.dismiss();
                request_obj.myResponseMethod("", "400");
                return;
            }
            try {
                if (response.isSuccessful()) {
                    String result = response.body().string();
                    Log.i(TAG, "AsynchPostJSON Responce: " + result);
                    if (isShow)
                        mProgressPopup.dismiss();
                    request_obj.myResponseMethod(result, String.valueOf(response.code()));
                } else {
                    if (isShow)
                        mProgressPopup.dismiss();
                    request_obj.myResponseMethod("", String.valueOf(response.code()));
                }
            } catch (IOException e) {
                e.printStackTrace();

                if (isShow)
                    mProgressPopup.dismiss();
                request_obj.myResponseMethod("", String.valueOf(response.code()));

            }
        }
    }
}