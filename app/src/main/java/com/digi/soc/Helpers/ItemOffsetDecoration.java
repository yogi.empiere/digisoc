package com.digi.soc.Helpers;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

    private int mItemOffsetLeftRight;
    private int mItemOffsetTopBottom;

    public ItemOffsetDecoration(int itemOffsetLeftRight,int itemOffsetTopBottom) {
        mItemOffsetLeftRight = itemOffsetLeftRight;
        mItemOffsetTopBottom = itemOffsetTopBottom;
    }

    public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId,int itemOffsetId2) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId),context.getResources().getDimensionPixelSize(itemOffsetId2));
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(mItemOffsetLeftRight, mItemOffsetTopBottom, mItemOffsetLeftRight, mItemOffsetTopBottom);
    }
}
