package com.digi.soc.Helpers;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Build;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.digi.soc.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Utility {

    public static void makeTextViewResizable(int lines,Context context,final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(lines,context,Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(lines,context,Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(lines,context,Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(int lines,Context context,final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new ClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable( lines,context,tv, -1, context.getResources().getString(R.string.see_less), false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(lines,context,tv, lines, context.getResources().getString(R.string.see_more), true);
                    }
                }


                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                    ds.setColor(Color.parseColor("#2ab2fe"));
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    public static final long AVERAGE_MONTH_IN_MILLIS = DateUtils.DAY_IN_MILLIS * 30;

    public static String getRelationTime(long time) {

        String agoTime = "";

        final long now = new Date().getTime();
        final long delta = now - time;
        long resolution = -1;
        if (delta <= DateUtils.MINUTE_IN_MILLIS) {
            resolution = DateUtils.SECOND_IN_MILLIS;
        } else if (delta <= DateUtils.HOUR_IN_MILLIS) {
            resolution = DateUtils.MINUTE_IN_MILLIS;
        } else if (delta <= DateUtils.DAY_IN_MILLIS) {
            resolution = DateUtils.HOUR_IN_MILLIS;
        } else if (delta <= DateUtils.WEEK_IN_MILLIS) {
            resolution = DateUtils.DAY_IN_MILLIS;
        } /*else if (delta <= AVERAGE_MONTH_IN_MILLIS) {
            return Integer.toString((int) (delta / DateUtils.WEEK_IN_MILLIS)) + " week(s) ago";
        } else if (delta <= DateUtils.YEAR_IN_MILLIS) {
            return Integer.toString((int) (delta / AVERAGE_MONTH_IN_MILLIS)) + " month(s) ago";
        } else {
            return Integer.toString((int) (delta / DateUtils.YEAR_IN_MILLIS)) + " year(s) ago";
        }

        return DateUtils.getRelativeTimeSpanString(time, now, resolution).toString();*/

         else if (delta <= AVERAGE_MONTH_IN_MILLIS) {
            agoTime =  Integer.toString((int) (delta / DateUtils.WEEK_IN_MILLIS)) + " week(s) ago";
        } else if (delta <= DateUtils.YEAR_IN_MILLIS) {
            agoTime =  Integer.toString((int) (delta / AVERAGE_MONTH_IN_MILLIS)) + " month(s) ago";
        } else {
            agoTime =  Integer.toString((int) (delta / DateUtils.YEAR_IN_MILLIS)) + " year(s) ago";
        }

        if(resolution!=-1){
            agoTime =  DateUtils.getRelativeTimeSpanString(time, now, resolution).toString();
        }

        if(!agoTime.isEmpty()){
            String tempAgoTime = "";
            if(agoTime.contains("seconds")){
                tempAgoTime =  agoTime.replaceAll("seconds","sec");
            }if(agoTime.contains("minutes")){
                tempAgoTime =  agoTime.replaceAll("minutes","min");
            }if(agoTime.contains("hours")){
                tempAgoTime = agoTime.replaceAll("hours","hr");
            }if(agoTime.contains("week(s)")){
                tempAgoTime =  agoTime.replaceAll("week(s)","week");
            }if(agoTime.contains("month(s)")){
                tempAgoTime = agoTime.replaceAll("month(s)","month");
            }if(agoTime.contains("year(s)")){
                tempAgoTime =  agoTime.replaceAll("year(s)","yr");
            }
            return tempAgoTime;
        }

        return agoTime;
    }

    public static String agoTimeShow(String date){
        String agoTime = "";

        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'");
            Date past = format.parse(date);

            agoTime = getRelationTime(past.getTime());


           /* Date now = new Date();
            long seconds= TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes=TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days=TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if(seconds<60)
            {
                System.out.println(seconds+" seconds ago");
                agoTime = seconds+" seconds ago";
            }
            else if(minutes<60)
            {
                System.out.println(minutes+" minutes ago");
                agoTime = minutes+" minutes ago";
            }
            else if(hours<24)
            {
                System.out.println(hours+" hours ago");
                agoTime = hours+" hours ago";
            }
            else if(days < 31)
            {
                System.out.println(days+" days ago");
                agoTime = days+" days ago";
            }else if((days/30) < 12){
                System.out.println((days/30)+" months ago");
                agoTime = (days/30)+" months ago";
            }else {
                System.out.println((days/365)+" years ago");
                agoTime = (days/365)+" years ago";
            }*/
        }
        catch (Exception j){
            j.printStackTrace();
        }

        return  agoTime;
    }

    public static String eventDateFormator(String dateString){

        String tempDate = "1 JAN 2020 01:00 PM";

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss+SS");
       // format.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        try {
            Date date = format.parse(dateString);
            String temp = newFormat.format(date);

            tempDate = temp.replaceAll("a.m.","AM").replaceAll("p.m.","PM").toUpperCase();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return tempDate;
    }

    public static void showToast(Context ct, String message) {
        Toast.makeText(ct, message, Toast.LENGTH_LONG).show();

    }

    public static void showDialog(Context context,String title,String message){
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle(title);
                alertDialog.setMessage(message);
                alertDialog.setPositiveButton("ok", (dialog, which) -> dialog.dismiss());
                alertDialog.show();
            }
        });

    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);

        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }


    public static String selectedDate(Context context,Date date) {
        SimpleDateFormat formatMonth = new SimpleDateFormat("MMM");
        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
        String selectedMonth = formatMonth.format(date);
        String selectedYear = formatYear.format(date);
        String selectedDate = selectedMonth + context.getResources().getString(R.string.more) + selectedYear;
        return selectedDate;
    }

    public static String eventDateFormat(Date date){
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String stringDate = formatDate.format(date);
        return stringDate;
    }

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public static boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>=android.os.Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private static class MySpannable {
        public MySpannable(boolean b) {

        }
    }
}
