package com.digi.soc.Helpers;

import java.util.Date;
import java.util.List;

public interface OnDateSelected{
    public void onDateSelectedListener(List<Date> list);
}