package com.digi.soc.Helpers;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.digi.soc.Fragments.AddFeedFragment;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Utils {
    public static  String DateOnly = "";

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void closeKeyboard (Context context, View view) {
        InputMethodManager imm = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        ((Activity)context).getWindow().setSoftInputMode(WindowManager
                .LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static boolean isInvalidEmail(CharSequence target) {
        return target == null || !Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    public static void startCropImageActivity(Uri imageUri,Context context) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start((Activity) context);
    }
    public static void showWarn(Context context, String msg) {
        showWarn(context, msg, 3);
    }

    public static void showWarn(Context context, String msg, int type) {
        String title;
        int t;
        switch (type) {
            case 0:
                t = SweetAlertDialog.NORMAL_TYPE;
                title = "Information";
                break;
            case 1:
                t = SweetAlertDialog.ERROR_TYPE;
                title = "Error";
                break;
            case 2:
                t = SweetAlertDialog.SUCCESS_TYPE;
                title = "Success";
                break;
            default://3
                t = SweetAlertDialog.WARNING_TYPE;
                title = "Warning";
                break;
        }


        new SweetAlertDialog(context, t)
                .setTitleText(title)
                .setContentText(msg)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId (Context context) {

        return Settings.Secure.getString(context.
                getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void showAlert(String msg, final Context ctx) {
        try {
            new AlertDialog.Builder(ctx)
                    .setMessage(msg)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static String getOSVersion () {
        return Build.VERSION.RELEASE;
    }

    public static String getDeviceName () {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String handleAPIResponceCode(Context ct, String code, String result) {
        if (code.equals("200")) {
            return result;
        } else if (code.equals("201")) {
            return result;
        } else if (code.equals("400") || code.equals("500") || code.equals("404")
                || code.equals("422") || code.equals("401") || code.equals("405")) {
            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.has("message")) {
                    new SweetAlertDialog(ct, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Alert!")
                            .setContentText(jObj.getString("message"))
                            .setConfirmText("OK")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return "";


        }
        return "";

    }

    public static void hideSoftKeyboard(Activity mActivity) {
        try {
            View view = mActivity.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //Check Permisson
    public static boolean Check_STORAGE(Context context) {
        int result = ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }
    public static boolean Check_CAMERA(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

}
