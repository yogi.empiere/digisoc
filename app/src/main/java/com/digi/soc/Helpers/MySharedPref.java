package com.digi.soc.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.digi.soc.R;

public class MySharedPref {

    public static String USER_ID = "id";
    public static String USER_AUTH = "Auth";
    public static String USER_TOKEN_TYPE = "token_type";
    public static String USER_NAME = "Name";
    public static String USER_EMAIL = "Email";
    public static String USER_IMAGE = "Image";

    public String isLogin = "isLoggedIn";

    private String MY_PREF_NAME;
    private Context context;

    public MySharedPref (Context context) {
        super();
        this.context = context;
        MY_PREF_NAME = context.getResources().getString(R.string.app_name);
    }

    public String getString (final String key, final String value) {
        SharedPreferences sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0);
        return sha.getString(key, value);
    }

    public void sharedPrefClear() {
        SharedPreferences.Editor sha = context.getSharedPreferences(MY_PREF_NAME, 0).edit();
        sha.clear();
        sha.apply();
        sha.commit();
    }

    public boolean isLogin() {
        return !new MySharedPref(context)
                .getString(MySharedPref.USER_ID, "").equals("");
    }

    public void setString (final String key, final String value) {
        SharedPreferences.Editor sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0).edit();
        sha.putString(key, value);
        sha.apply();
    }

}
