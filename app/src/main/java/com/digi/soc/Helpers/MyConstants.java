package com.digi.soc.Helpers;

import com.digi.soc.Models.AlarmModel;
import com.digi.soc.Models.ComplainModel;
import com.digi.soc.Models.EmergencyModel;
import com.digi.soc.Models.EventModel;
import com.digi.soc.Models.GatesModel;
import com.digi.soc.Models.MemberRequestModel;
import com.digi.soc.Models.NoticeModel;
import com.digi.soc.Models.PollModel;
import com.digi.soc.Models.RulesModel;
import com.digi.soc.Models.SecurityModel;
import com.digi.soc.Models.StaffModel;
import com.digi.soc.Models.VehicleModel;
import com.digi.soc.Models.VehicleTypeModel;
import com.digi.soc.Models.VendorModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.Models.WorkersModel;

import java.util.ArrayList;
import java.util.List;

public class MyConstants {

    public static ArrayList<NoticeModel> noticeData = new ArrayList<>();
    public static ArrayList<ComplainModel> complainData = new ArrayList<>();
    public static ArrayList<EventModel> eventData = new ArrayList<>();
    public static ArrayList<PollModel> pollData = new ArrayList<>();
    public static ArrayList<MemberRequestModel> memberReqData = new ArrayList<>();
    public static ArrayList<WingsModel> wingReqData = new ArrayList<>();
    public static List<AlarmModel> listAlarm ;
    public static List<AlarmModel> listClosed ;
    public static ArrayList<AlarmModel> arrAlarmModel = new ArrayList<>();
    public static ArrayList<AlarmModel> arrAlarmClosed = new ArrayList<>();
    public static ArrayList<RulesModel> rulesModels = new ArrayList<>();
    public static ArrayList<GatesModel> gatesModels = new ArrayList<>();
    public static ArrayList<EmergencyModel> emergencyModels = new ArrayList<>();
    public static ArrayList<WorkersModel> workersModels = new ArrayList<>();
    public static ArrayList<VendorModel> vendorModels = new ArrayList<>();
    public static ArrayList<StaffModel> staffModels = new ArrayList<>();
    public static ArrayList<SecurityModel> securityModels = new ArrayList<>();
    public static ArrayList<VehicleTypeModel> VehicleTypeModel = new ArrayList<>();

    public static boolean isResponceJsonOBJ = true;

    public static final int REQUEST_CAMERA = 1888;
    public static final int SELECT_FILE = 2888;
    public static final int CAMERA = 101;
}
