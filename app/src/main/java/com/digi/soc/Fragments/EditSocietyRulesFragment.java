package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.digi.soc.Adapters.RulesAdapter;
import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Controls.TextLight;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class EditSocietyRulesFragment extends BaseFragment {

    @BindView(R.id.edtRule)
    EditMedium edtRule;

    @BindView(R.id.btnAdd)
    Button btnUpdate;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.tvrule)
    TextLight tvrule;

    private String rules ="";
    private int ruleId ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_society_rules, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, "Edit Society Rules", false);

        ruleId = this.getArguments().getInt("ruleId");
        btnUpdate.setText(R.string.action_update);
        tvrule.setText(R.string.edit_rule);

        editRule(ruleId);
        bindWidget();

        return view;
    }

    private void bindWidget() {
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    updateRules();
                }
            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

    }
    private void editRule( int ruleId) {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.SOCIETY_RULES + "/"+ruleId,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject society_rule = jsonObject.getJSONObject("society_rule");
                        String description = society_rule.getString("description");
                        edtRule.setText(description);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }


    private boolean checkValidation() {
        boolean isError = false;

        rules = edtRule.getText().toString().trim();
        if (TextUtils.isEmpty(rules)) {
            isError = true;
            edtRule.requestFocus();
            edtRule.setError(getResources().getString(R.string.blank_rule_title));
        }
        return !isError;
    }

    private void updateRules() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("description", rules);

        new NetworkCall().callPostJSON(getContext(), Services.SOCIETY_RULES+"/"+ruleId,
                jObj, true, NetworkCall.REQ_PUT,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                    getActivity().onBackPressed();
                                });
                                alertDialog.show();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });

    }
}
