package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.digi.soc.Adapters.GatesAdapter;
import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class EditSocietyGatesFragment extends BaseFragment {

    @BindView(R.id.edtGate)
    EditMedium edtGate;

    @BindView(R.id.btnAdd)
    Button btnUpdate;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    private String gates ="";
    private int gateId ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_society_gates, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, "Edit Gates", false);

        gateId = this.getArguments().getInt("gateId");
        btnUpdate.setText(R.string.action_update);

        editGate(gateId);
        bindWidget();

        return view;
    }

    private void bindWidget() {
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                   updateGate();
                }
            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

    }
   private void editGate(int gateId) {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.SOCIETY_GATES + "/"+gateId,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject gate_name = jsonObject.getJSONObject("entry_gate");
                        String name = gate_name.getString("name");
                        edtGate.setText(name);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void updateGate() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("name", gates);

        new NetworkCall().callPostJSON(getContext(), Services.SOCIETY_GATES+"/"+gateId,
                jObj, true, NetworkCall.REQ_PUT,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                    getActivity().onBackPressed();
                                });

                                alertDialog.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });
    }
    private boolean checkValidation() {
        boolean isError = false;

        gates = edtGate.getText().toString().trim();

        if (TextUtils.isEmpty(gates)) {
            isError = true;
            edtGate.requestFocus();
            edtGate.setError(getResources().getString(R.string.blank_gate_title));
        }
        return !isError;
    }
}
