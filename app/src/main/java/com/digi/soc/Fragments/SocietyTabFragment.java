package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Adapters.AlarmAdapter;
import com.digi.soc.Adapters.AssetsAdapter;
import com.digi.soc.Adapters.BookingAdapter;
import com.digi.soc.Adapters.ComplainAdapter;
import com.digi.soc.Adapters.EmergencyAdapter;
import com.digi.soc.Adapters.EventAdapter;
import com.digi.soc.Adapters.GatesAdapter;
import com.digi.soc.Adapters.MemberRequestAdapter;
import com.digi.soc.Adapters.NoticeAdapter;
import com.digi.soc.Adapters.PollAdapter;
import com.digi.soc.Adapters.RulesAdapter;
import com.digi.soc.Adapters.SecurityAdapter;
import com.digi.soc.Adapters.StaffAdapter;
import com.digi.soc.Adapters.VendorAdapter;
import com.digi.soc.Adapters.VisitorAdapter;
import com.digi.soc.Adapters.WingsAdapter;
import com.digi.soc.Adapters.WorkersAdapter;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.AlarmModel;
import com.digi.soc.Models.BookingModel;
import com.digi.soc.Models.ComplainModel;
import com.digi.soc.Models.EmergencyModel;
import com.digi.soc.Models.EventModel;
import com.digi.soc.Models.GatesModel;
import com.digi.soc.Models.NoticeModel;
import com.digi.soc.Models.PollModel;
import com.digi.soc.Models.RulesModel;
import com.digi.soc.Models.SecurityModel;
import com.digi.soc.Models.StaffModel;
import com.digi.soc.Models.VendorModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.Models.WorkersModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class SocietyTabFragment extends BaseFragment {

    @BindView(R.id.tabSociety)
    TabLayout tabSociety;
    @BindView(R.id.tabMain)
    TabLayout tabMain;
    @BindView(R.id.rvWing)
    RecyclerView rvWing;
    @BindView(R.id.ivAddWing)
    ImageView ivAddWing;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivSetting)
    ImageView ivSetting;
    @BindView(R.id.edtSearch)
    EditText edtSearch;
    @BindView(R.id.tvEmptyList)
    TextView tvEmptyList;
    private int selTabPos;
    private String type;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_society_tab, container, false);

        ButterKnife.bind(this, view);

        assert getArguments() != null;
        selTabPos = getArguments().getInt("selTabPos");
        type = getArguments().getString("type");

        rvWing.setHasFixedSize(true);
        rvWing.setLayoutManager(new LinearLayoutManager(getContext()));

        setupTabs();

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                }
                return false;
            }
        });
        ivSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.changeFragment(getContext(), new SocietyFragment(), true);
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ivAddWing.setOnClickListener(view1 -> {
            switch (type) {
                case "Society":
                    if (selTabPos == 0)
                        MainActivity.changeFragment(getContext(), new AddWingFragment(), true);
                    else if (selTabPos == 1)
                        MainActivity.changeFragment(getContext(), new AddRulesFragment(), true);
                    else if (selTabPos == 2)
                        MainActivity.changeFragment(getContext(), new AddEmergencyContactFragment(), true);
                    else if (selTabPos == 3)
                        MainActivity.changeFragment(getContext(), new AddGatesFragment(), true);
                    break;

                case "People":
                    if (selTabPos == 1)
                        MainActivity.changeFragment(getContext(), new AddWorkersFragment(), true);
                    if (selTabPos == 2)
                        MainActivity.changeFragment(getContext(), new AddVendorFragment(), true);
                    if (selTabPos == 3)
                        MainActivity.changeFragment(getContext(), new AddSecurityFragment(), true);
                    if (selTabPos == 4)
                        MainActivity.changeFragment(getContext(), new AddStaffFragment(), true);
                    break;

                case "Home_Notice":
                    for (WingsModel model : MyConstants.wingReqData) {
                        model.setSelect(true);
                    }
                    MainActivity.changeFragment(getContext(), new AddNoticeFragment(), true);
                    break;

                case "Home_Complain":
                    MainActivity.changeFragment(getContext(), new AddComplainFragment(), true);
                    break;

                case "Home_Event":
                    for (WingsModel model : MyConstants.wingReqData) {
                        model.setSelect(true);
                    }
                    MainActivity.changeFragment(getContext(), new AddEventFragment(), true);
                    break;
                case "Home_Polls":
                    MainActivity.changeFragment(getContext(), new AddPollFragment(), true);
                    break;
                case "Alarm":
                    MainActivity.changeFragment(getContext(), new AddAlarmFragment(), true);
                    break;

            }
        });

        return view;
    }

    /*private void getDashboardData(int listFor) {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getActivity(), Services.DASHBOARD, jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String code) {
                try {
                    String returnResult = Utils.handleAPIResponceCode(getActivity(), code, response);
                    if (returnResult.isEmpty()) {
                        return;
                    }
                    JSONObject Obj = new JSONObject(response);

                    switch (listFor) {
                        case 1: //for Notice Fragment
                            JSONObject noticeObj = Obj.getJSONObject("notices");
                            JSONArray noticeDataList = noticeObj.getJSONArray("list");
                            MyConstants.noticeData = (ArrayList<NoticeModel>) new Gson().fromJson(String.valueOf(noticeDataList), new TypeToken<ArrayList<NoticeModel>>() {
                            }.getType());
                            setNoticeList();
                            break;
                            case 2: //for Complain Fragment
                             JSONObject complainObj = Obj.getJSONObject("complaints");
                             JSONArray complainDataList = complainObj.getJSONArray("list");
                             MyConstants.complainData = (ArrayList<ComplainModel>) new Gson().fromJson(String.valueOf(complainDataList), new TypeToken<ArrayList<ComplainModel>>() {
                             }.getType());
                                setComplainList();
                            break;

                        case 3://for wings list
                            Gson gson = new Gson();
                            Type type = new TypeToken<List<WingsModel>>() {}.getType();
                            List<WingsModel> list = gson.fromJson(Obj.getJSONObject("wings")
                                    .getJSONArray("list").toString(), type);
                            MyConstants.wingReqData.addAll(list);


                            break;

                        case 4://for Event list
                            JSONObject eventsObj = Obj.getJSONObject("events");
                            JSONArray eventsDataList = eventsObj.getJSONArray("list");
                            MyConstants.eventData = (ArrayList<EventModel>) new Gson().fromJson(String.valueOf(eventsDataList), new TypeToken<ArrayList<EventModel>>() {
                            }.getType());
                            setEventList();
                            break;

                        case 5://for active alarm
                            Gson gsonAlarm = new Gson();
                            Type typeAlarm = new TypeToken<List<AlarmModel>>() {
                            }.getType();
                            List<AlarmModel> listAlarm = gsonAlarm.fromJson(Obj
                                    .getJSONObject("alarms").getJSONObject("active")
                                    .getJSONArray("list").toString(), typeAlarm);

                            ArrayList<AlarmModel> arrAlarmModel = new ArrayList<>();
                            arrAlarmModel.addAll(listAlarm);

                            if (arrAlarmModel.size() == 0) {
                                tvEmptyList.setVisibility(View.VISIBLE);
                                rvWing.setVisibility(View.GONE);
                            } else {
                                tvEmptyList.setVisibility(View.GONE);
                                rvWing.setVisibility(View.VISIBLE);
                            }
                            AlarmAdapter alarmAdapter = new AlarmAdapter(getContext(), arrAlarmModel,1);
                            rvWing.setAdapter(alarmAdapter);

                            break;

                        case 6://for closed alarm
                            Gson gsonClosed = new Gson();
                            Type typeClosed = new TypeToken<List<AlarmModel>>() {
                            }.getType();
                            List<AlarmModel> listClosed =
                                    gsonClosed.fromJson(Obj.getJSONObject("alarms")
                                            .getJSONObject("closed").getJSONArray("list")
                                            .toString(), typeClosed);

                            ArrayList<AlarmModel> arrAlarmClosed = new ArrayList<>();
                            arrAlarmClosed.addAll(listClosed);

                            if (arrAlarmClosed.size() == 0) {
                                tvEmptyList.setVisibility(View.VISIBLE);
                                rvWing.setVisibility(View.GONE);
                            } else {
                                tvEmptyList.setVisibility(View.GONE);
                                rvWing.setVisibility(View.VISIBLE);
                            }

                            AlarmAdapter alarmClosedAdapter = new AlarmAdapter(getContext(), arrAlarmClosed,0);
                            rvWing.setAdapter(alarmClosedAdapter);

                            break;
                        case 7://for member request
                            JSONObject member_reqObj = Obj.getJSONObject("member_req");
                            JSONArray member_reqDataList = member_reqObj.getJSONArray("list");

                            MyConstants.memberReqData = (ArrayList<MemberRequestModel>)
                                    new Gson().fromJson(String.valueOf(member_reqDataList),
                                            new TypeToken<ArrayList<MemberRequestModel>>() {
                                            }.getType());

                            setMemberRequestList();
                            break;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
*/
    private void setNoticeTabs() {
        tabSociety.addTab(tabSociety.newTab().setText("All"));

        setWingsTabsData();

        tabSociety.getTabAt(selTabPos).select();

        setNoticeTabData(selTabPos);
    }

    private void setNoticeTabData(int pos) {
        //getDashboardData(1);
        // getNoticeList();
        setNoticeList();
    }

    /*private void getNoticeList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getActivity(), Services.NOTICE, jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String code) {
                try {
                    String returnResult = Utils.handleAPIResponceCode(getActivity(), code, response);
                    if (returnResult.isEmpty()) {
                        return;
                    }
                    JSONObject Obj = new JSONObject(response);
                    JSONArray noticeDataList = Obj.getJSONArray("list");
                    MyConstants.noticeData = (ArrayList<NoticeModel>) new Gson().
                            fromJson(String.valueOf(noticeDataList), new TypeToken<ArrayList<NoticeModel>>() {
                    }.getType());
                    setNoticeList();
                }
                catch (Exception e) {
                e.printStackTrace();
            }
        }
    }); }*/

/*    private void getComplainList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getActivity(), Services.COMPLAIN, jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String code) {
                try {
                    String returnResult = Utils.handleAPIResponceCode(getActivity(), code, response);
                    if (returnResult.isEmpty()) {
                        return;
                    }
                    JSONObject Obj = new JSONObject(response);
                    JSONArray complainDataList = Obj.getJSONArray("list");
                    MyConstants.complainData = (ArrayList<ComplainModel>) new Gson().fromJson(String.valueOf(complainDataList), new TypeToken<ArrayList<ComplainModel>>() {
                    }.getType());
                    setComplainList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getEventList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getActivity(), Services.EVENT, jObj,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String code) {
                try {
                    String returnResult = Utils.handleAPIResponceCode(getActivity(), code, response);
                    if (returnResult.isEmpty()) {
                        return;
                    }
                    JSONObject Obj = new JSONObject(response);
                    JSONArray eventsDataList = Obj.getJSONArray("list");
                    MyConstants.eventData = (ArrayList<EventModel>) new Gson().fromJson(String.valueOf(eventsDataList),
                            new TypeToken<ArrayList<EventModel>>() {
                            }.getType());
                    setEventList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }*/

    private void setComplainTabData(int pos) {
        //  getComplainList();
        // getDashboardData(2);
        setComplainList();
    }

    private void setPeopleTabData(int pos) {
        switch (pos) {
            case 0:
                break;
            case 1:
                setWorkersList();
                break;
            case 2:
                setVendorList();
                break;
            case 3:
                setSecurityList();
                break;
            case 4:
                setStaffList();
        }

    }

    private void setVisitorTabData(int pos) {
        setVisitorList();
    }

    private void setVisitorList() {
        VisitorAdapter visitorAdapter = new VisitorAdapter(getContext(), new ArrayList<>());
        rvWing.setAdapter(visitorAdapter);
    }

    private void setMemberRequestTabs() {
        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();

        setMemberRequestTabsData(selTabPos);
    }

    private void setWingsTabsData() {
        //getDashboardData(3);
        for (WingsModel model : MyConstants.wingReqData) {
            tabSociety.addTab(tabSociety.newTab().setText(model.getName()));
        }
        tabSociety.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                for (int i = 0; i < MyConstants.wingReqData.size(); i++) {
                    if (tab.getPosition() == 0) {
                        switch (type) {
                            case "Home_Notice":
                                setNoticeList();
                                break;
                            case "Home_Complain":
                                setComplainList();
                                break;
                            case "Home_Event":
                                setEventList();
                                break;
                            case "Alarm":
                                break;
                            case "Home_Polls":
                                break;
                        }
                    } else if (tab.getPosition() == i + 1) {
                        sortDatalist(MyConstants.wingReqData.get(i).getId());
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void sortDatalist(int id) {
        switch (type) {
            case "Home_Notice":
                sortNoticeListByWing(id);
                break;
            case "Home_Complain":
                sortComplainListByWing(id);
                break;
            case "Home_Event":
                sortEventListByWing(id);
                break;
            case "Alarm":
                sortAlarmListByWing(id);
                break;
            case "Home_Polls":
                sortPollListByWing(id);
                break;
        }
    }

    private void sortPollListByWing(int id) {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.SEATCH_SOCIETY_POLL_BYWING + id,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray pollDataList = jsonObject.getJSONArray("list");

                        ArrayList<PollModel> pollModels = (ArrayList<PollModel>)
                                new Gson().fromJson(String.valueOf(pollDataList),
                                        new TypeToken<ArrayList<PollModel>>() {
                                        }.getType());

                        if (pollModels.size() == 0) {
                            tvEmptyList.setVisibility(View.VISIBLE);
                            rvWing.setVisibility(View.GONE);
                        } else {
                            tvEmptyList.setVisibility(View.GONE);
                            rvWing.setVisibility(View.VISIBLE);
                        }

                        PollAdapter pollAdapter = new PollAdapter(getContext(), pollModels, 0);
                        rvWing.setAdapter(pollAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void sortAlarmListByWing(int id) {
        ArrayList<AlarmModel> alarmData = new ArrayList<>();
        for (int i = 0; i < MyConstants.arrAlarmModel.size(); i++) {
            AlarmModel alarmModel = MyConstants.arrAlarmModel.get(i);

            if (id == alarmModel.getApartment().getWing().getId()) {
                if (!alarmData.contains(alarmModel)) {
                    alarmData.add(alarmModel);
                }
            }
            if (i == MyConstants.arrAlarmModel.size() - 1) {
                AlarmAdapter alarmAdapter = new AlarmAdapter(getContext(), alarmData, 1);
                rvWing.setAdapter(alarmAdapter);
            }
        }
    }

    private void sortEventListByWing(int id) {
        ArrayList<EventModel> eventData = new ArrayList<>();
        for (int i = 0; i < MyConstants.eventData.size(); i++) {
            EventModel eventModel = MyConstants.eventData.get(i);
            for (int j = 0; j < eventModel.getAttendees().size(); j++) {
                EventModel.Attendees wingsModel = eventModel.getAttendees().get(j);
                if (id == wingsModel.getId()) {
                    if (!eventData.contains(eventModel)) {
                        eventData.add(eventModel);
                    }
                }
                if (i == MyConstants.eventData.size() - 1) {
                    EventAdapter eventAdapter = new EventAdapter(getChildFragmentManager(), getContext(), eventData);
                    rvWing.setAdapter(eventAdapter);
                }
            }
        }
    }

    private void sortComplainListByWing(int id) {
        ArrayList<ComplainModel> complainData = new ArrayList<>();
        for (int i = 0; i < MyConstants.complainData.size(); i++) {
            ComplainModel complainModel = MyConstants.complainData.get(i);

            if (id == complainModel.getWingDetail().getId()) {
                if (!complainData.contains(complainModel)) {
                    complainData.add(complainModel);
                }
            }
            if (i == MyConstants.complainData.size() - 1) {
                ComplainAdapter complainAdapter = new ComplainAdapter(getContext(), complainData);
                rvWing.setAdapter(complainAdapter);
            }
        }
    }

    private void sortNoticeListByWing(int id) {
        ArrayList<NoticeModel> noticeData = new ArrayList<>();
        for (int i = 0; i < MyConstants.noticeData.size(); i++) {
            NoticeModel noticeModel = MyConstants.noticeData.get(i);
            for (int j = 0; j < noticeModel.getWings().size(); j++) {
                WingsModel wingsModel = noticeModel.getWings().get(j);
                if (id == wingsModel.getId()) {
                    if (!noticeData.contains(noticeModel)) {
                        noticeData.add(noticeModel);
                    }
                }
                if (i == MyConstants.noticeData.size() - 1) {
                    NoticeAdapter noticeAdapter = new NoticeAdapter(getContext(), noticeData);
                    rvWing.setAdapter(noticeAdapter);
                }
            }
        }

    }

    private void setPollsTabs() {
        tabMain.addTab(tabMain.newTab().setText("Active"));
        tabMain.addTab(tabMain.newTab().setText("Close"));

        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();
        setPollsTabsData(selTabPos);
        tabMain.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setPollsTabsData(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setBookingTabs() {
        tabMain.addTab(tabMain.newTab().setText("Active"));
        tabMain.addTab(tabMain.newTab().setText("Closed"));

        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();

        tabSociety.getTabAt(selTabPos).select();

        setBookingData(selTabPos);
    }

    private void setBookingData(int pos) {
        setBookingList();
    }

    private void setBookingList() {
        ArrayList<BookingModel> datalists = new ArrayList<>();
        BookingModel bookingModel = new BookingModel();

        for (int i = 0; i < 5; i++) {
            bookingModel.setChair("Chair");
            bookingModel.setQnt("Qnt - 1");
            datalists.add(bookingModel);
        }

        BookingAdapter bookingAdapter = new BookingAdapter(getContext(), datalists);
        rvWing.setAdapter(bookingAdapter);
    }

    private void setAlarm() {
        tabMain.addTab(tabMain.newTab().setText("Active"));
        tabMain.addTab(tabMain.newTab().setText("Closed"));
        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();

        setAlarmData(selTabPos);

        tabMain.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setAlarmData(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setAlarmData(int pos) {
        if (pos == 0) { //getDashboardData(5);
            if (MyConstants.arrAlarmModel.size() == 0) {
                tvEmptyList.setVisibility(View.VISIBLE);
                rvWing.setVisibility(View.GONE);
            } else {
                tvEmptyList.setVisibility(View.GONE);
                rvWing.setVisibility(View.VISIBLE);
            }
            AlarmAdapter alarmAdapter = new AlarmAdapter(getContext(), MyConstants.arrAlarmModel, 1);
            rvWing.setAdapter(alarmAdapter);
        } else
        //  getDashboardData(6);
        {
            if (MyConstants.arrAlarmClosed.size() == 0) {
                tvEmptyList.setVisibility(View.VISIBLE);
                rvWing.setVisibility(View.GONE);
            } else {
                tvEmptyList.setVisibility(View.GONE);
                rvWing.setVisibility(View.VISIBLE);
            }

            AlarmAdapter alarmClosedAdapter = new AlarmAdapter(getContext(), MyConstants.arrAlarmClosed, 0);
            rvWing.setAdapter(alarmClosedAdapter);
        }
    }

    private void setPollsTabsData(int pos) {
        if (pos == 0) {
            getActivePollList();
        } else if (pos == 1) {
            getClosePollList();
        }
    }

    private void getActivePollList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.SOCIETY_POLL,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray pollDataList = jsonObject.getJSONArray("list");

                        MyConstants.pollData = (ArrayList<PollModel>)
                                new Gson().fromJson(String.valueOf(pollDataList),
                                        new TypeToken<ArrayList<PollModel>>() {
                                        }.getType());

                        if (MyConstants.pollData.size() == 0) {
                            tvEmptyList.setVisibility(View.VISIBLE);
                            rvWing.setVisibility(View.GONE);
                        } else {
                            tvEmptyList.setVisibility(View.GONE);
                            rvWing.setVisibility(View.VISIBLE);
                        }

                        PollAdapter pollAdapter = new PollAdapter(getContext(), MyConstants.pollData, 0);
                        rvWing.setAdapter(pollAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void getClosePollList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.SOCIETY_POLL_ClOSED,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray pollDataList = jsonObject.getJSONArray("list");

                        MyConstants.pollData = (ArrayList<PollModel>)
                                new Gson().fromJson(String.valueOf(pollDataList),
                                        new TypeToken<ArrayList<PollModel>>() {
                                        }.getType());

                        if (MyConstants.pollData.size() == 0) {
                            tvEmptyList.setVisibility(View.VISIBLE);
                            rvWing.setVisibility(View.GONE);
                        } else {
                            tvEmptyList.setVisibility(View.GONE);
                            rvWing.setVisibility(View.VISIBLE);
                        }
                        PollAdapter pollAdapter = new PollAdapter(getContext(), MyConstants.pollData, 1);
                        rvWing.setAdapter(pollAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setMemberRequestTabsData(int pos) {
        // setMemberRequestList();
        //   getDashboardData(7);
        setMemberRequestList();
    }

    private void setMemberRequestList() {
        MemberRequestAdapter memberRequestAdapter = new MemberRequestAdapter(getChildFragmentManager(), getContext(), MyConstants.memberReqData);
        rvWing.setAdapter(memberRequestAdapter);
    }

    private void setEventList() {
        EventAdapter eventAdapter = new EventAdapter(getChildFragmentManager(), getContext(), MyConstants.eventData);
        rvWing.setAdapter(eventAdapter);
    }

    private void setEventTabData(int pos) {
        // getEventList();
        // getDashboardData(4);
        setEventList();
    }

    private void setNoticeList() {
        NoticeAdapter noticeAdapter = new NoticeAdapter(getContext(), MyConstants.noticeData);
        rvWing.setAdapter(noticeAdapter);
    }

    private void setComplainList() {
        ComplainAdapter complainAdapter = new ComplainAdapter(getContext(), MyConstants.complainData);
        rvWing.setAdapter(complainAdapter);
    }

    private void setupTabs() {
        switch (type) {
            case "Society":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "Society", true);
                MainActivity.setBottomPos(getContext(), -1);
                setSocietyTabs();
                break;
            case "Home_Notice":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "Notice", true);
                MainActivity.setBottomPos(getContext(), -1);
                setNoticeTabs();
                break;
            case "Home_Complain":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "Complain", true);
                MainActivity.setBottomPos(getContext(), -1);
                setComplainTabs();
                break;
            case "People":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "People", true);
                MainActivity.setBottomPos(getContext(), -1);
                setPeopleTabs();
                break;
            case "Visitor":
                ivAddWing.setVisibility(View.GONE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search by name");
                setTop(false, "Visitor", true);
                MainActivity.setBottomPos(getContext(), 3);
                setVisitorTabs();
                break;
            case "Home_Event":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "Event", true);
                MainActivity.setBottomPos(getContext(), -1);
                setEventTabs();
                break;
            case "Home_Member_Request":
                ivAddWing.setVisibility(View.GONE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by name");
                setTop(false, "Member Request", true);
                MainActivity.setBottomPos(getContext(), -1);
                setMemberRequestTabs();
                break;
            case "Home_Polls":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search wing by name");
                setTop(false, "Polls", true);
                MainActivity.setBottomPos(getContext(), -1);
                setPollsTabs();
                break;
            case "Home_Booking":
                ivAddWing.setVisibility(View.GONE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search wing by name");
                setTop(false, "Booking", true);
                MainActivity.setBottomPos(getContext(), -1);
                setBookingTabs();
                break;
            case "Alarm":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search by name");
                setTop(false, "Alarm", true);
                MainActivity.setBottomPos(getContext(), -1);
                setAlarm();
                break;
        }

        setTabLister();
    }

    private void setVisitorTabs() {

        tabMain.addTab(tabMain.newTab().setText("All"));
        tabMain.addTab(tabMain.newTab().setText("My"));

        tabSociety.addTab(tabSociety.newTab().setText("All"));

        setWingsTabsData();

        tabSociety.getTabAt(selTabPos).select();

        tabMain.getTabAt(0).select();

        setVisitorTabData(selTabPos);

        setTabMainLister();
    }

    private void setEventTabs() {

        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();

        setEventTabData(selTabPos);

    }

    private void setComplainTabs() {
        tabMain.addTab(tabMain.newTab().setText("Pending"));
        tabMain.addTab(tabMain.newTab().setText("In Progress"));
        tabMain.addTab(tabMain.newTab().setText("Resolved"));
        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();
        tabMain.getTabAt(0).select();
        setComplainTabData(selTabPos);
        setTabMainLister();
    }

    private void setSocietyTabs() {
        tabSociety.addTab(tabSociety.newTab().setText("Wings"));
        tabSociety.addTab(tabSociety.newTab().setText("Rules"));
        tabSociety.addTab(tabSociety.newTab().setText("Emergency Contact"));
        tabSociety.addTab(tabSociety.newTab().setText("Entry Gates"));
        tabSociety.getTabAt(selTabPos).select();
        setSocietyTabData(selTabPos);

    }


    private void setPeopleTabs() {
        tabSociety.addTab(tabSociety.newTab().setText("Member"));
        tabSociety.addTab(tabSociety.newTab().setText("Worker"));
        tabSociety.addTab(tabSociety.newTab().setText("Vendor"));
        tabSociety.addTab(tabSociety.newTab().setText("Security"));
        tabSociety.addTab(tabSociety.newTab().setText("Staff"));

        tabSociety.getTabAt(selTabPos).select();

        setPeopleTabData(selTabPos);
    }

    private void setTabLister() {
        tabSociety.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selTabPos = tab.getPosition();
                switch (type) {
                    case "Society":
                        setSocietyTabData(selTabPos);
                        break;
                    case "Home_Notice":
                        setNoticeTabData(selTabPos);
                        break;
                    case "People":
                        setPeopleTabData(selTabPos);
                        break;
                    case "Visitor":
                        setVisitorTabData(selTabPos);
                        break;
                    case "Home_Complain":
                        setComplainTabData(selTabPos);
                        break;
                    case "Home_Event":
                        setEventTabData(selTabPos);
                        break;
                    case "Home_Member_Request":
                        setMemberRequestTabsData(selTabPos);
                        break;
                    case "Alarm":
                        setAlarmData(selTabPos);
                        break;
                    case "Home_Booking":
                        setBookingData(selTabPos);
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setTabMainLister() {
        tabMain.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selTabPos = tab.getPosition();
                switch (type) {
                    case "Visitor":

                        break;
                    case "":

                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setSocietyTabData(int pos) {
        switch (pos) {
            case 0:
                setWings();
                break;
            case 1:
                setRules();
                break;
            case 2:
                setEmergency();
                break;
            case 3:
                setGates();
                break;

        }


    }

    private void setWings() {
        JsonObject jObj = new JsonObject();

        new NetworkCall(getContext(), Services.DASHBOARD,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", requestCode);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        Gson gson = new Gson();
                        Type type = new TypeToken<List<WingsModel>>() {
                        }.getType();
                        List<WingsModel> list = gson.fromJson(jsonObject.getJSONObject("wings").
                                getJSONArray("list").toString(), type);

                        ArrayList<WingsModel> arrModel = new ArrayList<>();
                        ArrayList<WingsModel> arrTmpModel = new ArrayList<>();
                        arrModel.addAll(list);
                        arrTmpModel.addAll(list);

                        getBlocksForWings(arrModel, arrTmpModel);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                } else {
                    WingsAdapter wingsAdapter = new WingsAdapter(getContext(), new ArrayList<>());
                    rvWing.setAdapter(wingsAdapter);
                }
            }
        });
    }

    private void getBlocksForWings(ArrayList<WingsModel> arrModel, ArrayList<WingsModel> arrTmpModel) {

        rvWing.setHasFixedSize(true);
        rvWing.setLayoutManager(new LinearLayoutManager(getContext()));
        rvWing.setNestedScrollingEnabled(false);

        WingsAdapter wingsAdapter = new WingsAdapter(getContext(), arrModel);
        rvWing.setAdapter(wingsAdapter);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                arrModel.clear();
                arrModel.addAll(findWingsByName(s.toString(), arrTmpModel));
                wingsAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        for (int i = 0; i < arrModel.size(); i++) {

            JsonObject jObj = new JsonObject();

            int finalI = i;
            new NetworkCall(getContext(), Services.WINGS + arrModel.get(i).getId(),
                    jObj, true, NetworkCall.REQ_GET,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response, String requestCode) {
                    String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                    Log.e("codeWing-->", returnResult);
                    if (requestCode.equals("200")) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            Gson gson = new Gson();
                            Type type = new TypeToken<List<WingsModel.FloorsModel>>() {
                            }.getType();
                            List<WingsModel.FloorsModel> list = gson.fromJson(jsonObject.getJSONObject("wing").
                                    getJSONArray("floors").toString(), type);
                            ArrayList<WingsModel.FloorsModel> arrFloors = new ArrayList<>();
                            arrFloors.addAll(list);

                            arrModel.get(finalI).setFloors(arrFloors);
                            arrTmpModel.get(finalI).setFloors(arrFloors);
                            wingsAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
        }
    }

    public ArrayList<WingsModel> findWingsByName(String name, ArrayList<WingsModel> listArray) {
        ArrayList<WingsModel> matches = new ArrayList<WingsModel>();
        // go through list of members and compare name with given name
        for (WingsModel list : listArray) {
            if (list.getName().toLowerCase().contains(name.toLowerCase())) {
                matches.add(list); // adds matching member to the return list
            }
        }
        return matches; // return the matches, which is empty when no member with the given name was found
    }

    private void setAssets() {
        AssetsAdapter assetsAdapter = new AssetsAdapter(getContext(), new ArrayList<>());
        rvWing.setAdapter(assetsAdapter);
    }

    private void setRules() {
        JsonObject jObj = new JsonObject();

        new NetworkCall(getContext(), Services.SOCIETY_RULES,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray rulesDataList = jsonObject.getJSONArray("list");

                        MyConstants.rulesModels = (ArrayList<RulesModel>)
                                new Gson().fromJson(String.valueOf(rulesDataList),
                                        new TypeToken<ArrayList<RulesModel>>() {
                                        }.getType());
                        Collections.reverse(MyConstants.rulesModels);
                        RulesAdapter rulesAdapter = new RulesAdapter(getContext(), MyConstants.rulesModels);
                        rvWing.setAdapter(rulesAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setEmergency() {
        JsonObject jObj = new JsonObject();

        new NetworkCall(getContext(), Services.SOCIETY_EMERGENCY,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray emergencyDataList = jsonObject.getJSONArray("list");

                        MyConstants.emergencyModels = (ArrayList<EmergencyModel>)
                                new Gson().fromJson(String.valueOf(emergencyDataList),
                                        new TypeToken<ArrayList<EmergencyModel>>() {
                                        }.getType());
                        Collections.reverse(MyConstants.emergencyModels);
                        EmergencyAdapter emergencyAdapter = new EmergencyAdapter(getContext(), MyConstants.emergencyModels);
                        rvWing.setAdapter(emergencyAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setGates() {
        JsonObject jObj = new JsonObject();

        new NetworkCall(getContext(), Services.SOCIETY_GATES,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray rulesDataList = jsonObject.getJSONArray("list");

                        MyConstants.gatesModels = (ArrayList<GatesModel>)
                                new Gson().fromJson(String.valueOf(rulesDataList),
                                        new TypeToken<ArrayList<GatesModel>>() {
                                        }.getType());
                        Collections.reverse(MyConstants.gatesModels);
                        GatesAdapter gatesAdapter = new GatesAdapter(getContext(), MyConstants.gatesModels);
                        rvWing.setAdapter(gatesAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setWorkersList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.GET_SOCIETY_PERSON_BY + "WORKER",
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray workersDataList = jsonObject.getJSONArray("list");

                        MyConstants.workersModels = (ArrayList<WorkersModel>)
                                new Gson().fromJson(String.valueOf(workersDataList),
                                        new TypeToken<ArrayList<WorkersModel>>() {
                                        }.getType());
                        Collections.reverse(MyConstants.workersModels);
                        WorkersAdapter workersAdapter = new WorkersAdapter(getContext(), MyConstants.workersModels);
                        rvWing.setAdapter(workersAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setVendorList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.GET_SOCIETY_PERSON_BY + "VENDOR",
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray vendorDataList = jsonObject.getJSONArray("list");


                        MyConstants.vendorModels = (ArrayList<VendorModel>)
                                new Gson().fromJson(String.valueOf(vendorDataList),
                                        new TypeToken<ArrayList<VendorModel>>() {
                                        }.getType());
                        Collections.reverse(MyConstants.vendorModels);
                        VendorAdapter vendorAdapter = new VendorAdapter(getContext(), MyConstants.vendorModels);
                        rvWing.setAdapter(vendorAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setStaffList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.GET_SOCIETY_PERSON_BY + "STAFF",
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray staffDataList = jsonObject.getJSONArray("list");

                        MyConstants.staffModels = (ArrayList<StaffModel>)
                                new Gson().fromJson(String.valueOf(staffDataList),
                                        new TypeToken<ArrayList<StaffModel>>() {
                                        }.getType());
                        Collections.reverse(MyConstants.staffModels);
                        StaffAdapter vendorAdapter = new StaffAdapter(getContext(), MyConstants.staffModels);
                        rvWing.setAdapter(vendorAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setSecurityList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.GET_SOCIETY_GUARD,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray securityDataList = jsonObject.getJSONArray("security_guards");

                        MyConstants.securityModels = (ArrayList<SecurityModel>)
                                new Gson().fromJson(String.valueOf(securityDataList),
                                        new TypeToken<ArrayList<SecurityModel>>() {
                                        }.getType());
                        Collections.reverse(MyConstants.securityModels);
                        SecurityAdapter securityAdapter = new SecurityAdapter(getContext(), MyConstants.securityModels);
                        rvWing.setAdapter(securityAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }
}
