package com.digi.soc.Fragments.BottomSheetDialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digi.soc.Helpers.ItemOffsetDecoration;
import com.digi.soc.Models.EventModel;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttendeesDialogFragment extends DialogFragment {

    ArrayList<EventModel.Attendees> arrAttendees;

    public AttendeesDialogFragment(ArrayList<EventModel.Attendees> arrAttendees) {
        this.arrAttendees = arrAttendees;
    }

    @BindView(R.id.rvAttendees)
    RecyclerView mRvAttendees;

    @BindView(R.id.rlClose)
    RelativeLayout mRlClose;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attendees_dialog, container, false);

        ButterKnife.bind(this,view);

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen._7sdp, R.dimen._5sdp);
        mRvAttendees.addItemDecoration(itemDecoration);

        mRvAttendees.setHasFixedSize(true);
        mRvAttendees.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mRvAttendees.setItemViewCacheSize(2);
        AttendeesAdapter adapter = new AttendeesAdapter(arrAttendees);
        mRvAttendees.setAdapter(adapter);

        mRlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public class AttendeesAdapter extends RecyclerView.Adapter<AttendeesAdapter.ViewHolder>{

        ArrayList<EventModel.Attendees> arrAttendee;

        public AttendeesAdapter(ArrayList<EventModel.Attendees> arrAttendee) {
            this.arrAttendee = arrAttendee;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_attendees,parent,false);
            return new AttendeesAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            EventModel.Attendees model = arrAttendee.get(position);
            holder.checkWing.setText(model.getName());
        }

        @Override
        public int getItemCount() {
            return arrAttendee.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.checkWing)
            TextView checkWing;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this,itemView);
            }
        }
    }
}
