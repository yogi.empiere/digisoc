package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.digi.soc.Helpers.JsonParserUniversal;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class EditWorkersFragment extends BaseFragment {

    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.edtContact)
    EditText edtContact;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.spinner_wing)
    Spinner spinner_wing;

    private String name="";
    private String no="";
    private String workerId,workerName,contact_no,wingId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_people_workers, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);
        btnAdd.setText(R.string.action_update);
        spinner_wing.setVisibility(View.GONE);

        setTop(false, "Edit Workers", false);

        workerId = this.getArguments().getString("workerId");
        workerName = this.getArguments().getString("workerName");
        contact_no = this.getArguments().getString("contact_no");
        wingId = this.getArguments().getString("wingId");

        bindWidget();

        return view;
    }

      private void bindWidget() {
        edtName.setText(workerName);
        edtContact.setText(contact_no);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    updateWorkers();
                }

            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

    }
    private void updateWorkers() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;

       jObj.addProperty("type", "WORKER");
       jObj.addProperty("name", name);
       jObj.addProperty("contact_no", no);
       jObj.addProperty("wing_id", wingId);

        Log.e("code-->", workerId);
        new NetworkCall().callPostJSON(getActivity(), Services.SOCIETY_PERSON+"/"+workerId,
                jObj, true, NetworkCall.REQ_PUT,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", returnResult);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                    //edtRule.getText().clear();
                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });
    }

   private boolean checkValidation() {
        boolean isError = false;

      name = edtName.getText().toString().trim();
      no = edtContact.getText().toString().trim();

       if (TextUtils.isEmpty(name)) {
            isError = true;
            edtName.requestFocus();
            edtName.setError(getResources().getString(R.string.blank_worker_name));
        }
       else if (TextUtils.isEmpty(no)) {
           isError = true;
           edtContact.requestFocus();
           edtContact.setError(getResources().getString(R.string.blank_contact));
       }
        return !isError;
    }

}
