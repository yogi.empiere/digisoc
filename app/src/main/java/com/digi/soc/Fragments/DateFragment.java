package com.digi.soc.Fragments;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.digi.soc.Helpers.OnDateSelected;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.R;
import com.savvi.rangedatepicker.CalendarPickerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DateFragment extends BaseFragment {

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.btnSelect)
    Button btnSelect;

    @BindView(R.id.tvStartTime)
    TextView tvStartTime;

    @BindView(R.id.tvEndTime)
    TextView tvEndTime;

    @BindView(R.id.calendar_view)
    CalendarPickerView calendar;

    int clickcount = 0;

    public Date tempStartDate;
    private OnDateSelected onDateSelected;
    private ArrayList<Date> dates;
    String date;

    public DateFragment(OnDateSelected onDateSelected,ArrayList<Date> dates) {
        this.onDateSelected = onDateSelected;
        this.dates = dates;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_date, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);
        setTop(false, "Add Event", false);

        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 10);

        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -10);


        calendar.init(new Date(), nextYear.getTime(),
                new SimpleDateFormat("MMMM, yyyy", Locale.getDefault()))
                .inMode(CalendarPickerView.SelectionMode.RANGE);

        calendar.scrollToDate(new Date());

        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                Date c = Calendar.getInstance().getTime();
                System.out.println("Current time => " + c);

                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                String formattedDate = df.format(c);

                if (clickcount == 1) {
                    if (date.getTime() < tempStartDate.getTime()) {
                        Utility.showToast(getActivity(), "End date must be greater then start date");
                        tvStartTime.setText(" ");
                        tvEndTime.setText(" ");
                        return;
                    }
                    getTime(date);

                } else {
                    tempStartDate = date;
                    getTime(date);
                }

            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(startDate == null || endDate == null)
                {
                    Utils.showAlert("Please Select Date",getContext());
                }else {
                    SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy  hh.mm aa");
                    Date newStartDate = null;
                    try {
                        newStartDate = format.parse(startDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Date newEndDate = null;
                    try {
                        newEndDate = format.parse(endDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    ArrayList<Date>dateArrayList = new ArrayList<>();
                    dateArrayList.add(newStartDate);
                    dateArrayList.add(newEndDate);


                    onDateSelected.onDateSelectedListener(dateArrayList);

                   /* Bundle bundle = new Bundle();
                    bundle.putString("startDate",startDate  );
                    bundle.putString("endDate", endDate );
                    MainActivity.changeAppartmentFragment(getContext(), new AddEventFragment(), true,bundle);
                  //  */
                    getActivity().onBackPressed();
                }

            }
        });

        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());


        return view;
    }

    String startDate, endDate, startTime, endTime;

    private void getTime(Date d) {
        // Get Current Date
        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker,
                                          int selectedHour, int selectedMinute) {

                        Calendar currentTime = Calendar.getInstance();
                        currentTime.set(Calendar.HOUR_OF_DAY, selectedHour);
                        currentTime.set(Calendar.MINUTE, selectedMinute);

                        Date selectedDate = currentTime.getTime();
                        System.out.println("Current time in AM/PM: " + selectedDate);

                        SimpleDateFormat spf = new SimpleDateFormat("MMM dd, yyyy ");
                        String newDateString = spf.format(d);

                        DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
                        String dateString = dateFormat.format(selectedDate);
                        System.out.println("Current time in AM/PM: " + dateString);

                        date = newDateString + " " + dateString;

                        Log.e("newDateString", newDateString);
                        clickcount = clickcount + 1;
                        if (clickcount > 2) {
                            tvStartTime.setText(" ");
                            tvEndTime.setText(" ");
                            clickcount = 1;
                        }
                        if (clickcount == 1) {
                            startDate = date;
                            tvStartTime.setText(startDate);
                        } else if (clickcount == 2) {
                            endDate = date;
                            tvEndTime.setText(endDate);
                        } else {

                        }


                    }
                }, hour, minute, false);// Yes 24 hour time

        mTimePicker.show();
    }
}
