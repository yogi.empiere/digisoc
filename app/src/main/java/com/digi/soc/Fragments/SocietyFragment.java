package com.digi.soc.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Adapters.TopAdapter;
import com.digi.soc.MainActivity;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SocietyFragment extends BaseFragment {

    @BindView(R.id.rvTop)
    RecyclerView rvTop;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_society, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), 1);

        setTop(false, "Society", false);

        rvTop.setHasFixedSize(true);
        rvTop.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rvTop.setNestedScrollingEnabled(false);

        setTopList();

        return view;
    }

    private void setTopList() {
        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<Integer> iconList = new ArrayList<>();

        nameList.add("Wings");
        iconList.add(R.drawable.ic_wings);

       /* nameList.add("Assets");
        iconList.add(R.drawable.ic_assets);*/

        nameList.add("Rules");
        iconList.add(R.drawable.ic_rules);

        nameList.add("Emergency Contact");
        iconList.add(R.drawable.ic_emergency_contact);

        nameList.add("Entry Gates");
        iconList.add(R.drawable.ic_entrygates);

        TopAdapter topAdapter = new TopAdapter(getContext(), nameList, iconList, 0, "Society");
        rvTop.setAdapter(topAdapter);
    }


}
