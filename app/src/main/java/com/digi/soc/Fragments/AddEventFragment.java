package com.digi.soc.Fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Adapters.SelectedWingsAdapter;
import com.digi.soc.Adapters.WingsSelAdapter;
import com.digi.soc.Helpers.ItemOffsetDecoration;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.OnDateSelected;
import com.digi.soc.Helpers.OnWingDeleted;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.ComplainModel;
import com.digi.soc.Models.EventModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressWarnings("ConstantConditions")
public class AddEventFragment extends BaseFragment {

    @BindView(R.id.btnAdd)
    Button btnAdd;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.btnSelectWing)
    Button btnSelectWing;
    @BindView(R.id.edtTitle)
    EditText edtTitle;
    @BindView(R.id.edtDesc)
    EditText edtDesc;
    @BindView(R.id.edtContact)
    EditText edtContact;
    @BindView(R.id.edtLocation)
    EditText edtLocation;
    @BindView(R.id.layoutWings)
    LinearLayout layoutWings;
    @BindView(R.id.tvSelectAll)
    TextView tvSelectAll;
    @BindView(R.id.btnAddW)
    Button btnAddW;
    @BindView(R.id.btnCancelW)
    Button btnCancelW;
    @BindView(R.id.rvWing)
    RecyclerView rvWing;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.ivSelImage)
    ImageView ivSelImage;

    private JSONObject eventData, created_by;

    private String userChoosenTask = "";
    private String selectedDate = "";
    private String eventPicture = "";
    private String eventName = "";
    private String eventDesc = "";
    private String eventPlace = "";
    private String eventStart = "";
    private String eventEnd = "";

    private boolean isSelecAll = true;

    private ArrayList<WingsModel> wingSelList = new ArrayList<>();

    private ArrayList<Date> eventDates = new ArrayList<>();
    private JsonArray wingsArray = new JsonArray();


    private ArrayList<WingsModel> datalist = new ArrayList<>();
    private WingsSelAdapter adapter;

    private String startDate="";
    private String endDate="";

    public AddEventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_event, container, false);
        ButterKnife.bind(this, view);
        MainActivity.setBottomPos(getContext(), -1);
        setTop(false, getResources().getString(R.string.add_event), false);

        for (WingsModel model : MyConstants.wingReqData) {
            wingSelList.add(model);
        }
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen._7sdp, R.dimen._5sdp);
        rvWing.addItemDecoration(itemDecoration);
        rvWing.setNestedScrollingEnabled(false);

      //  startDate = this.getArguments().getString("startDate");
     //   endDate = this.getArguments().getString("endDate");
        bindWidgetEvent();
        return view;
    }

    private void bindWidgetEvent() {

        tvDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(getActivity());
                MainActivity.changeFragment(getContext(), new DateFragment(new OnDateSelected() {
                    @Override
                    public void onDateSelectedListener(List<Date> list) {

                        eventDates.clear();
                        eventDates.addAll(list);

                        eventStart = Utility.eventDateFormat(list.get(0));
                        eventEnd = Utility.eventDateFormat(list.get(list.size() - 1));

                      //  selectedDate = Utility.selectedDate(getContext(), list.get(list.size() - 1));
                        selectedDate = eventStart +" - "+eventEnd;


                    }
                }, eventDates), true);

            }
        });

        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

        btnSelectWing.setOnClickListener(view1 -> {
            Utils.hideSoftKeyboard(getActivity());
            btnSelectWing.setVisibility(View.GONE);
            layoutWings.setVisibility(View.VISIBLE);

            tvSelectAll.setVisibility(View.VISIBLE);
            btnAddW.setVisibility(View.VISIBLE);
            btnCancelW.setVisibility(View.VISIBLE);

            if (wingSelList.size() == 0 || wingSelList.size() < MyConstants.wingReqData.size()) {
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                isSelecAll = false;
            } else {
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                isSelecAll = true;
            }
            // getWingsData();
            setData();
        });

        btnCancelW.setOnClickListener(view1 -> {
            Utils.hideSoftKeyboard(getActivity());

            for (WingsModel model : MyConstants.wingReqData) {
                model.setSelect(true);
            }
            isSelecAll = true;
            wingSelList.clear();
            for (WingsModel model : MyConstants.wingReqData) {
                wingSelList.add(model);
            }
            btnSelectWing.setVisibility(View.VISIBLE);
            btnSelectWing.setText(getResources().getString(R.string.select_wing));
            layoutWings.setVisibility(View.GONE);
        });

        tvSelectAll.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(getActivity());
            if (isSelecAll) {
                isSelecAll = false;
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                for (WingsModel model : MyConstants.wingReqData) {
                    model.setSelect(false);
                }
                adapter.notifyDataSetChanged();
            } else {
                isSelecAll = true;
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                for (WingsModel model : MyConstants.wingReqData) {
                    model.setSelect(true);
                }
                adapter.notifyDataSetChanged();
            }
        });

        btnAddW.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(getActivity());

            wingSelList.clear();

            for (int x = 0; x < MyConstants.wingReqData.size(); x++) {
                if (MyConstants.wingReqData.get(x).isSelect())
                    wingSelList.add(MyConstants.wingReqData.get(x));
            }

            if (wingSelList.size() > 0) {

                btnSelectWing.setVisibility(View.VISIBLE);
                btnSelectWing.setText(getResources().getString(R.string.edit_wing));
                layoutWings.setVisibility(View.VISIBLE);

                rvWing.setAdapter(new SelectedWingsAdapter(getContext(), wingSelList, new OnWingDeleted() {
                    @Override
                    public void onWingDeleteListner() {
                        if (wingSelList.size() == 0) {
                            btnSelectWing.setVisibility(View.VISIBLE);
                            layoutWings.setVisibility(View.GONE);

                            for (WingsModel model : MyConstants.wingReqData) {
                                model.setSelect(false);
                            }

                        } else {
                            for (WingsModel modelDatalist : MyConstants.wingReqData) {
                                boolean isMatch = false;
                                for (WingsModel modelSelWings : wingSelList) {
                                    if (modelDatalist.getName().equals(modelSelWings.getName())) {
                                        isMatch = true;
                                    }
                                    if (wingSelList.get(wingSelList.size() - 1).equals(modelSelWings)) {
                                        if (isMatch)
                                            modelDatalist.setSelect(true);
                                        else
                                            modelDatalist.setSelect(false);
                                    }
                                }
                            }
                        }
                    }
                }));
                rvWing.setLayoutManager(new GridLayoutManager(getContext(), 2));

                btnAddW.setVisibility(View.GONE);
                btnCancelW.setVisibility(View.GONE);
                tvSelectAll.setVisibility(View.GONE);
            } else {
                Utility.showDialog(getContext(), getResources().getString(R.string.error), getResources().getString(R.string.blank_event_wings));
            }

        });

        btnAdd.setOnClickListener(view -> {
            if (checkValidation()) {
                addEvent();

            }
        });



    }

    private void addEvent() {
        JsonObject jObj;
        jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("title", eventName);
        jObj.addProperty("description", eventDesc);
        jObj.addProperty("place", eventPlace);
        jObj.addProperty("start_time", eventStart);
        jObj.addProperty("end_time", eventEnd);
        jObj.add("attendees", wingsArray);

        Log.e("Json", jObj.toString());

        new NetworkCall().callPostJSON(getActivity(), Services.EVENT,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getActivity(), requestCode, response);
                        Log.e("code-->", returnResult);
                        if (requestCode.equals("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean success = jsonObject.getBoolean("success");
                                setEventData(jsonObject);
                                if (success) {
                                    android.app.AlertDialog.Builder alertDialog;
                                    alertDialog = new android.app.AlertDialog.Builder(getContext());
                                    alertDialog.setTitle("Success!");
                                    alertDialog.setMessage("Event created");

                                    alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                        edtTitle.getText().clear();
                                        edtDesc.getText().clear();
                                        edtLocation.getText().clear();
                                        edtContact.getText().clear();
                                        eventPicture = "";
                                        ivSelImage.setImageResource(R.drawable.ic_add_image);
                                        tvDate.setText(getResources().getString(R.string.select_date));
                                        eventStart = "";
                                        eventEnd = "";
                                        selectedDate = "";
                                        if (getActivity().getCurrentFocus() != null) {
                                            getActivity().getCurrentFocus().clearFocus();
                                        }
                                        getActivity().onBackPressed();
                                    });
                                    getActivity().runOnUiThread(() -> alertDialog.show());
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }


                });
    }

    private void setEventData(JSONObject jsonObject) throws JSONException {
        eventData = jsonObject.getJSONObject("event");
        created_by = eventData.getJSONObject("created_by");
        ArrayList<String> roles;
        roles = (ArrayList<String>) new Gson().fromJson(created_by.getString("roles"),
                new TypeToken<ArrayList<String>>() {}.getType());

        ArrayList<EventModel.Attendees> wings;
        wings = (ArrayList<EventModel.Attendees>) new Gson().fromJson(eventData.getString("attendees"),
                new TypeToken<ArrayList<EventModel.Attendees>>() {}.getType());

        EventModel event = new EventModel(
                eventData.getInt("id"),
                eventData.getString("title"),
                eventData.getString("description"),
                eventData.getString("place"),
                eventData.getString("start_time"),
                eventData.getString("end_time"),
                eventData.getString("updated_at"),
                eventData.getString("created_at"),
                new EventModel.CreatedBy(
                        created_by.getInt("id"),
                        created_by.getString("first_name"),
                        created_by.getString("last_name"),
                        created_by.getString("email"),
                        created_by.getString("contact_no"),
                        created_by.getString("profile_pic"),
                        roles),wings);

        MyConstants.eventData.add(event);
    }

    private boolean checkValidation() {
        boolean isError = false;

        eventDesc = edtDesc.getText().toString();
        eventName = edtTitle.getText().toString();
        eventPlace = edtLocation.getText().toString();
        String contactNo = edtContact.getText().toString();

        wingsArray = new JsonArray();
        for (WingsModel list : MyConstants.wingReqData) {
            if (list.isSelect()) {
                wingsArray.add(list.getId());
            }
        }
        if (TextUtils.isEmpty(eventName)) {
            isError = true;
            edtTitle.requestFocus();
            edtTitle.setError(getResources().getString(R.string.blank_event_title));
        } else if (TextUtils.isEmpty(eventDesc)) {
            isError = true;
            edtDesc.requestFocus();
            edtDesc.setError(getResources().getString(R.string.blank_event_desc));
        } else if (TextUtils.isEmpty(eventPlace)) {
            isError = true;
            edtLocation.requestFocus();
            edtLocation.setError(getResources().getString(R.string.blank_event_place));
        } else if (TextUtils.isEmpty(contactNo)) {
            isError = true;
            edtContact.requestFocus();
            edtContact.setError(getResources().getString(R.string.blank_event_conatct));
        } else if (TextUtils.isEmpty(eventStart)) {
            isError = true;
            Utility.showDialog(getActivity(), getResources().getString(R.string.error), getResources().getString(R.string.blank_event_start_date));
        } else if (TextUtils.isEmpty(eventEnd)) {
            isError = true;
            Utility.showDialog(getActivity(), getResources().getString(R.string.error), getResources().getString(R.string.blank_event_end_date));
        } else if (wingsArray.size() == 0) {
            isError = true;
            Utility.showDialog(getActivity(), getResources().getString(R.string.error), getResources().getString(R.string.blank_event_wings));
        }
        return !isError;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(selectedDate)) {
            tvDate.setText(selectedDate);

        }
 /*       if (wingSelList.size() > 0) {
            layoutWings.setVisibility(View.VISIBLE);
            btnAddW.setVisibility(View.GONE);
            btnCancelW.setVisibility(View.GONE);
            tvSelectAll.setVisibility(View.GONE);
            rvWing.setAdapter(new SelectedWingsAdapter(getContext(), wingSelList, () -> {
                btnSelectWing.setVisibility(View.VISIBLE);
                layoutWings.setVisibility(View.GONE);
            }));
            rvWing.setLayoutManager(new GridLayoutManager(getContext(), 2));
        }*/
    }

    private void getWingsData() {
        if (datalist.size() > 0) {
            setData();
        } else {
            JsonObject jObj = new JsonObject();
            new NetworkCall(getContext(), Services.DASHBOARD,
                    jObj, true, NetworkCall.REQ_GET,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response, String requestCode) {
                    String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                    Log.e("code-->", requestCode);
                    if (requestCode.equals("200")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Gson gson = new Gson();
                            Type type = new TypeToken<List<WingsModel>>() {
                            }.getType();
                            List<WingsModel> list = gson.fromJson(jsonObject.getJSONObject("wings").getJSONArray("list").toString(), type);

                            ArrayList<WingsModel> arrModel = new ArrayList<>();
                            arrModel.addAll(list);

                            for (WingsModel d : arrModel) {
                                WingsModel wingsModel = new WingsModel();
                                wingsModel.setWings_Name(d.getName());
                                wingsModel.setWings_id(d.getId());
                                wingsModel.setSelect(false);
                                datalist.add(wingsModel);
                            }
                            setData();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    if (!returnResult.isEmpty()) {
                        return;
                    } else {
                        for (int i = 0; i < 6; i++) {
                            WingsModel wingsModel = new WingsModel();
                            wingsModel.setWings_Name("Wing " + i);
                            wingsModel.setSelect(false);
                            datalist.add(wingsModel);
                        }
                        setData();
                    }
                }
            });
        }
    }

    private void setData() {
        rvWing.setHasFixedSize(true);
        rvWing.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rvWing.setItemViewCacheSize(2);
        adapter = new WingsSelAdapter(getContext(), MyConstants.wingReqData, new OnWingDeleted() {
            @Override
            public void onWingDeleteListner() {
                isSelecAll = false;
                tvSelectAll.setText(getResources().getString(R.string.select_all));
            }
        });
        rvWing.setAdapter(adapter);
    }
}
