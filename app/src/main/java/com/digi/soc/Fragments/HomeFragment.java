package com.digi.soc.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Activity.AddNoticeActivity;
import com.digi.soc.Adapters.HomeAdapter;
import com.digi.soc.Adapters.TopAdapter;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.AlarmModel;
import com.digi.soc.Models.ComplainModel;
import com.digi.soc.Models.EventModel;
import com.digi.soc.Models.HomeModel;
import com.digi.soc.Models.MemberRequestModel;
import com.digi.soc.Models.NoticeModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.SignupActivity;
import com.digi.soc.SocietyTabActivity;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.digi.soc.MainActivity.changeFragment;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.rvTop)
    RecyclerView rvTop;

    @BindView(R.id.rvHome)
    RecyclerView rvHome;

    @BindView(R.id.ivAddPost)
    ImageView ivAddPost;

    @BindView(R.id.ivSetting)
    ImageView ivSetting;

    private Animation shake, scale;
    private ArrayList<HomeModel> homeFeedData;
    private HomeAdapter homeAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        topAnimation();

        MainActivity.setBottomPos(getContext(), 0);

        setTop(true, "Home", false);

        topDataList();

        ivAddPost.setOnClickListener(view1 ->
                MainActivity.changeFragment(getContext(),
                        new AddFeedFragment(), true));

        getDashboardData();
        setTopList();

        ivSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.changeFragment(getContext(), new SettingsFragment(), true);
            }
        });

        return view;
    }

    private void topDataList() {

        rvTop.setHasFixedSize(true);
        rvTop.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
        rvTop.setNestedScrollingEnabled(false);
    }

    private void setHomeData() {
        rvHome.setHasFixedSize(true);
        homeAdapter = new HomeAdapter(getContext(), homeFeedData);
        rvHome.setAdapter(homeAdapter);
        rvHome.setLayoutManager(new LinearLayoutManager(getContext()));
        rvHome.setNestedScrollingEnabled(false);
    }

    private void topAnimation() {
        shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        scale = AnimationUtils.loadAnimation(getContext(), R.anim.scale);
        ivNoti.startAnimation(shake);
        ivAlarm.startAnimation(scale);
        ivAlarm.setOnClickListener(view1 -> {
         /*   Intent i = new Intent(getContext(), SocietyTabActivity.class);
            i.putExtra("type","Alarm");
            startActivity(i);*/
            Bundle bundle = new Bundle();
            bundle.putString("type", "Alarm");
            Fragment fragment = new SocietyTabFragment();
            fragment.setArguments(bundle);
            changeFragment(getContext(), fragment, true);
        });
    }

    private void setTopList() {
        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<Integer> iconList = new ArrayList<>();

        nameList.add("Notice");
        iconList.add(R.drawable.ic_notice);

        nameList.add("Complain");
        iconList.add(R.drawable.ic_complain);

        nameList.add("Polls");
        iconList.add(R.drawable.ic_poll);

        nameList.add("Event");
        iconList.add(R.drawable.ic_event);

       /* nameList.add("Booking");
        iconList.add(R.drawable.ic_booking);*/

        nameList.add("Member Req.");
        iconList.add(R.drawable.ic_member_req);

        TopAdapter topAdapter = new TopAdapter(getContext(), nameList, iconList, 0);
        rvTop.setAdapter(topAdapter);
    }

    private void getDashboardData() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getActivity(), Services.DASHBOARD, jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String code) {
                try {
                    String returnResult = Utils.handleAPIResponceCode(getActivity(), code, response);
                    if (returnResult.isEmpty()) {
                        return;
                    }
                    JSONObject Obj = new JSONObject(response);
                    JSONObject homeFeedsObj = Obj.getJSONObject("feed");
                    JSONArray homeFeedList = homeFeedsObj.getJSONArray("list");
                    homeFeedData = (ArrayList<HomeModel>) new Gson().fromJson(String.valueOf(homeFeedList), new TypeToken<ArrayList<HomeModel>>() {}.getType());
                    setHomeData();

                   //notice tab Data
                    MyConstants.noticeData.clear();
                    JSONObject noticeObj = Obj.getJSONObject("notices");
                    JSONArray noticeDataList = noticeObj.getJSONArray("list");
                    MyConstants.noticeData = (ArrayList<NoticeModel>) new Gson().fromJson(String.valueOf(noticeDataList),
                            new TypeToken<ArrayList<NoticeModel>>() {
                    }.getType());


                    //complain tab Data
                    MyConstants.complainData.clear();
                     JSONObject complainObj = Obj.getJSONObject("complaints");
                    JSONArray complainDataList = complainObj.getJSONArray("list");
                    MyConstants.complainData = (ArrayList<ComplainModel>) new Gson().fromJson(String.valueOf(complainDataList), new TypeToken<ArrayList<ComplainModel>>() {
                    }.getType());

                    //wings tab Data
                    MyConstants.wingReqData.clear();
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<WingsModel>>() {}.getType();
                    List<WingsModel> list = gson.fromJson(Obj.getJSONObject("wings")
                            .getJSONArray("list").toString(), type);
                    MyConstants.wingReqData.addAll(list);


                    //for Event list
                    MyConstants.eventData.clear();
                    JSONObject eventsObj = Obj.getJSONObject("events");
                    JSONArray eventsDataList = eventsObj.getJSONArray("list");
                    MyConstants.eventData = (ArrayList<EventModel>)
                            new Gson().fromJson(String.valueOf(eventsDataList), new TypeToken<ArrayList<EventModel>>() {
                    }.getType());


                    //for member request
                    MyConstants.memberReqData.clear();
                    JSONObject member_reqObj = Obj.getJSONObject("member_req");
                    JSONArray member_reqDataList = member_reqObj.getJSONArray("list");

                    MyConstants.memberReqData = (ArrayList<MemberRequestModel>)
                            new Gson().fromJson(String.valueOf(member_reqDataList),
                                    new TypeToken<ArrayList<MemberRequestModel>>() {
                                    }.getType());

                    //for  active alarmdata
                    MyConstants.arrAlarmModel.clear();
                    Gson gsonAlarm = new Gson();
                    Type typeAlarm = new TypeToken<List<AlarmModel>>() {
                    }.getType();
                    MyConstants.listAlarm = gsonAlarm.fromJson(Obj
                            .getJSONObject("alarms").getJSONObject("active")
                            .getJSONArray("list").toString(), typeAlarm);

                   MyConstants.arrAlarmModel.addAll(MyConstants.listAlarm);

                    //for closed alarm
                    Gson gsonClosed = new Gson();
                    Type typeClosed = new TypeToken<List<AlarmModel>>() {
                    }.getType();
                    MyConstants.listClosed =
                            gsonClosed.fromJson(Obj.getJSONObject("alarms")
                                    .getJSONObject("closed").getJSONArray("list")
                                    .toString(), typeClosed);
                    MyConstants.arrAlarmClosed.addAll(MyConstants.listClosed);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

}
