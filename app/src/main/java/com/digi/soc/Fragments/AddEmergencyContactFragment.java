package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.EmergencyModel;
import com.digi.soc.Models.RulesModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class AddEmergencyContactFragment extends BaseFragment  {

    @BindView(R.id.edtname)
    EditMedium edtname;

    @BindView(R.id.edtno)
    EditMedium edtno;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    private String name = "";
    private String no = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_society_emergency_contact, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, "Add Emergency Contact", false);

        bindWidget();

        return view;
    }

    private void bindWidget() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkValidation()) {
                    addEmergencyContact();
                }

            }
        });

        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

    }
    private void addEmergencyContact() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("name", name);
        jObj.addProperty("contact_no", no);

        new NetworkCall().callPostJSON(getActivity(), Services.SOCIETY_EMERGENCY,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");

                                /*JSONObject emergency_contact = jsonObject.getJSONObject("emergency_contact");
                                int id = emergency_contact.getInt("id");
                                String name = emergency_contact.getString("name");
                                String contact_no = emergency_contact.getString("contact_no");

                                EmergencyModel emergencyModel = new EmergencyModel();
                                emergencyModel.setId(id);
                                emergencyModel.setName(name);
                                emergencyModel.setContact_no(contact_no);

                                MyConstants.emergencyModels.add(emergencyModel);*/

                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                    edtname.getText().clear();
                                    edtno.getText().clear();

                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });

    }


    private boolean checkValidation() {
        boolean isError = false;

         name = edtname.getText().toString().trim();
         no = edtno.getText().toString().trim();

        if (TextUtils.isEmpty(name)) {
            isError = true;
            edtname.requestFocus();
            edtname.setError(getResources().getString(R.string.blank_emergency_name));
        }
        else if (TextUtils.isEmpty(no)) {
            isError = true;
            edtno.requestFocus();
            edtno.setError(getResources().getString(R.string.blank_emergency_no));
        }
        return !isError;
    }
    @Override
    public void onDestroy() {
        Fragment fragment = new SocietyTabFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "Society");
        bundle.putInt("selTabPos", 2);
        fragment.setArguments(bundle);
        MainActivity.changeFragment(getContext(), fragment, true);
        super.onDestroy();
    }
}
