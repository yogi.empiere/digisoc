package com.digi.soc.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.digi.soc.Adapters.SelectedWingsAdapter;
import com.digi.soc.Adapters.WingsSelAdapter;
import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Controls.TextLight;
import com.digi.soc.Controls.TextMedium;
import com.digi.soc.Helpers.ItemOffsetDecoration;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.MySharedPref;
import com.digi.soc.Helpers.OnWingDeleted;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class AddPollFragment extends BaseFragment {

    @BindView(R.id.btnAddPoll)
    Button btnAddPoll;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.rvWing)
    RecyclerView rvWing;

    @BindView(R.id.btnSelectWing)
    Button btnSelectWing;

    @BindView(R.id.layoutWings)
    LinearLayout layoutWings;

    @BindView(R.id.tvSelectAll)
    TextView tvSelectAll;

    @BindView(R.id.btnAddW)
    Button btnAddW;

    @BindView(R.id.btnCancelW)
    Button btnCancelW;

    @BindView(R.id.edtTitle)
    EditMedium edtTitle;

    @BindView(R.id.edtPollOption1)
    EditMedium edtPollOption1;

    @BindView(R.id.edtPollOption2)
    EditMedium edtPollOption2;

    @BindView(R.id.edtPollOption3)
    EditMedium edtPollOption3;

    @BindView(R.id.edtPollOption4)
    EditMedium edtPollOption4;

    @BindView(R.id.addOtherOption)
    TextMedium addOtherOption;

    @BindView(R.id.layoutOptions)
    LinearLayout layoutOptions;
    ArrayList<TextLight> arrayTitleOption = new ArrayList<TextLight>();
    ArrayList<EditMedium> arrayedtOption = new ArrayList<EditMedium>();

    private boolean isSelecAll = false;
    private WingsSelAdapter adapter;
    private ArrayList<WingsModel> wingSelList = new ArrayList<>();
    private JsonArray wingsArray = new JsonArray();
    private JsonArray optionArray = new JsonArray();
    private String pollTitle = "";
    private String option1 = "";
    private String option2 = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_poll, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, getString(R.string.add_poll), false);

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen._7sdp, R.dimen._5sdp);
        rvWing.addItemDecoration(itemDecoration);
        rvWing.setNestedScrollingEnabled(false);

        bindWidget();


        return view;
    }

    private void bindWidget() {
        btnAddPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    addPoll();
                }
            }

        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

        btnSelectWing.setOnClickListener(view1 -> {

            Utils.hideSoftKeyboard(getActivity());
            btnSelectWing.setVisibility(View.GONE);
            layoutWings.setVisibility(View.VISIBLE);

            tvSelectAll.setVisibility(View.VISIBLE);
            btnAddW.setVisibility(View.VISIBLE);
            btnCancelW.setVisibility(View.VISIBLE);

         /*   if (wingSelList.size() == 0 || wingSelList.size() < MyConstants.wingReqData.size()) {
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                isSelecAll = false;
            } else {
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                isSelecAll = true;
            }*/
            setData();
        });

        btnCancelW.setOnClickListener(view1 ->
        {
            Utils.hideSoftKeyboard(getActivity());

            isSelecAll = false;
            wingSelList.clear();

            btnSelectWing.setVisibility(View.VISIBLE);
            btnSelectWing.setText(getResources().getString(R.string.select_wing));
            layoutWings.setVisibility(View.GONE);
        });

        tvSelectAll.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(getActivity());
            if (isSelecAll) {
                isSelecAll = false;
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                for (WingsModel model : MyConstants.wingReqData) {
                    model.setSelect(false);
                }
                adapter.notifyDataSetChanged();
            } else {
                isSelecAll = true;
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                for (WingsModel model : MyConstants.wingReqData) {
                    model.setSelect(true);
                }
                adapter.notifyDataSetChanged();
            }
        });

        btnAddW.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(getActivity());
            wingSelList.clear();

            for (int x = 0; x < MyConstants.wingReqData.size(); x++) {
                if (MyConstants.wingReqData.get(x).isSelect())
                    wingSelList.add(MyConstants.wingReqData.get(x));
            }

            if (wingSelList.size() > 0) {

                btnSelectWing.setVisibility(View.VISIBLE);
                btnSelectWing.setText(getResources().getString(R.string.edit_wing));
                layoutWings.setVisibility(View.VISIBLE);

                rvWing.setAdapter(new SelectedWingsAdapter(getContext(), wingSelList, new OnWingDeleted() {
                    @Override
                    public void onWingDeleteListner() {
                        if (wingSelList.size() == 0) {
                            btnSelectWing.setVisibility(View.VISIBLE);
                            layoutWings.setVisibility(View.GONE);

                            for (WingsModel model : MyConstants.wingReqData) {
                                model.setSelect(false);
                            }

                        } else {
                            for (WingsModel modelDatalist : MyConstants.wingReqData) {
                                boolean isMatch = false;
                                for (WingsModel modelSelWings : wingSelList) {
                                    if (modelDatalist.getName().equals(modelSelWings.getName())) {
                                        isMatch = true;
                                    }
                                    if (wingSelList.get(wingSelList.size() - 1).equals(modelSelWings)) {
                                        if (isMatch)
                                            modelDatalist.setSelect(true);
                                        else
                                            modelDatalist.setSelect(false);
                                    }
                                }
                            }
                        }
                    }
                }));
                rvWing.setLayoutManager(new GridLayoutManager(getContext(), 2));

                btnAddW.setVisibility(View.GONE);
                btnCancelW.setVisibility(View.GONE);
                tvSelectAll.setVisibility(View.GONE);
            } else {
                Utility.showDialog(getContext(), getResources().getString(R.string.error), getResources().getString(R.string.blank_event_wings));
            }

        });

        addOtherOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrayTitleOption.size() < 2) {
                    addview();
                }
            }
        });
    }

    private void addview() {
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = vi.inflate(R.layout.row_poll_list, null);
        layoutOptions.addView(view);

        TextLight tvOption = view.findViewById(R.id.tvOption);
        EditMedium edtPollOption = view.findViewById(R.id.edtPollOption);
        ImageView ivClose = view.findViewById(R.id.ivClose);

        arrayTitleOption.add(tvOption);
        arrayedtOption.add(edtPollOption);
        if (arrayTitleOption.size() == 2) {
            addOtherOption.setVisibility(View.GONE);
        }

        layoutOptions.setVisibility(View.VISIBLE);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayTitleOption.remove(layoutOptions.indexOfChild(view));
                arrayedtOption.remove(layoutOptions.indexOfChild(view));
                layoutOptions.removeViewAt(layoutOptions.indexOfChild(view));
                if (arrayTitleOption.size() < 2) {
                    addOtherOption.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void setData() {
        for (WingsModel model : MyConstants.wingReqData) {
            model.setSelect(false);
            wingSelList.add(model);
        }
        rvWing.setHasFixedSize(true);
        rvWing.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rvWing.setItemViewCacheSize(2);
        adapter = new WingsSelAdapter(getContext(), MyConstants.wingReqData, new OnWingDeleted() {
            @Override
            public void onWingDeleteListner() {
                isSelecAll = false;
                tvSelectAll.setText(getResources().getString(R.string.select_all));
            }
        });
        rvWing.setAdapter(adapter);
    }

    private void addPoll() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("title", pollTitle);
        jObj.add("options", optionArray);
        jObj.add("wings", wingsArray);

        new NetworkCall().callPostJSON(getActivity(), Services.SOCIETY_POLL,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        Log.e("responsecode-->", response);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");

                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }


                });
    }

    private boolean checkValidation() {
        boolean isError = false;

        pollTitle = edtTitle.getText().toString();
        option1 = edtPollOption1.getText().toString();
        option2 = edtPollOption2.getText().toString();

        for (WingsModel list : MyConstants.wingReqData) {
            if (list.isSelect()) {
                wingsArray.add(list.getId());
            }
        }
        if (TextUtils.isEmpty(pollTitle)) {
            isError = true;
            edtTitle.requestFocus();
            edtTitle.setError(getString(R.string.blank_poll_title));
        } else if (TextUtils.isEmpty(option1)) {
            isError = true;
            edtPollOption1.requestFocus();
            edtPollOption1.setError(getString(R.string.blank_poll_option1));
        } else if (TextUtils.isEmpty(option2)) {
            isError = true;
            edtPollOption2.requestFocus();
            edtPollOption2.setError(getString(R.string.blank_poll_option2));
        } else if (arrayedtOption.size() > 0) {
            optionArray = new JsonArray();
            optionArray.add(option1);
            optionArray.add(option2);
            for (int i = 0; i < arrayedtOption.size(); i++) {
                String edtoption = arrayedtOption.get(i).getText().toString();
                if (TextUtils.isEmpty(edtoption)) {
                    isError = true;
                    arrayedtOption.get(i).requestFocus();
                    arrayedtOption.get(i).setError(getString(R.string.blank_poll_option));
                    break;
                } else {
                    optionArray.add(edtoption);
                }
                if (i == arrayedtOption.size() - 1) {
                    if (wingsArray.size() == 0) {
                        isError = true;
                        Utility.showDialog(getActivity(),
                                getResources().getString(R.string.error),
                                getResources().getString(R.string.blank_poll_wings));
                    }
                }
            }
        }else if (wingsArray.size() == 0) {
            isError = true;
            Utility.showDialog(getActivity(),
                    getResources().getString(R.string.error),
                    getResources().getString(R.string.blank_poll_wings));
        }else if(arrayedtOption.size() == 0){
            optionArray = new JsonArray();
            optionArray.add(option1);
            optionArray.add(option2);
        }

        return !isError;
    }

}
