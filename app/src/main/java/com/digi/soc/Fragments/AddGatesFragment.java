package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.GatesModel;
import com.digi.soc.Models.RulesModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class AddGatesFragment extends BaseFragment {

    @BindView(R.id.edtGate)
    EditMedium edtGate;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    private String gates ="";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_society_gates, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, getResources().getString(R.string.add_gate), false);

        bindWidget();

        return view;
    }

    private void bindWidget() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    addGate();
                }
            }
        });

        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

    }
    private void addGate() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("name", gates);

        new NetworkCall().callPostJSON(getActivity(), Services.SOCIETY_GATES,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");

                                /*JSONObject entry_gate = jsonObject.getJSONObject("entry_gate");
                                int id = entry_gate.getInt("id");
                                String name = entry_gate.getString("name");

                                GatesModel gatesModel = new GatesModel();
                                gatesModel.setId(id);
                                gatesModel.setName(name);

                                MyConstants.gatesModels.add(gatesModel);*/

                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                    edtGate.getText().clear();

                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });

    }
    private boolean checkValidation() {
        boolean isError = false;

        gates = edtGate.getText().toString().trim();

        if (TextUtils.isEmpty(gates)) {
            isError = true;
            edtGate.requestFocus();
            edtGate.setError(getResources().getString(R.string.blank_gate_title));
        }
        return !isError;
    }

    @Override
    public void onDestroy() {
        Fragment fragment = new SocietyTabFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "Society");
        bundle.putInt("selTabPos", 3);
        fragment.setArguments(bundle);
        MainActivity.changeFragment(getContext(), fragment, true);
        super.onDestroy();
    }
}
