package com.digi.soc.Fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.digi.soc.Controls.TextLight;
import com.digi.soc.Helpers.JsonParserUniversal;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.MySharedPref;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class AddSecurityFragment extends BaseFragment {

    @BindView(R.id.edtFName)
    EditText edtFName;

    @BindView(R.id.edtLName)
    EditText edtLName;

    @BindView(R.id.edtContact)
    EditText edtContact;

    @BindView(R.id.edtPass)
    EditText edtPass;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.llDob)
    LinearLayout llDob;

    @BindView(R.id.tvcalendar)
    TextLight tvcalendar;


    private String fName="";
    private String lName="";
    private String pass="";
    private String no="";
    private String dateStr = "";
    private String start_date = "";
    private JsonParserUniversal jsonParserUniversal;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_people_security, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, getString(R.string.add_security), false);
        jsonParserUniversal = new JsonParserUniversal();


        bindWidget();

        return view;
    }

    private void bindWidget() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    Utils.hideSoftKeyboard(getActivity());
                    addSecurity();
                }

            }
        });
       btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());


        llDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(getActivity());
                pickDate(tvcalendar);
            }
        });
        tvcalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(getActivity());
                pickDate(tvcalendar);
            }
        });
    }

    private void pickDate(final TextView tv_date) {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateStr = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth + " ";
                tv_date.setText(dateStr);
            }
        }, mYear, mMonth, mDay);
        c.add(Calendar.DATE, 1);

        try {
            if (tv_date.getId() == R.id.tvcalendar) {
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }

        }catch (Exception e) {
            e.printStackTrace();
        }

        datePickerDialog.show();
    }

    private void addSecurity() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;

       jObj.addProperty("user_type", "security-guard");
       jObj.addProperty("first_name", fName);
       jObj.addProperty("last_name", lName);
       jObj.addProperty("contact_no", no);
       jObj.addProperty("password", pass);
       jObj.addProperty("society_id", 1);
       jObj.addProperty("date_of_birth", start_date);

       new NetworkCall().callPostJSON(getActivity(), Services.ADD_SOCIETY_GUARD,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {

                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });
    }

   private boolean checkValidation() {
        boolean isError = false;

      fName = edtFName.getText().toString().trim();
      lName = edtLName.getText().toString().trim();
      no = edtContact.getText().toString().trim();
      pass = edtPass.getText().toString().trim();
      start_date = tvcalendar.getText().toString().trim();

       if (TextUtils.isEmpty(fName)) {
            isError = true;
            edtFName.requestFocus();
            edtFName.setError("Please Enter First Name");
        }
       else if (TextUtils.isEmpty(lName)) {
           isError = true;
           edtLName.requestFocus();
           edtLName.setError("Please Enter Last Name");
       }
       else if (TextUtils.isEmpty(no)) {
           isError = true;
           edtContact.requestFocus();
           edtContact.setError(getResources().getString(R.string.blank_contact));
       }
       else if (TextUtils.isEmpty(pass)) {
           isError = true;
           edtPass.requestFocus();
           edtPass.setError("Please Enter Password");
       }
       else if (start_date.equals("")) {
           isError = true;
           tvcalendar.requestFocus();
           tvcalendar.setError("Please Enter Birthdate");
       }
        return !isError;
    }
    @Override
    public void onDestroy() {
        Fragment fragment = new SocietyTabFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "People");
        bundle.putInt("selTabPos", 3);
        fragment.setArguments(bundle);
        MainActivity.changeFragment(getContext(), fragment, true);
        super.onDestroy();
    }
}
