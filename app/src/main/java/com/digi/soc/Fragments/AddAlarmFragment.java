package com.digi.soc.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.digi.soc.Adapters.WingsAdapter;
import com.digi.soc.Helpers.JsonParserUniversal;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.AlarmModel;
import com.digi.soc.Models.ApartmentModel;
import com.digi.soc.Models.EventModel;
import com.digi.soc.Models.NoticeModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class AddAlarmFragment extends BaseFragment {

    @BindView(R.id.btnAddAlarm)
    Button btnAddAlarm;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.spinner_wing)
    Spinner spinner_wing;

    @BindView(R.id.spinner_apartment)
    Spinner spinner_apartment;

    private int apartmentNo = 0, wingId = 0;

    private JsonParserUniversal jsonParserUniversal;
    private JSONObject alarmdata, apartment, created_by;

    public static ArrayList<WingsModel> wingModelList = new ArrayList<>();
    public static ArrayList<String> wingNameList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_alarm, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, getString(R.string.addAlarm), false);
        jsonParserUniversal = new JsonParserUniversal();
        getApartment();
        getWing();
        bindWidget();
        return view;
    }

    private void bindWidget() {
        btnAddAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAlarm();
            }

        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());
    }


    private void addAlarm() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("apartment_id", apartmentNo);
        jObj.addProperty("type", "This is a alarm type");

        new NetworkCall().callPostJSON(getActivity(), Services.ALARM,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");

                                setAlarmData(jsonObject);

                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }


                });
    }

    private void setAlarmData(JSONObject jsonObject) throws JSONException {

        alarmdata = jsonObject.getJSONObject("alarm");
        apartment = alarmdata.getJSONObject("apartment");
        created_by = alarmdata.getJSONObject("created_by");
        JSONObject current_tenant = apartment.getJSONObject("current_tenant");
        JSONObject owned_by = apartment.getJSONObject("owned_by");
        JSONObject wing = apartment.getJSONObject("wing");

        ArrayList<String> roles_current_tenant;
        roles_current_tenant = (ArrayList<String>) new Gson().fromJson(current_tenant.getString("roles"),
                new TypeToken<ArrayList<String>>() {
                }.getType());

        ArrayList<String> roles_created_by;
        roles_created_by = (ArrayList<String>) new Gson().fromJson(created_by.getString("roles"),
                new TypeToken<ArrayList<String>>() {
                }.getType());

        ArrayList<String> roles_owned_by;
        roles_owned_by = (ArrayList<String>) new Gson().fromJson(owned_by.getString("roles"),
                new TypeToken<ArrayList<String>>() {
                }.getType());

        AlarmModel.WingDetail wingDetail = new AlarmModel.WingDetail(
                wing.getInt("id"),
                wing.getString("name"),
                wing.getString("no_of_floors"));

        AlarmModel.OwnedDetail current_tenantObj =
                new AlarmModel.OwnedDetail(
                        current_tenant.getInt("id"),
                        current_tenant.getString("first_name"),
                        current_tenant.getString("last_name"),
                        current_tenant.getString("email"),
                        current_tenant.getString("contact_no"),
                        current_tenant.getString("profile_pic"), roles_current_tenant);

        AlarmModel.OwnedDetail own_detailObj =
                new AlarmModel.OwnedDetail(
                        owned_by.getInt("id"),
                        owned_by.getString("first_name"),
                        owned_by.getString("last_name"),
                        owned_by.getString("email"),
                        owned_by.getString("contact_no"),
                        owned_by.getString("profile_pic"), roles_owned_by);

        AlarmModel alarm = new AlarmModel(
                alarmdata.getInt("id"),
                alarmdata.getString("type"),
                alarmdata.getString("status"),
                new AlarmModel.Apartment(
                        apartment.getBoolean("isRented"),
                        apartment.getInt("id"),
                        apartment.getInt("floor_no"),
                        apartment.getString("apartment_no"),
                        current_tenantObj, own_detailObj, wingDetail),
                alarmdata.getString("created_at"),
                new AlarmModel.OwnedDetail(
                        created_by.getInt("id"),
                        created_by.getString("first_name"),
                        created_by.getString("last_name"),
                        created_by.getString("email"),
                        created_by.getString("contact_no"),
                        created_by.getString("profile_pic"), roles_created_by));

        MyConstants.arrAlarmModel.add(alarm);
    }

    private void getWing() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getActivity(), Services.GET_WINGS, jObj,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getActivity(), requestCode, response);
                if (returnResult.isEmpty())
                    return;

                try {
                    JSONObject jsonObject = new JSONObject(returnResult);
                    JSONArray wings = jsonObject.getJSONArray("wings");
                    System.out.println(wings + "wingsarray");

                    wingModelList.clear();
                    wingNameList.clear();
                    WingsModel wingData = new WingsModel();
                    wingData.setName(getString(R.string.select_wing));
                    wingData.setId(0);
                    wingModelList.add(wingData);
                    wingNameList.add(wingData.getName());

                    for (int i = 0; i < wings.length(); i++) {
                        JSONObject object = wings.getJSONObject(i);
                        WingsModel wingsModel = (WingsModel) jsonParserUniversal.parseJson(object, new WingsModel());

                        if (wingsModel.getName() != null && !wingsModel.getName().toLowerCase().equals("null")) {
                            wingNameList.add(wingsModel.getName());
                            wingModelList.add(wingsModel);
                        }
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, wingNameList);
                    spinner_wing.setAdapter(adapter);
                    spinner_wing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            wingId = wingModelList.get(i).getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getApartment() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getActivity(), Services.APARTMENT, jObj,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getActivity(), requestCode, response);
                if (returnResult.isEmpty())
                    return;

                try {
                    JSONObject jsonObject = new JSONObject(returnResult);
                    JSONArray apartments = jsonObject.getJSONArray("apartments");
                    System.out.println(apartments + "apartments");

                    Gson gson = new Gson();
                    Type type = new TypeToken<List<ApartmentModel>>() {
                    }.getType();
                    List<ApartmentModel> list = gson.fromJson(jsonObject.getJSONArray("apartments").toString(), type);

                    ArrayList<ApartmentModel> arrModel = new ArrayList<>();
                    arrModel.addAll(list);

                    ArrayAdapter<ApartmentModel> adapter = new ArrayAdapter<>(getActivity(),
                            R.layout.spinner_text, arrModel);

                    spinner_apartment.setAdapter(adapter);
                    spinner_apartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            apartmentNo = arrModel.get(i).getId();

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


}
