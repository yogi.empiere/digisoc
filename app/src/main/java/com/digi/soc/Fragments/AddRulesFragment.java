package com.digi.soc.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.RulesModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.digi.soc.Fragments.AddRulesFragment.*;

@SuppressWarnings("ConstantConditions")
public class AddRulesFragment extends BaseFragment  {

    @BindView(R.id.edtRule)
    EditMedium edtRule;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    private String rules = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_society_rules, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, "Add Rules", false);

        bindWidget();

        return view;
    }

    private void bindWidget() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    addRules();
                }

            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

    }

    private void addRules() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("description", rules);

        new NetworkCall().callPostJSON(getActivity(), Services.SOCIETY_RULES,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");

                               /* JSONObject society_rule = jsonObject.getJSONObject("society_rule");
                                int id = society_rule.getInt("id");
                                String description = society_rule.getString("description");

                                RulesModel rulesModel = new RulesModel();
                                rulesModel.setId(id);
                                rulesModel.setDescription(description);

                                MyConstants.rulesModels.add(rulesModel);*/

                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                    edtRule.getText().clear();

                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });
    }

    private boolean checkValidation() {
        boolean isError = false;

        rules = edtRule.getText().toString().trim();

        if (TextUtils.isEmpty(rules)) {
            isError = true;
            edtRule.requestFocus();
            edtRule.setError(getResources().getString(R.string.blank_rule_title));
        }
        return !isError;
    }
    @Override
    public void onDestroy() {
        Fragment fragment = new SocietyTabFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "Society");
        bundle.putInt("selTabPos", 1);
        fragment.setArguments(bundle);
        MainActivity.changeFragment(getContext(), fragment, true);
        super.onDestroy();
    }



}
