package com.digi.soc.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.digi.soc.Adapters.SelectedWingsAdapter;
import com.digi.soc.Adapters.WingsSelAdapter;
import com.digi.soc.Helpers.ItemOffsetDecoration;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.MySharedPref;
import com.digi.soc.Helpers.OnWingDeleted;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.HomeModel;
import com.digi.soc.Models.NoticeModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.SocietyTabActivity;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

@SuppressWarnings("ConstantConditions")
public class AddNoticeFragment extends BaseFragment {

    @BindView(R.id.btnAddNotice)
    Button btnAddNotice;

    @BindView(R.id.ivSelImage)
    ImageView ivSelImage;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.btnSelectWing)
    Button btnSelectWing;

    @BindView(R.id.edtTitle)
    EditText edtTitle;

    @BindView(R.id.edtNoticeDesc)
    EditText edtNoticeDesc;

    @BindView(R.id.layoutWings)
    LinearLayout layoutWings;

    @BindView(R.id.tvSelectAll)
    TextView tvSelectAll;

    @BindView(R.id.btnAddW)
    Button btnAddW;

    @BindView(R.id.btnCancelW)
    Button btnCancelW;

    @BindView(R.id.rvWing)
    RecyclerView rvWing;

    @BindView(R.id.ivRoundImage)
    ImageView ivRoundImage;

    @BindView(R.id.rlImageview)
    RelativeLayout rlImageview;

    @BindView(R.id.ll1)
    LinearLayout ll1;
    private JSONObject noticeData, created_by;

    ArrayList<WingsModel> wingSelList = new ArrayList<>();
    private ArrayList<Integer> selectedWings = new ArrayList<>();

    private File destination;
    private boolean isSelecAll = true;
    private WingsSelAdapter adapter;
    private ArrayList<WingsModel> datalist = new ArrayList<>();
    private ArrayList<Integer> wingsArray = new ArrayList();
    private String noticeTitle = "";
    private String noticeDesc = "";
    private View view;

    private Uri selectedUri;
    private String filePathFirst = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_add_notice, container, false);


        ButterKnife.bind(this, view);
        MainActivity.setBottomPos(getContext(), -1);
        setTop(false, getString(R.string.addnotice), false);

        for (WingsModel model : MyConstants.wingReqData) {
            wingSelList.add(model);
        }

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen._7sdp, R.dimen._5sdp);
        rvWing.addItemDecoration(itemDecoration);
        rvWing.setNestedScrollingEnabled(false);

        bindWidget();
        Utils.hideSoftKeyboard(getActivity());
        setData();
        return view;
    }

    private void bindWidget() {
      btnAddNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    uploadNotice();
                }
            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

        btnSelectWing.setOnClickListener(view1 -> {
            Utils.hideSoftKeyboard(getActivity());
            btnSelectWing.setVisibility(View.GONE);
            layoutWings.setVisibility(View.VISIBLE);

            tvSelectAll.setVisibility(View.VISIBLE);
            btnAddW.setVisibility(View.VISIBLE);
            btnCancelW.setVisibility(View.VISIBLE);

            if (wingSelList.size() == 0 || wingSelList.size() < MyConstants.wingReqData.size()) {
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                isSelecAll = false;
            } else {
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                isSelecAll = true;
            }
            // getWingsData();
            setData();
        });

        btnCancelW.setOnClickListener(view1 -> {
            Utils.hideSoftKeyboard(getActivity());

            for (WingsModel model : MyConstants.wingReqData) {
                model.setSelect(true);
            }
            isSelecAll = true;
            wingSelList.clear();
            for (WingsModel model : MyConstants.wingReqData) {
                wingSelList.add(model);
            }
            btnSelectWing.setVisibility(View.VISIBLE);
            btnSelectWing.setText(getResources().getString(R.string.select_wing));
            layoutWings.setVisibility(View.GONE);
        });

        tvSelectAll.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(getActivity());
            if (isSelecAll) {
                isSelecAll = false;
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                for (WingsModel model : MyConstants.wingReqData) {
                    model.setSelect(false);
                }
                adapter.notifyDataSetChanged();
            } else {
                isSelecAll = true;
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                for (WingsModel model : MyConstants.wingReqData) {
                    model.setSelect(true);
                }
                adapter.notifyDataSetChanged();
            }
        });

        btnAddW.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(getActivity());

            wingSelList.clear();

            for (int x = 0; x < MyConstants.wingReqData.size(); x++) {
                if (MyConstants.wingReqData.get(x).isSelect())
                    wingSelList.add(MyConstants.wingReqData.get(x));
            }

            if (wingSelList.size() > 0) {

                btnSelectWing.setVisibility(View.VISIBLE);
                btnSelectWing.setText(getResources().getString(R.string.edit_wing));
                layoutWings.setVisibility(View.VISIBLE);

                rvWing.setAdapter(new SelectedWingsAdapter(getContext(), wingSelList, new OnWingDeleted() {
                    @Override
                    public void onWingDeleteListner() {
                        if (wingSelList.size() == 0) {
                            btnSelectWing.setVisibility(View.VISIBLE);
                            layoutWings.setVisibility(View.GONE);

                            for (WingsModel model : MyConstants.wingReqData) {
                                model.setSelect(false);
                            }

                        } else {
                            for (WingsModel modelDatalist : MyConstants.wingReqData) {
                                boolean isMatch = false;
                                for (WingsModel modelSelWings : wingSelList) {
                                    if (modelDatalist.getName().equals(modelSelWings.getName())) {
                                        isMatch = true;
                                    }
                                    if (wingSelList.get(wingSelList.size() - 1).equals(modelSelWings)) {
                                        if (isMatch)
                                            modelDatalist.setSelect(true);
                                        else
                                            modelDatalist.setSelect(false);
                                    }
                                }
                            }
                        }
                    }
                }));
                rvWing.setLayoutManager(new GridLayoutManager(getContext(), 2));

                btnAddW.setVisibility(View.GONE);
                btnCancelW.setVisibility(View.GONE);
                tvSelectAll.setVisibility(View.GONE);
            } else {
                Utility.showDialog(getContext(), getResources().getString(R.string.error), getResources().getString(R.string.blank_event_wings));
            }

        });
        
        rlImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showImagePickerChooser();

            }
        });

        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(getActivity());
            }
        });
    }

    /**
     * Open image picker..
     */
    private void showImagePickerChooser() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Utils.Check_STORAGE(getContext())) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MyConstants.CAMERA);

            } else
                CropImage.startPickImageActivity(getContext(), AddNoticeFragment.this);
        } else
            CropImage.startPickImageActivity(getContext(), AddNoticeFragment.this);
    }


    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start(getContext(), this);
    }


    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getContext(), data);
            if (CropImage.isReadExternalStoragePermissionsRequired(getContext(), imageUri)) {
                selectedUri = imageUri;
                filePathFirst = selectedUri.getPath().toString();
                destination = new File(filePathFirst);
                String strFileName = destination.getName();
                ivRoundImage.setImageURI(imageUri);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);

            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                ivRoundImage.setImageURI(result.getUri());
                selectedUri = result.getUri();
                filePathFirst = selectedUri.getPath().toString();
                destination = new File(filePathFirst);
                String strFileName = destination.getName();

                System.out.println("file path==" + strFileName);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MyConstants.CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    CropImage.startPickImageActivity(getContext(), AddNoticeFragment.this);

                } else {
                    Utils.showWarn(getActivity(), "Storage and Camera permission is required for upload profile image.");
                }
                break;
        }
    }

    private void addData() {

        if (datalist.size() > 0) {
            setData();
        } else {

            JsonObject jObj = new JsonObject();

            new NetworkCall(getContext(), Services.DASHBOARD,
                    jObj, true, NetworkCall.REQ_GET,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response, String requestCode) {
                    String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                    Log.e("code-->", requestCode);
                    if (requestCode.equals("200")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Gson gson = new Gson();
                            Type type = new TypeToken<List<WingsModel>>() {
                            }.getType();
                            List<WingsModel> list = gson.fromJson(jsonObject.getJSONObject("wings").getJSONArray("list").toString(), type);

                            ArrayList<WingsModel> arrModel = new ArrayList<>();
                            arrModel.addAll(list);

                            for (WingsModel d : arrModel) {
                                WingsModel wingsModel = new WingsModel();
                                wingsModel.setWings_Name(d.getName());
                                wingsModel.setWings_id(d.getId());
                                wingsModel.setSelect(false);
                                datalist.add(wingsModel);
                            }
                            setData();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    if (!returnResult.isEmpty()) {
                        return;
                    } else {
                        for (int i = 0; i < 6; i++) {
                            WingsModel wingsModel = new WingsModel();
                            wingsModel.setWings_Name("Wing " + i);
                            wingsModel.setSelect(false);
                            datalist.add(wingsModel);
                        }
                        setData();
                    }
                }
            });
        }
    }

    private void setData() {
        rvWing.setHasFixedSize(true);
        rvWing.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rvWing.setItemViewCacheSize(2);
        adapter = new WingsSelAdapter(getContext(), MyConstants.wingReqData, () -> {
            isSelecAll = false;
            tvSelectAll.setText(getResources().getString(R.string.select_all));
        });
        rvWing.setAdapter(adapter);
    }

    private boolean checkValidation() {
        boolean isError = false;
        noticeTitle = edtTitle.getText().toString();
        noticeDesc = edtNoticeDesc.getText().toString();

        selectedWings = new ArrayList<>();
        selectedWings.clear();
        for (WingsModel list : wingSelList) {
                if (list.isSelect()) {
                    selectedWings.add(list.getId());
                }
            }
        if (TextUtils.isEmpty(filePathFirst)) {
            Utils.showAlert(getString(R.string.blank_validation_image), getContext());
        }
        else if (TextUtils.isEmpty(noticeTitle)) {
            isError = true;
            edtTitle.requestFocus();
            edtTitle.setError(getResources().getString(R.string.blank_notice_title));
        } else if (TextUtils.isEmpty(noticeDesc)) {
            isError = true;
            edtNoticeDesc.requestFocus();
            edtNoticeDesc.setError(getResources().getString(R.string.blank_notice_desc));
        } else if (selectedWings.size() == 0) {
            isError = true;
            Utility.showDialog(getActivity(), getResources().getString(R.string.error),
                    getResources().getString(R.string.blank_notice_wings));
        }

        return !isError;
    }
    private void uploadNotice() {

        String auth = new MySharedPref(getActivity()).getString(MySharedPref.USER_AUTH, "");

        HashMap<String, String> wingsHashMap = new HashMap<String, String>();
        for (int i : selectedWings){
            wingsHashMap.put("wing_ids[]", "" + i);
        }
        AndroidNetworking.upload(Services.NOTICE)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/x-www-form-urlencoded")
                .addHeaders("Authorization", "Bearer " + auth)
                .addMultipartFile("notice_img", destination)
                .addMultipartParameter("notice_title", noticeTitle)
                .addMultipartParameter("notice_description", noticeDesc)
                .addMultipartParameter(wingsHashMap)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            JSONObject error = jsonObject.getJSONObject("error");
                            String message = error.getString("message");
                             setNoticeData(jsonObject);

                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                            alertDialog.setTitle("Success!");
                            alertDialog.setMessage(message);

                            alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                if (getActivity().getCurrentFocus() != null) {
                                    getActivity().getCurrentFocus().clearFocus();
                                }
                                getActivity().onBackPressed();
                            });
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    alertDialog.show();
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }



                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError ===>", String.valueOf(anError));
                    }
                });
    }
    private void setNoticeData(JSONObject jsonObject) throws JSONException {
        noticeData =  jsonObject.getJSONObject("notice");
        created_by =  noticeData.getJSONObject("created_by");

        ArrayList<String> roles;
        roles = (ArrayList<String>) new Gson().fromJson(created_by.getString("roles"),
                new TypeToken<ArrayList<String>>() {}.getType());
        ArrayList<WingsModel> wings;
        wings = (ArrayList<WingsModel>) new Gson().fromJson(noticeData.getString("wings"),
                new TypeToken<ArrayList<WingsModel>>() {}.getType());

        NoticeModel notice  = new NoticeModel(
                noticeData.getInt("id"),
                noticeData.getString("title"),
                noticeData.getString("description"),
                noticeData.getString("image"),
                new NoticeModel.CreatedBy(roles,
                        created_by.getInt("id"),
                        created_by.getString("first_name"),
                        created_by.getString("last_name"),
                        created_by.getString("email"),
                        created_by.getString("contact_no"),
                        created_by.getString("profile_pic")) ,
                noticeData.getString("created_at"),
                noticeData.getString("updated_at"),
                noticeData.getBoolean("isRead"),
                wings);

        MyConstants.noticeData.add(notice);
    }
}

