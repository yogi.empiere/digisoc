package com.digi.soc.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.MySharedPref;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Permissions;
import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

@SuppressWarnings("ConstantConditions")
public class AddFeedFragment extends BaseFragment {

    @BindView(R.id.edtFeed)
    EditMedium edtFeed;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    private File destination;

    @BindView(R.id.ivSelImage)
    ImageView ivSelImage;

   @BindView(R.id.ivRoundImage)
    ImageView ivRoundImage;

    private Uri selectedUri;
    private String filePathFirst = "";
    private String feed ;

    @BindView(R.id.rlImageview)
    RelativeLayout rlImageview;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_post, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, "Add Feed", false);

        bindWidget();

        return view;
    }

    private void bindWidget() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(checkValidation()) {
                   uploadFeed();
               }
            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

        rlImageview.setOnClickListener(view1 -> {
            showImagePickerChooser();
        });

    }
    /**
     * Open image picker..
     */
    private void showImagePickerChooser() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Utils.Check_STORAGE(getContext())) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MyConstants.CAMERA);

            } else
                CropImage.startPickImageActivity(getContext(), AddFeedFragment.this);
        } else
            CropImage.startPickImageActivity(getContext(), AddFeedFragment.this);
    }


    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start(getContext(), this);
    }


    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getContext(), data);
            if (CropImage.isReadExternalStoragePermissionsRequired(getContext(), imageUri)) {
                selectedUri = imageUri;
                filePathFirst = selectedUri.getPath().toString();
                destination = new File(filePathFirst);
                String strFileName = destination.getName();
                ivRoundImage.setImageURI(imageUri);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);

            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                ivRoundImage.setImageURI(result.getUri());
                selectedUri = result.getUri();
                filePathFirst = selectedUri.getPath().toString();
                destination = new File(filePathFirst);
                String strFileName = destination.getName();

                System.out.println("file path==" + strFileName);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MyConstants.CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    CropImage.startPickImageActivity(getContext(), AddFeedFragment.this);

                } else {
                    Utils.showWarn(getActivity(), "Storage and Camera permission is required for upload profile image.");
                }
                break;
        }
    }

    private void uploadFeed() {
        String auth = new MySharedPref(getActivity()).getString(MySharedPref.USER_AUTH, "");

        AndroidNetworking.upload(Services.CREATE_FEED)
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer " + auth)
                .addMultipartFile("feed_img", destination)
                .addMultipartParameter("feed_description", feed)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            JSONObject error = jsonObject.getJSONObject("error");
                            String message = error.getString("message");
                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                            alertDialog.setTitle("Success!");
                            alertDialog.setMessage(message);

                            alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                edtFeed.getText().clear();

                                if (getActivity().getCurrentFocus() != null) {
                                    getActivity().getCurrentFocus().clearFocus();
                                }
                                getActivity().onBackPressed();
                            });
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    alertDialog.show();
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError ===>", String.valueOf(anError));
                    }
                });
    }
    private boolean checkValidation() {
        boolean isError = false;

        feed = edtFeed.getText().toString().trim();

        if (TextUtils.isEmpty(filePathFirst)) {
            Utils.showAlert(getString(R.string.blank_validation_image), getContext());
        }else if (TextUtils.isEmpty(feed)) {
            isError = true;
            edtFeed.requestFocus();
            edtFeed.setError(getResources().getString(R.string.blank_feed_title));
        }
        return !isError;
    }
}
