package com.digi.soc.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.digi.soc.Adapters.SelectedWingsAdapter;
import com.digi.soc.Adapters.WingsSelAdapter;
import com.digi.soc.Helpers.ItemOffsetDecoration;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.MySharedPref;
import com.digi.soc.Helpers.OnWingDeleted;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.AlarmModel;
import com.digi.soc.Models.ComplainModel;
import com.digi.soc.Models.NoticeModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

@SuppressWarnings("ConstantConditions")
public class AddComplainFragment extends BaseFragment {

    @BindView(R.id.btnAddComplain)
    Button btnAddComplain;

    @BindView(R.id.ivSelImage)
    ImageView ivSelImage;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.edtTitle)
    EditText edtTitle;

    @BindView(R.id.edtComplainDesc)
    EditText edtComplainDesc;

    @BindView(R.id.ivRoundImage)
    ImageView ivRoundImage;

    @BindView(R.id.rlImageview)
    RelativeLayout rlImageview;

    private Uri selectedUri;
    private String filePathFirst = "";
    private File destination;
    private JSONObject complainData, created_by;
    private JSONArray complaint_status;

    private String complainTitle = "";
    private String complainDesc = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_complain, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, getString(R.string.addComplain), false);

        bindWidget();
        return view;
    }

    private void bindWidget() {
        btnAddComplain.setOnClickListener(v -> {
            if (checkValidation()) {
                uploadComplain();
            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

        rlImageview.setOnClickListener(view1 -> {
            showImagePickerChooser();
        });

    }

    /**
     * Open image picker..
     */
    private void showImagePickerChooser() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Utils.Check_STORAGE(getContext())) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MyConstants.CAMERA);

            } else
                CropImage.startPickImageActivity(getContext(), AddComplainFragment.this);
        } else
            CropImage.startPickImageActivity(getContext(), AddComplainFragment.this);
    }


    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start(getContext(), this);
    }


    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getContext(), data);
            if (CropImage.isReadExternalStoragePermissionsRequired(getContext(), imageUri)) {
                selectedUri = imageUri;
                filePathFirst = selectedUri.getPath().toString();
                destination = new File(filePathFirst);
                String strFileName = destination.getName();
                ivRoundImage.setImageURI(imageUri);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);

            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                ivRoundImage.setImageURI(result.getUri());
                selectedUri = result.getUri();
                filePathFirst = selectedUri.getPath().toString();
                destination = new File(filePathFirst);
                String strFileName = destination.getName();

                System.out.println("file path==" + strFileName);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MyConstants.CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    CropImage.startPickImageActivity(getContext(), AddComplainFragment.this);

                } else {
                    Utils.showWarn(getActivity(), "Storage and Camera permission is required for upload profile image.");
                }
                break;
        }
    }

    private boolean checkValidation() {
        boolean isError = false;

        complainTitle = edtTitle.getText().toString();
        complainDesc = edtComplainDesc.getText().toString();

        if (TextUtils.isEmpty(filePathFirst)) {
            Utils.showAlert(getString(R.string.blank_validation_image), getContext());
        }else if (TextUtils.isEmpty(complainTitle)) {
            isError = true;
            edtTitle.requestFocus();
            edtTitle.setError(getResources().getString(R.string.blank_complain_title));
        } else if (TextUtils.isEmpty(complainDesc)) {
            isError = true;
            edtComplainDesc.requestFocus();
            edtComplainDesc.setError(getResources().getString(R.string.blank_complain_desc));
        }
        return !isError;
    }

    private void uploadComplain() {
        String auth = new MySharedPref(getActivity()).getString(MySharedPref.USER_AUTH, "");
        AndroidNetworking.upload(Services.COMPLAIN)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/x-www-form-urlencoded")
                .addHeaders("Authorization", "Bearer " + auth)
                .addMultipartFile("complaint_img", destination)
                .addMultipartParameter("complaint_title", complainTitle)
                .addMultipartParameter("complaint_description", complainDesc)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            JSONObject error = jsonObject.getJSONObject("error");
                            String message = error.getString("message");

                            setComplainData(jsonObject);

                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                            alertDialog.setTitle("Success!");
                            alertDialog.setMessage(message);

                            alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                if (getActivity().getCurrentFocus() != null) {
                                    getActivity().getCurrentFocus().clearFocus();
                                }
                                getActivity().onBackPressed();
                            });
                            getActivity().runOnUiThread(() -> alertDialog.show());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError ===>", String.valueOf(anError));
                    }
                });
    }

    private void setComplainData(JSONObject jsonObject) throws JSONException {
        complainData = jsonObject.getJSONObject("complaint");
        created_by = complainData.getJSONObject("created_by");
        created_by = complainData.getJSONObject("created_by");
        JSONObject wing = complainData.getJSONObject("wing");
     //   complaint_status = complainData.getJSONArray("complaint_status");

        ArrayList<String> roles;
        roles = (ArrayList<String>) new Gson().fromJson(created_by.getString("roles"),
                new TypeToken<ArrayList<String>>() {}.getType());

        ArrayList<ComplainModel.complaint_status> complaintStatuses;
        /*complaintStatuses = (ArrayList<ComplainModel.complaint_status>)
                new Gson().fromJson(complainData.getString("complaint_status"),
                new TypeToken<ArrayList<WingsModel>>() {}.getType());*/
       /* ArrayList<WingsModel> wings;
        wings = (ArrayList<WingsModel>) new Gson().fromJson(complainData.getString("wing"),
                new TypeToken<ArrayList<WingsModel>>() {}.getType());*/

        ComplainModel.WingDetail wingDetail = new ComplainModel.WingDetail(
                wing.getInt("id"),
                wing.getString("name"),
                wing.getString("no_of_floors"));

        ComplainModel complain = new ComplainModel(
                complainData.getInt("id"),
                complainData.getString("title"),
                complainData.getString("description"),
                complainData.getInt("current_status"),
                complainData.getString("image"),
                new ComplainModel.CreatedBy(
                        created_by.getInt("id"),
                        created_by.getString("first_name"),
                        created_by.getString("last_name"),
                        created_by.getString("email"),
                        created_by.getString("contact_no"),
                        created_by.getString("profile_pic"),
                        roles),
                complainData.getString("created_at"),
                complainData.getString("updated_at"),
                null,wingDetail);

        MyConstants.complainData.add(complain);
    }
}
