package com.digi.soc.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digi.soc.Adapters.TopAdapter;
import com.digi.soc.MainActivity;
import com.digi.soc.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeopleFragment extends BaseFragment {

    @BindView(R.id.rvTop)
    RecyclerView rvTop;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), 2);

        setTop(false, "People", false);

        rvTop.setHasFixedSize(true);
        rvTop.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rvTop.setNestedScrollingEnabled(false);

        setTopList();

        return view;
    }

    private void setTopList() {
        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<Integer> iconList = new ArrayList<>();

        nameList.add("Member");
        iconList.add(R.drawable.ic_member);

        nameList.add("Worker");
        iconList.add(R.drawable.ic_worker);

        nameList.add("Vendor");
        iconList.add(R.drawable.ic_vendor);

        nameList.add("Security");
        iconList.add(R.drawable.ic_security);

        nameList.add("Staff");
        iconList.add(R.drawable.ic_staff);

        TopAdapter topAdapter = new TopAdapter(getContext(), nameList, iconList, 0, "People");
        rvTop.setAdapter(topAdapter);
    }
}
