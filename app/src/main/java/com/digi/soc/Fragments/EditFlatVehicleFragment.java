package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class EditFlatVehicleFragment extends BaseFragment {

    @BindView(R.id.spinner_VehicleType)
    Spinner spinner_VehicleType;

    @BindView(R.id.edtVehicleNum)
    EditMedium edtVehicleNum;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    private String vehicleNo ="";
    private String vehicleType ="";
    private String vehicleNoFormat ="^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$";

    List<String> vehicleList = new ArrayList<String>();
    ArrayAdapter<String> vehicleAdapter;
    private int vehicleId ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_flat_vehicles, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, getString(R.string.edit_vehiclel), false);

        vehicleId = this.getArguments().getInt("vehicleId");
        bindWidget();
        vehicleAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, vehicleList);
        spinner_VehicleType.setAdapter(vehicleAdapter);
        getMemberVehicleType();
        getVehicleData(vehicleId);
        return view;
    }



    private void bindWidget() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    editVehicle();
                }
            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

    }

    private void getMemberVehicleType() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.GET_MEMBER_VEHICLE_TYPE ,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject vehicleTypeList = jsonObject.getJSONObject("vehicle_types");
                        String TWO_WHEELS = vehicleTypeList.getString("TWO_WHEELS");
                        String FOUR_WHEELS = vehicleTypeList.getString("FOUR_WHEELS");

                        vehicleList.add(getString(R.string.select_vehicle_type));
                        vehicleList.add(TWO_WHEELS);
                        vehicleList.add(FOUR_WHEELS);

                        vehicleAdapter.notifyDataSetChanged();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        }); }

    private void editVehicle() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;

        jObj.addProperty("type",vehicleType );
        jObj.addProperty("number", vehicleNo);

        new NetworkCall().callPostJSON(getActivity(), Services.CREATE_MEMBER_VEHICLE +"/"+vehicleId,
                jObj, true, NetworkCall.REQ_PUT,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");

                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {


                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });

    }

    private boolean checkValidation() {
        boolean isError = false;

        vehicleNo = edtVehicleNum.getText().toString().trim();
        vehicleType= spinner_VehicleType.getSelectedItem().toString();
      if (vehicleType.equals(getString(R.string.select_vehicle_type))) {
            isError = true;
          Utility.showDialog(getActivity(),
                  getResources().getString(R.string.error),
                  getResources().getString(R.string.blank_vehicle_type));
        }
       else if (TextUtils.isEmpty(vehicleNo)) {
            isError = true;
            edtVehicleNum.requestFocus();
            edtVehicleNum.setError(getResources().getString(R.string.blank_vehicle_no));
        }
      else if (!vehicleNo.matches(vehicleNoFormat)) {
          isError = true;
          edtVehicleNum.requestFocus();
          edtVehicleNum.setError(getResources().getString(R.string.blank_vehicle_type_no));
      }

        return !isError;
    }

    private void getVehicleData(int vehicleId) {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.GET_MEMBER_VEHICLE + "/"+vehicleId,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject vehicle = jsonObject.getJSONObject("vehicle");
                        String type = vehicle.getString("type");
                        String number = vehicle.getString("number");

                        edtVehicleNum.setText(number);

                        for (int i = 0; i < vehicleList.size(); i++) {
                            if (type.equals(vehicleList.get(i))) {
                                spinner_VehicleType.setSelection(i);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });

    }
}
