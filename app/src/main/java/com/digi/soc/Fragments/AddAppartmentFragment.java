package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.digi.soc.Controls.ButtonMedium;
import com.digi.soc.Helpers.JsonParserUniversal;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.gson.JsonObject;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class AddAppartmentFragment extends BaseFragment {

    @BindView(R.id.btnAddAppartment)
    ButtonMedium btnAddAppartment;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.edtAppartmentNo)
    EditText edtAppartmentNo;

    @BindView(R.id.edtcurrentTenantNo)
    EditText edtcurrentTenantNo;

    @BindView(R.id.edtFloorNo)
    EditText edtFloorNo;

    @BindView(R.id.edtOwnedBy)
    EditText edtOwnedBy;

    @BindView(R.id.rg_rent)
    RadioGroup rg_rent;

    @BindView(R.id.rd_yes)
    MaterialRadioButton rd_yes;

    @BindView(R.id.rd_no)
    MaterialRadioButton rd_no;

    private boolean selectedType ;
    private int wingId ;
    private String appartmentNo = "";
    private String currentTenantNo = "";
    private String floorNo = "";
    private String ownedBy = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_appartment, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, getString(R.string.add_appartment), false);
        wingId = this.getArguments().getInt("WingId");
        bindWidget();
        return view;
    }

    private void bindWidget() {
        btnAddAppartment.setOnClickListener(v -> {
            if (checkValidation()) {
                addAppartment();
            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

        rg_rent.setOnCheckedChangeListener((radioGroup, i) -> {
            if (i == R.id.rd_yes) {
                selectedType = true;
            } else if (i == R.id.rd_no) {
                selectedType =false;
            }
        });
    }

    private boolean checkValidation() {
        boolean isError = false;
        appartmentNo = edtAppartmentNo.getText().toString().trim();
        currentTenantNo = edtcurrentTenantNo.getText().toString().trim();
        floorNo = edtFloorNo.getText().toString().trim();
        ownedBy = edtOwnedBy.getText().toString().trim();

        if (TextUtils.isEmpty(appartmentNo)) {
            isError = true;
            edtAppartmentNo.requestFocus();
            edtAppartmentNo.setError(getResources().getString(R.string.blank_appartment_no));
        }else if (TextUtils.isEmpty(currentTenantNo)) {
            isError = true;
            edtcurrentTenantNo.requestFocus();
            edtcurrentTenantNo.setError(getResources().getString(R.string.blank_currentTenant_no));
        }else if (TextUtils.isEmpty(floorNo)) {
            isError = true;
            edtFloorNo.requestFocus();
            edtFloorNo.setError(getResources().getString(R.string.blank_floor_no));
        }else if (TextUtils.isEmpty(ownedBy)) {
            isError = true;
            edtOwnedBy.requestFocus();
            edtOwnedBy.setError(getResources().getString(R.string.blank_floor_no));
        }

        return !isError;
    }

    private void addAppartment() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("apartment_no", appartmentNo);
        jObj.addProperty("current_tenant", currentTenantNo);
        jObj.addProperty("owned_by", ownedBy);
        jObj.addProperty("isRented", selectedType);
        jObj.addProperty("floor_no", floorNo);
        jObj.addProperty("wing_id", String.valueOf(wingId));

        new NetworkCall().callPostJSON(getActivity(), Services.APARTMENT,
                jObj, true, NetworkCall.REQ_POST,
                true, (response, requestCode) -> {
                    String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                    Log.e("code-->", requestCode);
                    if (requestCode.equals("200")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject error = jsonObject.getJSONObject("error");
                            String message = error.getString("message");

                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                            alertDialog.setTitle("Success!");
                            alertDialog.setMessage(message);
                            alertDialog.setPositiveButton("Ok", (dialog, which) -> {

                                if (getActivity().getCurrentFocus() != null) {
                                    getActivity().getCurrentFocus().clearFocus();
                                }
                                getActivity().onBackPressed();
                            });
                            getActivity().runOnUiThread(() -> alertDialog.show());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (!returnResult.isEmpty()) {
                        return;
                    }
                });
    }
}
