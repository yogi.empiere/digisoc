package com.digi.soc.Fragments;


import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.digi.soc.Adapters.FlatMemberContactAdapter;
import com.digi.soc.Adapters.FlatMemberVehicleAdapter;
import com.digi.soc.Adapters.PollAdapter;
import com.digi.soc.Adapters.VisitorAdapter;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.PollModel;
import com.digi.soc.Models.VehicleModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FlatFragment extends BaseFragment {
    //implements AppBarLayout.OnOffsetChangedListener
    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;

    @BindView(R.id.rvMemberDetail)
    RecyclerView rvMemberDetail;

    @BindView(R.id.rvVehicleDetail)
    RecyclerView rvVehicleDetail;

    @BindView(R.id.rvVisitorDetail)
    RecyclerView rvVisitorDetail;

    @BindView(R.id.ll1)
    LinearLayout ll1;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbar_layout;

    @BindView(R.id.expandedImage)
    ImageView expandedImage;

    @BindView(R.id.tool_bar)
    Toolbar tool_bar;

    @BindView(R.id.nestedView)
    NestedScrollView nestedView;

    @BindView(R.id.ivAddVehicle)
    ImageView ivAddVehicle;


    private Animation zoomAnimation;

    ArrayList<VehicleModel> vehicleModels = new ArrayList<>();

    public FlatFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_flat, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), 4);

        setTop(false, getString(R.string.myFlat), false);

        zoomEffect();
        view();
        init();
        return view;
    }

    private void init() {
        ivAddVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.changeFragment(getContext(), new AddFlatVehicleFragment(), true);
                  }
        });
    }

    private void zoomEffect() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.zoom_in);
                    expandedImage.startAnimation(animation);
                    expandedImage.setVisibility(View.GONE);
                    scaleView(expandedImage,10f,50f);
                } else if (isShow) {
                    Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.zoom_out);
                    expandedImage.startAnimation(animation);
                    expandedImage.setVisibility(View.VISIBLE);
                    expandedImage.setVisibility(View.VISIBLE);
                    isShow = false;
                }
            }
        });

    }
    
    public void scaleView(View v, float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(1000);
        v.startAnimation(anim);
    }

    private void view() {
        rvMemberDetail.setHasFixedSize(true);
        rvMemberDetail.setLayoutManager(new LinearLayoutManager(getContext()));
        rvMemberDetail.setAdapter(new FlatMemberContactAdapter(getContext(), new ArrayList<>()));

        setVehicleView();

        rvVisitorDetail.setHasFixedSize(true);
        rvVisitorDetail.setLayoutManager(new LinearLayoutManager(getContext()));
        rvVisitorDetail.setAdapter(new VisitorAdapter(getContext(), new ArrayList<>()));

    }

    private void setVehicleView() {
        rvVehicleDetail.setHasFixedSize(true);
        rvVehicleDetail.setLayoutManager(new LinearLayoutManager(getContext()));
        getMemberVehicleList();
    }

    private void getMemberVehicleList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.GET_MEMBER_VEHICLE ,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray vehicleDataList = jsonObject.getJSONArray("list");

                       vehicleModels = (ArrayList<VehicleModel>)
                                new Gson().fromJson(String.valueOf(vehicleDataList),
                                        new TypeToken<ArrayList<VehicleModel>>() {
                                        }.getType());

                        FlatMemberVehicleAdapter FlatMemberVehicleAdapter = new FlatMemberVehicleAdapter(getContext(), vehicleModels);
                        rvVehicleDetail.setAdapter(FlatMemberVehicleAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        }); }
}



