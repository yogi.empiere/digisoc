package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.digi.soc.Adapters.EmergencyAdapter;
import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class EditSocietyEmergencyContactFragment extends BaseFragment {
    @BindView(R.id.edtname)
    EditMedium edtname;

    @BindView(R.id.edtno)
    EditMedium edtno;

    @BindView(R.id.btnAdd)
    Button btnUpdate;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    private String name = "";
    private String no = "";
    private int emergencyId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_society_emergency_contact, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, "Edit Society Emergency Contact", false);

        emergencyId = this.getArguments().getInt("emergencyId");
        btnUpdate.setText("Update");

        editContact();
        bindWidget();

        return view;
    }

    private void bindWidget() {
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    updateContact();
                }
            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

    }

    private boolean checkValidation() {
        boolean isError = false;

        name = edtname.getText().toString().trim();
        no = edtno.getText().toString().trim();

        if (TextUtils.isEmpty(name)) {
            isError = true;
            edtname.requestFocus();
            edtname.setError(getResources().getString(R.string.blank_emergency_name));
        } else if (TextUtils.isEmpty(no)) {
            isError = true;
            edtno.requestFocus();
            edtno.setError(getResources().getString(R.string.blank_emergency_no));
        }
        return !isError;
    }

    private void editContact() {

        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.SOCIETY_EMERGENCY + "/" + emergencyId,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject emergency_contact = jsonObject.getJSONObject("emergency_contact");
                        String name = emergency_contact.getString("name");
                        String contact_no = emergency_contact.getString("contact_no");
                        edtname.setText(name);
                        edtno.setText(contact_no);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void updateContact() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("name", name);
        jObj.addProperty("contact_no", no);

        new NetworkCall().callPostJSON(getContext(), Services.SOCIETY_EMERGENCY + "/" + emergencyId,
                jObj, true, NetworkCall.REQ_PUT,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                    getActivity().onBackPressed();
                                });

                                alertDialog.show();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });

    }
}

