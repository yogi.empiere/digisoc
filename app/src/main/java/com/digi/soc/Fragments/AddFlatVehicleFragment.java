package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableRow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.digi.soc.Adapters.FlatMemberVehicleAdapter;
import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.VehicleModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class AddFlatVehicleFragment extends BaseFragment {

    @BindView(R.id.spinner_VehicleType)
    Spinner spinner_VehicleType;

    @BindView(R.id.edtVehicleNum)
    EditMedium edtVehicleNum;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    private String vehicleNo ="";
    private String vehicleType ="";
   private String vehicleNoFormat ="^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$";
    private int DIVIDER_COUNT = 0;
    List<String> vehicleList = new ArrayList<String>();
    ArrayAdapter<String> vehicleAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_flat_vehicles, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, getString(R.string.add_vehiclel), false);

        bindWidget();
        vehicleAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, vehicleList);
        spinner_VehicleType.setAdapter(vehicleAdapter);

        getMemberVehicleType();
        return view;
    }

    private void bindWidget() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    addVehicle();
                }
            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

     /*  edtVehicleNum.addTextChangedListener(new TextWatcher() {

            private static final int TOTAL_SYMBOLS = 13; // size of pattern MP-09-AB-1234
            private static final int TOTAL_DIGITS = 10; // max numbers of digits in pattern: 0000 x 4
            private static final int DIVIDER_MODULO = 3; // means divider position is every 5th symbol beginning with 1
            private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
            private static final char DIVIDER = ' ';

           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {

           }

           @Override
           public void afterTextChanged(Editable s) {
               if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
                   if(DIVIDER_COUNT < 3)
                   {
                       s.replace(0, s.length(), buildCorrectString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
                   }
                   DIVIDER_COUNT++ ;
               }

           }
            private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
                boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
                for (int i = 0; i < s.length(); i++) { // check that every element is right
                    if (i > 0 && (i + 1) % dividerModulo == 0) {
                        isCorrect &= divider == s.charAt(i);
                    } else {
                        isCorrect &= true;
                    }
                }
                return isCorrect;
            }

            private String buildCorrectString(char[] digits, int dividerPosition, char divider) {
                final StringBuilder formatted = new StringBuilder();

                for (int i = 0; i < digits.length; i++) {
                    if (digits[i] != 0) {
                        formatted.append(digits[i]);
                        if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
                            formatted.append(divider);
                        }
                    }
                }

                return formatted.toString();
            }

            private char[] getDigitArray(final Editable s, final int size) {
                char[] digits = new char[size];
                int index = 0;
                for (int i = 0; i < s.length() && index < size; i++) {
                    char current = s.charAt(i);
                    if (Character.isDigit(current)) {
                        digits[index] = current;
                        index++;
                    }
                }
                return digits;
            }
       });*/
    }

    private void getMemberVehicleType() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getContext(), Services.GET_MEMBER_VEHICLE_TYPE ,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject vehicleTypeList = jsonObject.getJSONObject("vehicle_types");
                        String TWO_WHEELS = vehicleTypeList.getString("TWO_WHEELS");
                        String FOUR_WHEELS = vehicleTypeList.getString("FOUR_WHEELS");

                        vehicleList.add(getString(R.string.select_vehicle_type));
                        vehicleList.add(TWO_WHEELS);
                        vehicleList.add(FOUR_WHEELS);

                        vehicleAdapter.notifyDataSetChanged();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        }); }

    private void addVehicle() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;

        jObj.addProperty("type",vehicleType );
        jObj.addProperty("number", vehicleNo);

        new NetworkCall().callPostJSON(getActivity(), Services.CREATE_MEMBER_VEHICLE,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");

                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {

                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });

    }

    private boolean checkValidation() {
        boolean isError = false;

        vehicleNo = edtVehicleNum.getText().toString().trim();
        vehicleType= spinner_VehicleType.getSelectedItem().toString();
      if (vehicleType.equals(getString(R.string.select_vehicle_type))) {
            isError = true;
          Utility.showDialog(getActivity(),
                  getResources().getString(R.string.error),
                  getResources().getString(R.string.blank_vehicle_type));
        }
       else if (TextUtils.isEmpty(vehicleNo)) {
            isError = true;
            edtVehicleNum.requestFocus();
            edtVehicleNum.setError(getResources().getString(R.string.blank_vehicle_no));
        }
      else if (!vehicleNo.matches(vehicleNoFormat)) {
          isError = true;
          edtVehicleNum.requestFocus();
          edtVehicleNum.setError(getResources().getString(R.string.blank_vehicle_type_no));
      }

        return !isError;
    }
}
