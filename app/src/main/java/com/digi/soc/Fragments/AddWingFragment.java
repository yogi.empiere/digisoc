package com.digi.soc.Fragments;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.MySharedPref;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

@SuppressWarnings("ConstantConditions")
public class AddWingFragment extends BaseFragment {

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.edtWingName)
    EditMedium edtWingName;

    @BindView(R.id.edtFloorNo)
    EditMedium edtFloorNo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_wing, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, getString(R.string.addWing), false);
        bindWidget();
        return view;
    }

    private void bindWidget() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SocietyTabFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type", "Society");
                bundle.putInt("selTabPos",0);
                fragment.setArguments(bundle);
                MainActivity.changeFragment(getContext(), fragment, false);
            }
        });

        //btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addWing();
            }

        });
    }

    private void addWing() {
        String wingName = edtWingName.getText().toString().trim();
        String floorNo = edtFloorNo.getText().toString().trim();

        if (TextUtils.isEmpty(wingName)) {
            edtWingName.requestFocus();
            edtWingName.setError("Please Enter Wing Name");
        } else if (TextUtils.isEmpty(floorNo)) {
            edtFloorNo.requestFocus();
            edtFloorNo.setError("Please Enter Number of floor ");
        } else {
            if (getActivity().getCurrentFocus() != null) {
                getActivity().getCurrentFocus().clearFocus();
            }
            JsonObject jObj = new JsonObject();
            MyConstants.isResponceJsonOBJ = true;
            jObj.addProperty("name", wingName);
            jObj.addProperty("no_of_floors", floorNo);

            new NetworkCall().callPostJSON(getActivity(), Services.CREATE_WING,
                    jObj, true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                        @Override
                        public void myResponseMethod(String response, String requestCode) {
                            String returnResult = Utils.handleAPIResponceCode(getActivity(), requestCode, response);

                            if (requestCode.equals("200")) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String message = jsonObject.getString("message");

                                    JSONObject wing = jsonObject.getJSONObject("wing");
                                    int id = wing.getInt("id");
                                    String name = wing.getString("name");
                                    int no_of_floors = wing.getInt("no_of_floors");

                                    WingsModel wingsModel = new WingsModel();
                                    wingsModel.setId(id);
                                    wingsModel.setName(name);
                                    wingsModel.setNo_of_floors(no_of_floors);

                                    MyConstants.wingReqData.add(wingsModel);

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                                    alertDialog.setTitle("Success!");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                        edtWingName.getText().clear();
                                        edtFloorNo.getText().clear();
                                        if (getActivity().getCurrentFocus() != null) {
                                            getActivity().getCurrentFocus().clearFocus();
                                        }
                                        getActivity().onBackPressed();
                                    });

                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            alertDialog.show();
                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!returnResult.isEmpty()) {
                                return;
                            }
                        }
                    });
        }
    }

}
