package com.digi.soc.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.digi.soc.Helpers.Layout;
import com.digi.soc.MainActivity;
import com.digi.soc.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.digi.soc.MainActivity.changeFragment;

@SuppressWarnings("ConstantConditions")
public abstract class BaseFragment extends Fragment {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivAlarm)
    ImageView ivAlarm;

    @BindView(R.id.ivNoti)
    ImageView ivNoti;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ivUserProfile)
    ImageView ivUserProfile;

    @BindView(R.id.ivSetting)
    ImageView ivSetting;

    @BindView(R.id.tvNotiNo)
    TextView tvNotiNo;

    @BindView(R.id.tvAlarmNo)
    TextView tvAlarmNo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = null;
        if (getLayout() != null) {
            view = inflater.inflate(getLayout().value(), container, false);
            ButterKnife.bind(this, view);

        }
        ivBack.setOnClickListener(view1 -> getActivity().onBackPressed());


        getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        return view;
    }

    private Layout getLayout() {
        return getClass().getAnnotation(Layout.class);
    }

    void setTop(boolean isFull, String title, boolean isBack) {
        tvTitle.setText(title);
        if (isFull) {
            ivAlarm.setVisibility(View.VISIBLE);
            ivNoti.setVisibility(View.VISIBLE);
            tvNotiNo.setVisibility(View.VISIBLE);
            tvAlarmNo.setVisibility(View.VISIBLE);
            ivUserProfile.setVisibility(View.VISIBLE);
            ivSetting.setVisibility(View.VISIBLE);
            ivBack.setVisibility(View.GONE);
        } else {
            ivNoti.setVisibility(View.INVISIBLE);
            tvNotiNo.setVisibility(View.INVISIBLE);
            ivUserProfile.setVisibility(View.INVISIBLE);
            ivSetting.setVisibility(View.INVISIBLE);

            if (isBack) {
                ivBack.setVisibility(View.VISIBLE);
                ivAlarm.setVisibility(View.GONE);
                tvAlarmNo.setVisibility(View.GONE);
            } else {
                ivBack.setVisibility(View.GONE);
                ivAlarm.setVisibility(View.INVISIBLE);
                tvAlarmNo.setVisibility(View.INVISIBLE);
            }
        }
    }



}
