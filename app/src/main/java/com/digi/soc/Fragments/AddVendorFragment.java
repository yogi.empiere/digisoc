package com.digi.soc.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.digi.soc.Helpers.JsonParserUniversal;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.VendorModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.Models.WorkersModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ConstantConditions")
public class AddVendorFragment extends BaseFragment {

    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.edtContact)
    EditText edtContact;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.spinner_wing)
    Spinner spinner_wing;

    private int wingId = 0;
    private String name="";
    private String no="";

    private JsonParserUniversal jsonParserUniversal;

    public static ArrayList<WingsModel> wingModelList=new ArrayList<>();
    public static ArrayList<String> wingNameList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_people_workers, container, false);

        ButterKnife.bind(this, view);

        MainActivity.setBottomPos(getContext(), -1);

        setTop(false, "Add Vendor", false);
        jsonParserUniversal = new JsonParserUniversal();
        getWing();

        bindWidget();

        return view;
    }

    private void bindWidget() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    addVendor();
                }

            }
        });
        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

    }
    private void getWing() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(getActivity(), Services.GET_WINGS, jObj,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getActivity(), requestCode, response);
                if (returnResult.isEmpty())
                    return;

                try {
                    JSONObject jsonObject = new JSONObject(returnResult);
                    JSONArray wings = jsonObject.getJSONArray("wings");

                    wingModelList.clear();
                    wingNameList.clear();
                    WingsModel wingData = new WingsModel();
                    wingData.setName(getString(R.string.select_wing));
                    wingData.setId(0);
                    wingModelList.add(wingData);
                    wingNameList.add(wingData.getName());

                    for (int i = 0; i < wings.length(); i++) {
                        JSONObject object = wings.getJSONObject(i);
                        WingsModel wingsModel = (WingsModel) jsonParserUniversal.parseJson(object, new WingsModel());

                        if (wingsModel.getName() != null && !wingsModel.getName().toLowerCase().equals("null")) {
                            wingNameList.add(wingsModel.getName());
                            wingModelList.add(wingsModel);
                        }
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),R.layout.spinner_text, wingNameList);
                    spinner_wing.setAdapter(adapter);
                    spinner_wing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            wingId = wingModelList.get(i).getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

      }
    private void addVendor() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;

       jObj.addProperty("type", "VENDOR");
       jObj.addProperty("name", name);
       jObj.addProperty("contact_no", no);
       jObj.addProperty("wing_id", wingId);

        new NetworkCall().callPostJSON(getActivity(), Services.SOCIETY_PERSON,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getContext(), requestCode, response);
                        Log.e("code-->", returnResult);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                /*JSONObject society_person = jsonObject.getJSONObject("society_person");
                                int id = society_person.getInt("id");
                                String type = society_person.getString("type");
                                String name = society_person.getString("name");
                                String contact_no = society_person.getString("contact_no");
                                String wing_id = society_person.getString("wing_id");

                                VendorModel vendorModel = new VendorModel();
                                vendorModel.setId(id);
                                vendorModel.setType(type);
                                vendorModel.setName(name);
                                vendorModel.setContact_no(contact_no);
                                vendorModel.setWing_id(wing_id);

                                MyConstants.vendorModels.add(vendorModel);*/

                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");
                                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {

                                    if (getActivity().getCurrentFocus() != null) {
                                        getActivity().getCurrentFocus().clearFocus();
                                    }
                                    getActivity().onBackPressed();
                                });
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });
    }

   private boolean checkValidation() {
        boolean isError = false;

      name = edtName.getText().toString().trim();
      no = edtContact.getText().toString().trim();

       if (TextUtils.isEmpty(name)) {
            isError = true;
            edtName.requestFocus();
            edtName.setError(getResources().getString(R.string.blank_worker_name));
        }
       else if (TextUtils.isEmpty(no)) {
           isError = true;
           edtContact.requestFocus();
           edtContact.setError(getResources().getString(R.string.blank_contact));
       }
        return !isError;
    }
    @Override
    public void onDestroy() {
        Fragment fragment = new SocietyTabFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "People");
        bundle.putInt("selTabPos", 2);
        fragment.setArguments(bundle);
        MainActivity.changeFragment(getContext(), fragment, true);
        super.onDestroy();
    }
}
