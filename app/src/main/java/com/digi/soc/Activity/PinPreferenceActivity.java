package com.digi.soc.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digi.soc.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PinPreferenceActivity extends AppCompatActivity {
    @BindView(R.id.llChangePin)
    LinearLayout llChangePin;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_preference);
        ButterKnife.bind(this);
        tvTitle.setText(getResources().getString(R.string.txt_pin_preference));
        bindWidget();
    }

    private void bindWidget() {
        llChangePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PinPreferenceActivity.this,ChangePinActivity.class);
                startActivity(i);
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}