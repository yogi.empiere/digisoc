package com.digi.soc.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.digi.soc.Helpers.JsonParserUniversal;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.ApartmentModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddAlarmActivity extends AppCompatActivity {
    @BindView(R.id.btnAddAlarm)
    Button btnAddAlarm;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.spinner_wing)
    Spinner spinner_wing;

    @BindView(R.id.spinner_apartment)
    Spinner spinner_apartment;

    private int apartmentNo = 0,wingId = 0;

    private JsonParserUniversal jsonParserUniversal;

    public static ArrayList<WingsModel> wingModelList=new ArrayList<>();
    public static ArrayList<String> wingNameList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_alarm);
        ButterKnife.bind(this);
        MainActivity.setBottomPos(this, -1);

      //  setTop(false, "Add Alarm", false);
        jsonParserUniversal = new JsonParserUniversal();
        getApartment();
        getWing();
        bindWidget();
    }
    private void bindWidget() {
        btnAddAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAlarm();
            }

        });
        btnCancel.setOnClickListener(view1 -> onBackPressed());
    }


    private void addAlarm() {
        JsonObject jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("apartment_id", apartmentNo);
        jObj.addProperty("type", "This is a alarm type");

        new NetworkCall().callPostJSON(this, Services.ALARM,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getApplication(), requestCode, response);
                        Log.e("code-->", requestCode);
                        if (requestCode.equals("200")) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject error = jsonObject.getJSONObject("error");
                                String message = error.getString("message");

                                /*"alarm": {
                                    "id": 11,
                                            "type": "This is a alarm type",
                                            "status": "active",
                                            "apartment": {
                                        "id": 12,
                                                "floor_no": 1,
                                                "apartment_no": "201",
                                                "isRented": false,
                                                "current_tenant": {
                                            "id": 2,
                                                    "first_name": "Vivek",
                                                    "last_name": "Sadariya",
                                                    "email": "",
                                                    "contact_no": "8141297933",
                                                    "profile_pic": "https://lifeatbrio.com/wp-content/uploads/2016/11/user-placeholder.jpg",
                                                    "roles": [
                                            "member",
                                                    "secretary"
                ]
                                        },
                                        "owned_by": {
                                            "id": 2,
                                                    "first_name": "Vivek",
                                                    "last_name": "Sadariya",
                                                    "email": "",
                                                    "contact_no": "8141297933",
                                                    "profile_pic": "https://lifeatbrio.com/wp-content/uploads/2016/11/user-placeholder.jpg",
                                                    "roles": [
                                            "member",
                                                    "secretary"
                ]
                                        },
                                        "wing": {
                                            "id": 9,
                                                    "name": "Wing A",
                                                    "no_of_floors": 4
                                        }
                                    }
*/
                                android.app.AlertDialog.Builder alertDialog =
                                        new android.app.AlertDialog.Builder(AddAlarmActivity.this);
                                alertDialog.setTitle("Success!");
                                alertDialog.setMessage(message);

                                alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                    if (getCurrentFocus() != null) {
                                        getCurrentFocus().clearFocus();
                                    }
                                    onBackPressed();
                                });
                               runOnUiThread(new Runnable() {
                                    public void run() {
                                        alertDialog.show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });
    }

    private void getWing() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(this, Services.GET_WINGS, jObj,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getApplication(), requestCode, response);
                if (returnResult.isEmpty())
                    return;

                try {
                    JSONObject jsonObject = new JSONObject(returnResult);
                    JSONArray wings = jsonObject.getJSONArray("wings");
                    System.out.println(wings+"wingsarray");

                    wingModelList.clear();
                    wingNameList.clear();
                    WingsModel wingData = new WingsModel();
                    wingData.setName(getString(R.string.select_wing));
                    wingData.setId(0);
                    wingModelList.add(wingData);
                    wingNameList.add(wingData.getName());

                    for (int i = 0; i < wings.length(); i++) {
                        JSONObject object = wings.getJSONObject(i);
                        WingsModel wingsModel = (WingsModel) jsonParserUniversal.parseJson(object, new WingsModel());

                        if (wingsModel.getName() != null && !wingsModel.getName().toLowerCase().equals("null")) {
                            wingNameList.add(wingsModel.getName());
                            wingModelList.add(wingsModel);
                        }
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplication(),R.layout.spinner_text, wingNameList);
                    spinner_wing.setAdapter(adapter);
                    spinner_wing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            wingId = wingModelList.get(i).getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getApartment() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(this, Services.APARTMENT, jObj,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(getApplication(), requestCode, response);
                if (returnResult.isEmpty())
                    return;

                try {
                    JSONObject jsonObject = new JSONObject(returnResult);
                    JSONArray apartments = jsonObject.getJSONArray("apartments");
                    System.out.println(apartments+"apartments");

                    Gson gson = new Gson();
                    Type type = new TypeToken<List<ApartmentModel>>() {}.getType();
                    List<ApartmentModel> list = gson.fromJson(jsonObject.getJSONArray("apartments").toString(), type);

                    ArrayList<ApartmentModel> arrModel = new ArrayList<>();
                    arrModel.addAll(list);

                    ArrayAdapter<ApartmentModel> adapter = new ArrayAdapter<>(getApplication(),
                            R.layout.spinner_text, arrModel);

                    spinner_apartment.setAdapter(adapter);
                    spinner_apartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            apartmentNo = arrModel.get(i).getId();

                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
