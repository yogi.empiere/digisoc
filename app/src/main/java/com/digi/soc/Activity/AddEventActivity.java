package com.digi.soc.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digi.soc.Adapters.SelectedWingsAdapter;
import com.digi.soc.Adapters.WingsSelAdapter;
import com.digi.soc.Fragments.DateFragment;
import com.digi.soc.Helpers.ItemOffsetDecoration;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.OnDateSelected;
import com.digi.soc.Helpers.OnWingDeleted;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.SocietyTabActivity;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddEventActivity extends AppCompatActivity {
    @BindView(R.id.btnAdd)
    Button btnAdd;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.btnSelectWing)
    Button btnSelectWing;
    @BindView(R.id.edtTitle)
    EditText edtTitle;
    @BindView(R.id.edtDesc)
    EditText edtDesc;
    @BindView(R.id.edtContact)
    EditText edtContact;
    @BindView(R.id.edtLocation)
    EditText edtLocation;
    @BindView(R.id.layoutWings)
    LinearLayout layoutWings;
    @BindView(R.id.tvSelectAll)
    TextView tvSelectAll;
    @BindView(R.id.btnAddW)
    Button btnAddW;
    @BindView(R.id.btnCancelW)
    Button btnCancelW;
    @BindView(R.id.rvWing)
    RecyclerView rvWing;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.ivSelImage)
    ImageView ivSelImage;

    private String userChoosenTask = "";
    private String selectedDate = "";
    private String eventPicture = "";
    private String eventName = "";
    private String eventDesc = "";
    private String eventPlace = "";
    private String eventStart = "";
    private String eventEnd = "";

    private boolean isSelecAll = false;

    private ArrayList<WingsModel> wingSelList = new ArrayList<>();

    private ArrayList<Date> eventDates = new ArrayList<>();
    private JsonArray wingsArray = new JsonArray();


    private ArrayList<WingsModel> datalist = new ArrayList<>();
    private WingsSelAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_event);
        ButterKnife.bind(this);

        MainActivity.setBottomPos(getApplicationContext(), -1);


        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen._7sdp, R.dimen._5sdp);
        rvWing.addItemDecoration(itemDecoration);

        rvWing.setNestedScrollingEnabled(false);

        //addData();
        bindWidgetEvent();
    }


    private void bindWidgetEvent() {

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SocietyTabActivity.changeFragment(getApplication(), new DateFragment(new OnDateSelected() {
                    @Override
                    public void onDateSelectedListener(List<Date> list) {
                        eventDates.clear();
                        eventDates.addAll(list);

                        eventStart = Utility.eventDateFormat(list.get(0));
                        eventEnd = Utility.eventDateFormat(list.get(list.size() - 1));

                        selectedDate = Utility.selectedDate(getApplicationContext(), list.get(list.size() - 1));
                    }
                }, eventDates), true);

            }
        });

        btnCancel.setOnClickListener(view1 -> onBackPressed());

        btnSelectWing.setOnClickListener(view1 -> {
            btnSelectWing.setVisibility(View.GONE);
            layoutWings.setVisibility(View.VISIBLE);

            tvSelectAll.setVisibility(View.VISIBLE);
            btnAddW.setVisibility(View.VISIBLE);
            btnCancelW.setVisibility(View.VISIBLE);

            if (wingSelList.size() == 0 || wingSelList.size() < datalist.size()) {
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                isSelecAll = false;
            } else {
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                isSelecAll = true;
            }

            getWingsData();
        });

        btnCancelW.setOnClickListener(view1 -> {

            for (WingsModel model : datalist) {
                model.setSelect(false);
            }
            isSelecAll = false;
            wingSelList.clear();
            btnSelectWing.setVisibility(View.VISIBLE);
            layoutWings.setVisibility(View.GONE);
        });

        tvSelectAll.setOnClickListener(view -> {
            if (isSelecAll) {
                isSelecAll = false;
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                for (WingsModel model : datalist) {
                    model.setSelect(false);
                }
                adapter.notifyDataSetChanged();
            } else {
                isSelecAll = true;
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                for (WingsModel model : datalist) {
                    model.setSelect(true);
                }
                adapter.notifyDataSetChanged();
            }
        });

        btnAddW.setOnClickListener(view -> {

            wingSelList.clear();

            for (int x = 0; x < datalist.size(); x++) {
                if (datalist.get(x).isSelect())
                    wingSelList.add(datalist.get(x));
            }

            if (wingSelList.size() > 0) {

                btnSelectWing.setVisibility(View.VISIBLE);
                layoutWings.setVisibility(View.VISIBLE);

                rvWing.setAdapter(new SelectedWingsAdapter(this, wingSelList, new OnWingDeleted() {
                    @Override
                    public void onWingDeleteListner() {
                        if (wingSelList.size() == 0) {
                            btnSelectWing.setVisibility(View.VISIBLE);
                            layoutWings.setVisibility(View.GONE);

                            for (WingsModel model : datalist) {
                                model.setSelect(false);
                            }

                        } else {
                            for (WingsModel modelDatalist : datalist) {
                                boolean isMatch = false;
                                for (WingsModel modelSelWings : wingSelList) {
                                    if (modelDatalist.getWings_Name().equals(modelSelWings.getWings_Name())) {
                                        isMatch = true;
                                    }
                                    if (wingSelList.get(wingSelList.size() - 1).equals(modelSelWings)) {
                                        if (isMatch)
                                            modelDatalist.setSelect(true);
                                        else
                                            modelDatalist.setSelect(false);
                                    }
                                }
                            }
                        }
                    }
                }));
                rvWing.setLayoutManager(new GridLayoutManager(this, 2));

                btnAddW.setVisibility(View.GONE);
                btnCancelW.setVisibility(View.GONE);
                tvSelectAll.setVisibility(View.GONE);
            } else {
                Utility.showDialog(this, getResources().getString(R.string.error), getResources().getString(R.string.blank_event_wings));
            }

        });

        btnAdd.setOnClickListener(view -> {
            if (checkValidation()) {
                addEvent();

            }
        });

        ivSelImage.setOnClickListener(view -> {
            //selectImage();
        });

    }

    private void addEvent() {
        JsonObject jObj;
        jObj = new JsonObject();
        MyConstants.isResponceJsonOBJ = true;
        jObj.addProperty("title", eventName);
        jObj.addProperty("description", eventDesc);
        jObj.addProperty("place", eventPlace);
        jObj.addProperty("start_time", eventStart);
        jObj.addProperty("end_time", eventEnd);
        jObj.add("attendees", wingsArray);

        Log.e("Json", jObj.toString());

        new NetworkCall().callPostJSON(this, Services.EVENT,
                jObj, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response, String requestCode) {
                        String returnResult = Utils.handleAPIResponceCode(getApplicationContext(), requestCode, response);
                        Log.e("code-->", returnResult);
                        if (requestCode.equals("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean success = jsonObject.getBoolean("success");
                                if (success) {
                                    android.app.AlertDialog.Builder alertDialog;
                                    alertDialog = new android.app.AlertDialog.Builder(AddEventActivity.this);
                                    alertDialog.setTitle("Success!");
                                    alertDialog.setMessage("Event created");

                                    alertDialog.setPositiveButton("Ok", (dialog, which) -> {
                                        edtTitle.getText().clear();
                                        edtDesc.getText().clear();
                                        edtLocation.getText().clear();
                                        edtContact.getText().clear();
                                        eventPicture = "";
                                        ivSelImage.setImageResource(R.drawable.ic_add_image);
                                        tvDate.setText(getResources().getString(R.string.select_date));
                                        eventStart = "";
                                        eventEnd = "";
                                        selectedDate = "";
                                        if (getCurrentFocus() != null) {
                                            getCurrentFocus().clearFocus();
                                        }
                                        onBackPressed();
                                    });
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            alertDialog.show();
                                        }
                                    });
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        if (!returnResult.isEmpty()) {
                            return;
                        }
                    }
                });
    }

    private boolean checkValidation() {
        boolean isError = false;

        eventDesc = edtDesc.getText().toString();
        eventName = edtTitle.getText().toString();
        eventPlace = edtLocation.getText().toString();
        String contactNo = edtContact.getText().toString();

        wingsArray = new JsonArray();
        for (WingsModel list : datalist) {
            if (list.isSelect()) {
                wingsArray.add(list.getWings_id());
            }
        }
        if (TextUtils.isEmpty(eventName)) {
            isError = true;
            edtTitle.requestFocus();
            edtTitle.setError(getResources().getString(R.string.blank_event_title));
        } else if (TextUtils.isEmpty(eventDesc)) {
            isError = true;
            edtDesc.requestFocus();
            edtDesc.setError(getResources().getString(R.string.blank_event_desc));
            edtDesc.setError(getResources().getString(R.string.blank_event_desc));
        } else if (TextUtils.isEmpty(eventPlace)) {
            isError = true;
            edtLocation.requestFocus();
            edtLocation.setError(getResources().getString(R.string.blank_event_place));
        } else if (TextUtils.isEmpty(contactNo)) {
            isError = true;
            edtContact.requestFocus();
            edtContact.setError(getResources().getString(R.string.blank_event_conatct));
        } else if (TextUtils.isEmpty(eventStart)) {
            isError = true;
            Utility.showDialog(this, getResources().getString(R.string.error), getResources().getString(R.string.blank_event_start_date));
        } else if (TextUtils.isEmpty(eventEnd)) {
            isError = true;
            Utility.showDialog(this, getResources().getString(R.string.error), getResources().getString(R.string.blank_event_end_date));
        } else if (wingsArray.size() == 0) {
            isError = true;
            Utility.showDialog(this, getResources().getString(R.string.error), getResources().getString(R.string.blank_event_wings));
        }
        return !isError;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(selectedDate)) {
            tvDate.setText(selectedDate);
        }
        if (wingSelList.size() > 0) {
            layoutWings.setVisibility(View.VISIBLE);
            btnAddW.setVisibility(View.GONE);
            btnCancelW.setVisibility(View.GONE);
            tvSelectAll.setVisibility(View.GONE);
            rvWing.setAdapter(new SelectedWingsAdapter(this, wingSelList, () -> {
                btnSelectWing.setVisibility(View.VISIBLE);
                layoutWings.setVisibility(View.GONE);
            }));
            rvWing.setLayoutManager(new GridLayoutManager(this, 2));
        }
    }

    private void getWingsData() {
        if (datalist.size() > 0) {
            setData();
        } else {
            JsonObject jObj = new JsonObject();
            new NetworkCall(this, Services.DASHBOARD,
                    jObj, true, NetworkCall.REQ_GET,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response, String requestCode) {
                    String returnResult = Utils.handleAPIResponceCode(getApplicationContext(), requestCode, response);
                    Log.e("code-->", requestCode);
                    if (requestCode.equals("200")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Gson gson = new Gson();
                            Type type = new TypeToken<List<WingsModel>>() {
                            }.getType();
                            List<WingsModel> list = gson.fromJson(jsonObject.getJSONObject("wings").getJSONArray("list").toString(), type);

                            ArrayList<WingsModel> arrModel = new ArrayList<>();
                            arrModel.addAll(list);

                            for (WingsModel d : arrModel) {
                                WingsModel wingsModel = new WingsModel();
                                wingsModel.setWings_Name(d.getName());
                                wingsModel.setWings_id(d.getId());
                                wingsModel.setSelect(false);
                                datalist.add(wingsModel);
                            }
                            setData();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    if (!returnResult.isEmpty()) {
                        return;
                    } else {
                        for (int i = 0; i < 6; i++) {
                            WingsModel wingsModel = new WingsModel();
                            wingsModel.setWings_Name("Wing " + i);
                            wingsModel.setSelect(false);
                            datalist.add(wingsModel);
                        }
                        setData();
                    }
                }
            });
        }
    }

    private void setData() {
        rvWing.setHasFixedSize(true);
        rvWing.setLayoutManager(new GridLayoutManager(this, 3));
        rvWing.setItemViewCacheSize(2);
        adapter = new WingsSelAdapter(this, datalist, new OnWingDeleted() {
            @Override
            public void onWingDeleteListner() {
                isSelecAll = false;
                tvSelectAll.setText(getResources().getString(R.string.select_all));
            }
        });
        rvWing.setAdapter(adapter);
    }
}

