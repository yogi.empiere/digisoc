package com.digi.soc.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digi.soc.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePinActivity extends AppCompatActivity {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.etreEnterPin)
    EditText etreEnterPin;
    @BindView(R.id.etCurrenPin)
    EditText etCurrenPin;
    @BindView(R.id.etnewPin)
    EditText etnewPin;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    boolean isShowText = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);
        ButterKnife.bind(this);
        bindWidget();

    }

    private void bindWidget() {
        tvTitle.setText(getResources().getString(R.string.txt_change_pin));
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void onTextVisible(EditText editText) {
        if (isShowText) {
            isShowText = false;
            editText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            isShowText = true;
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    public void onChangePin(View view) {
        onTextVisible(etCurrenPin);
    }

    public void onConfirmPin(View view) {
        onTextVisible(etreEnterPin);
    }

    public void onNewPin(View view) {
        onTextVisible(etnewPin);
    }
}