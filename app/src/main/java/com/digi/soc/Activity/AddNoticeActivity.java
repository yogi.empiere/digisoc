package com.digi.soc.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.digi.soc.Adapters.SelectedWingsAdapter;
import com.digi.soc.Adapters.WingsSelAdapter;
import com.digi.soc.Helpers.ItemOffsetDecoration;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.MySharedPref;
import com.digi.soc.Helpers.OnWingDeleted;
import com.digi.soc.Helpers.Utility;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.MainActivity;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.R;
import com.digi.soc.SocietyTabActivity;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddNoticeActivity extends AppCompatActivity {

    @BindView(R.id.btnAddNotice)
    Button btnAddNotice;

    @BindView(R.id.ivSelImage)
    ImageView ivSelImage;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.btnSelectWing)
    Button btnSelectWing;

    @BindView(R.id.edtTitle)
    EditText edtTitle;

    @BindView(R.id.edtNoticeDesc)
    EditText edtNoticeDesc;

    @BindView(R.id.layoutWings)
    LinearLayout layoutWings;

    @BindView(R.id.tvSelectAll)
    TextView tvSelectAll;

    @BindView(R.id.btnAddW)
    Button btnAddW;

    @BindView(R.id.btnCancelW)
    Button btnCancelW;

    @BindView(R.id.rvWing)
    RecyclerView rvWing;

    @BindView(R.id.ivRoundImage)
    ImageView ivRoundImage;


    ArrayList<WingsModel> wingSelList = new ArrayList<>();
    private ArrayList<Integer> selectedWings = new ArrayList<>();

    File destination;
    private String userChoosenTask = "";
    private String noticePicture = "";
    private boolean isSelecAll = false;
    private WingsSelAdapter adapter;
    private ArrayList<WingsModel> datalist = new ArrayList<>();
    private ArrayList<Integer> wingsArray = new ArrayList();
    private String noticeTitle = "";
    private String noticeDesc = "";


    private android.app.AlertDialog.Builder alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_notice);
        ButterKnife.bind(this);

        MainActivity.setBottomPos(getApplicationContext(), -1);

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen._7sdp, R.dimen._5sdp);
        rvWing.addItemDecoration(itemDecoration);

        rvWing.setNestedScrollingEnabled(false);

        bindWidget();
    }
    private void bindWidget() {
        btnAddNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    uploadNotice();
                }
            }

        });
        btnCancelW.setOnClickListener(view1 -> {

            for (WingsModel model : datalist) {
                model.setSelect(false);
            }
            isSelecAll = false;
            wingSelList.clear();
            btnSelectWing.setVisibility(View.VISIBLE);
            layoutWings.setVisibility(View.GONE);
        });

        btnSelectWing.setOnClickListener(view1 -> {
            btnSelectWing.setVisibility(View.GONE);
            layoutWings.setVisibility(View.VISIBLE);

            tvSelectAll.setVisibility(View.VISIBLE);
            btnAddW.setVisibility(View.VISIBLE);
            btnCancelW.setVisibility(View.VISIBLE);

            if (wingSelList.size() == 0 || wingSelList.size() < datalist.size()) {
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                isSelecAll = false;
            } else {
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                isSelecAll = true;
            }
            addData();
        });

        btnCancel.setOnClickListener(view1 ->onBackPressed());

        tvSelectAll.setOnClickListener(view1 -> {
            if (isSelecAll) {
                isSelecAll = false;
                tvSelectAll.setText(getResources().getString(R.string.select_all));
                for (WingsModel model : datalist) {
                    model.setSelect(false);
                }
                adapter.notifyDataSetChanged();
            } else {
                isSelecAll = true;
                tvSelectAll.setText(getResources().getString(R.string.deselect_all));
                for (WingsModel model : datalist) {
                    model.setSelect(true);
                }
                adapter.notifyDataSetChanged();
            }
        });

        ivSelImage.setOnClickListener(view1 -> {
            selectImage();
            ivSelImage.setVisibility(View.GONE);
            ivRoundImage.setVisibility(View.VISIBLE);
        });

        btnAddW.setOnClickListener(view1 -> {
            wingSelList.clear();
            for (int x = 0; x < datalist.size(); x++) {
                if (datalist.get(x).isSelect())
                    wingSelList.add(datalist.get(x));
            }
            if (wingSelList.size() > 0) {

                btnSelectWing.setVisibility(View.VISIBLE);
                layoutWings.setVisibility(View.VISIBLE);

                rvWing.setAdapter(new SelectedWingsAdapter(getApplication(), wingSelList, new OnWingDeleted() {
                    @Override
                    public void onWingDeleteListner() {
                        if (wingSelList.size() == 0) {
                            btnSelectWing.setVisibility(View.VISIBLE);
                            layoutWings.setVisibility(View.GONE);

                            for (WingsModel model : datalist) {
                                model.setSelect(false);
                            }

                        } else {
                            for (WingsModel modelDatalist : datalist) {
                                boolean isMatch = false;
                                for (WingsModel modelSelWings : wingSelList) {
                                    if (modelDatalist.getWings_Name().equals(modelSelWings.getWings_Name())) {
                                        isMatch = true;
                                    }
                                    if (wingSelList.get(wingSelList.size() - 1).equals(modelSelWings)) {
                                        if (isMatch)
                                            modelDatalist.setSelect(true);
                                        else
                                            modelDatalist.setSelect(false);
                                    }
                                }
                            }
                        }
                    }
                }));
                rvWing.setLayoutManager(new GridLayoutManager(getApplication(), 2));

                btnAddW.setVisibility(View.GONE);
                btnCancelW.setVisibility(View.GONE);
                tvSelectAll.setVisibility(View.GONE);
            } else {
                Utility.showDialog(this, getResources().getString(R.string.error), getResources().getString(R.string.blank_event_wings));
            }

        });
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(getApplicationContext());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),MyConstants.SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, MyConstants.REQUEST_CAMERA);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == MyConstants.SELECT_FILE){
                Uri selectedImage = data.getData();
                String filePath[] = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);// c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);

                destination = new File(picturePath);

                onSelectFromGalleryResult(data);

            }
            else if (requestCode == MyConstants.REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        String pathFinal = saveToInternalStorage(thumbnail);
        destination = new File(pathFinal);
        ivRoundImage.setImageBitmap(thumbnail);
    }

    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplication());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,"profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath.getAbsolutePath();
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ivRoundImage.setImageBitmap(bm);
    }

    private void addData() {

        if (datalist.size() > 0) {
            setData();
        } else {

            JsonObject jObj = new JsonObject();

            new NetworkCall(this, Services.DASHBOARD,
                    jObj, true, NetworkCall.REQ_GET,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response, String requestCode) {
                    String returnResult = Utils.handleAPIResponceCode(getApplicationContext(), requestCode, response);
                    Log.e("code-->", requestCode);
                    if (requestCode.equals("200")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Gson gson = new Gson();
                            Type type = new TypeToken<List<WingsModel>>() {
                            }.getType();
                            List<WingsModel> list = gson.fromJson(jsonObject.getJSONObject("wings").getJSONArray("list").toString(), type);

                            ArrayList<WingsModel> arrModel = new ArrayList<>();
                            arrModel.addAll(list);

                            for (WingsModel d : arrModel) {
                                WingsModel wingsModel = new WingsModel();
                                wingsModel.setWings_Name(d.getName());
                                wingsModel.setWings_id(d.getId());
                                wingsModel.setSelect(false);
                                datalist.add(wingsModel);
                            }
                            setData();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    if (!returnResult.isEmpty()) {
                        return;
                    } else {
                        for (int i = 0; i < 6; i++) {
                            WingsModel wingsModel = new WingsModel();
                            wingsModel.setWings_Name("Wing " + i);
                            wingsModel.setSelect(false);
                            datalist.add(wingsModel);
                        }
                        setData();
                    }
                }
            });
        }
    }

    private void setData() {
        rvWing.setHasFixedSize(true);
        rvWing.setLayoutManager(new GridLayoutManager(this, 3));
        rvWing.setItemViewCacheSize(2);
        adapter = new WingsSelAdapter(this, datalist, new OnWingDeleted() {
            @Override
            public void onWingDeleteListner() {
                isSelecAll = false;
                tvSelectAll.setText(getResources().getString(R.string.select_all));
            }
        });
        rvWing.setAdapter(adapter);
    }

    private boolean checkValidation() {
        boolean isError = false;
        noticeTitle = edtTitle.getText().toString();
        noticeDesc = edtNoticeDesc.getText().toString();

        selectedWings = new ArrayList<>();
        selectedWings.clear();
        for (WingsModel list : datalist) {
            if (list.isSelect()) {
                selectedWings.add(list.getWings_id());
            }
        }
        if (TextUtils.isEmpty(noticeTitle)) {
            isError = true;
            edtTitle.requestFocus();
            edtTitle.setError(getResources().getString(R.string.blank_notice_title));
        } else if (TextUtils.isEmpty(noticeDesc)) {
            isError = true;
            edtNoticeDesc.requestFocus();
            edtNoticeDesc.setError(getResources().getString(R.string.blank_notice_desc));
        }else if (selectedWings.size() == 0) {
            isError = true;
            Utility.showDialog(this, getResources().getString(R.string.error),
                    getResources().getString(R.string.blank_notice_wings));
        } /* else if (TextUtils.isEmpty(noticePicture)) {
            isError = true;
            Utility.showDialog(this, getResources().getString(R.string.error),
                    getResources().getString(R.string.blank_event_picture));
        }*/
        return !isError;
    }

    private void uploadNotice() {
        String auth = new MySharedPref(this).getString(MySharedPref.USER_AUTH, "");

        HashMap<String, String> wingsHashMap = new HashMap<String, String>();
        for (int i : selectedWings){
            wingsHashMap.put("wing_ids[]", "" + i);
        }

        AndroidNetworking.upload(Services.NOTICE)
                .addHeaders("Accept", "application/json")
                .addHeaders("Content-Type", "application/x-www-form-urlencoded")
                .addHeaders("Authorization", "Bearer " + auth)
                .addMultipartFile("notice_img", destination)
                .addMultipartParameter("notice_title", noticeTitle)
                .addMultipartParameter("notice_description", noticeDesc)
                .addMultipartParameter(wingsHashMap)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            JSONObject error = jsonObject.getJSONObject("error");
                            String message = error.getString("message");
                            alertDialog = new android.app.AlertDialog.Builder(AddNoticeActivity.this);
                            alertDialog.setTitle("Success!");
                            alertDialog.setMessage(message);

                            alertDialog.setPositiveButton("Ok", (dialog, which) -> {

                                ivSelImage.setImageResource(R.drawable.ic_add_image);

                                if (getCurrentFocus() != null) {
                                    getCurrentFocus().clearFocus();
                                }
                                Intent i = new Intent(AddNoticeActivity.this, SocietyTabActivity.class);
                                i.putExtra("type", "Home_Notice");
                                i.putExtra("selTabPos",0);
                                startActivity(i);

                            });
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    alertDialog.show();
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError ===>", String.valueOf(anError));
                    }
                });
    }
}
