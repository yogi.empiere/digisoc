package com.digi.soc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;

import java.io.File;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity {

    @BindView(R.id.icBack)
     ImageView icBack;

    @BindView(R.id.etSociety)
     EditMedium etSociety;

    @BindView(R.id.etFirstName)
     EditMedium etFirstName;

    @BindView(R.id.etLastName)
     EditMedium etLastName;

    @BindView(R.id.etContact)
     EditMedium etContact;

    @BindView(R.id.etPassword)
     EditMedium etPassword;

    @BindView(R.id.etConfirmPass)
     EditMedium etConfirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ButterKnife.bind(this);
        bindWidget();
        doRegister();

    }

    private void bindWidget() {
        icBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void doRegister() {
        String Society = etSociety.getText().toString().trim();
        String FirstName = etFirstName.getText().toString().trim();
        String LastName = etLastName.getText().toString().trim();
        String Contact = etContact.getText().toString().trim();
        String Password = etPassword.getText().toString().trim();
        String ConfirmPass = etConfirmPass.getText().toString().trim();

        if (TextUtils.isEmpty(Society)) {
            etSociety.requestFocus();
            etSociety.setError(getResources().getString(R.string.blank_society_name));
        } else if (TextUtils.isEmpty(FirstName)) {
            etFirstName.requestFocus();
            etFirstName.setError(getResources().getString(R.string.blank_first_name));
        } else if (TextUtils.isEmpty(LastName)) {
            etLastName.requestFocus();
            etLastName.setError(getResources().getString(R.string.blank_last_name));
        } else if (TextUtils.isEmpty(Contact)) {
            etContact.requestFocus();
            etContact.setError(getResources().getString(R.string.blank_contact));
        }  else if (TextUtils.isEmpty(Password) || Password.length() < 8) {
            etPassword.requestFocus();
            etPassword.setError(getResources().getString(R.string.invalid_length_password));
        }else if (TextUtils.isEmpty(ConfirmPass)) {
            etConfirmPass.requestFocus();
            etConfirmPass.setError(getResources().getString(R.string.blank_confirm_password));
        }else if(!Password.equals(ConfirmPass))
        {
            Utils.showWarn(SignupActivity.this,getResources().getString(R.string.password_not_match),2 );
        } else {
            HashMap<String,String> userdata = new HashMap<>();
            userdata.put("society_id",Society);
            userdata.put("first_name",FirstName);
            userdata.put("last_name",LastName);
            userdata.put("contact_no",Contact);
            userdata.put("password",Password);
            userdata.put("date_of_birth","");
            userdata.put("otp","");

        }
    }
}
