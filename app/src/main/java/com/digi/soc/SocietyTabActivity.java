package com.digi.soc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.digi.soc.Activity.AddAlarmActivity;
import com.digi.soc.Activity.AddComplainActivity;
import com.digi.soc.Activity.AddEventActivity;
import com.digi.soc.Activity.AddNoticeActivity;
import com.digi.soc.Adapters.AlarmAdapter;
import com.digi.soc.Adapters.AssetsAdapter;
import com.digi.soc.Adapters.BookingAdapter;
import com.digi.soc.Adapters.ComplainAdapter;
import com.digi.soc.Adapters.EmergencyAdapter;
import com.digi.soc.Adapters.EventAdapter;
import com.digi.soc.Adapters.GatesAdapter;
import com.digi.soc.Adapters.MemberRequestAdapter;
import com.digi.soc.Adapters.NoticeAdapter;
import com.digi.soc.Adapters.PollAdapter;
import com.digi.soc.Adapters.RulesAdapter;
import com.digi.soc.Adapters.StaffAdapter;
import com.digi.soc.Adapters.VendorAdapter;
import com.digi.soc.Adapters.VisitorAdapter;
import com.digi.soc.Adapters.WingsAdapter;
import com.digi.soc.Adapters.WorkersAdapter;
import com.digi.soc.Fragments.AddAlarmFragment;
import com.digi.soc.Fragments.AddComplainFragment;
import com.digi.soc.Fragments.AddEmergencyContactFragment;
import com.digi.soc.Fragments.AddEventFragment;
import com.digi.soc.Fragments.AddGatesFragment;
import com.digi.soc.Fragments.AddNoticeFragment;
import com.digi.soc.Fragments.AddRulesFragment;
import com.digi.soc.Fragments.AddStaffFragment;
import com.digi.soc.Fragments.AddVendorFragment;
import com.digi.soc.Fragments.AddWingFragment;
import com.digi.soc.Fragments.AddWorkersFragment;
import com.digi.soc.Helpers.MyConstants;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.Models.AlarmModel;
import com.digi.soc.Models.BookingModel;
import com.digi.soc.Models.ComplainModel;
import com.digi.soc.Models.EmergencyModel;
import com.digi.soc.Models.EventModel;
import com.digi.soc.Models.GatesModel;
import com.digi.soc.Models.MemberRequestModel;
import com.digi.soc.Models.NoticeModel;
import com.digi.soc.Models.PollModel;
import com.digi.soc.Models.RulesModel;
import com.digi.soc.Models.StaffModel;
import com.digi.soc.Models.VendorModel;
import com.digi.soc.Models.WingsModel;
import com.digi.soc.Models.WorkersModel;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SocietyTabActivity extends AppCompatActivity {
    private int selTabPos;
    private String type;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivAlarm)
    ImageView ivAlarm;

    @BindView(R.id.ivNoti)
    ImageView ivNoti;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ivUserProfile)
    ImageView ivUserProfile;

    @BindView(R.id.ivSetting)
    ImageView ivSetting;

    @BindView(R.id.tvNotiNo)
    TextView tvNotiNo;

    @BindView(R.id.tvAlarmNo)
    TextView tvAlarmNo;

    @BindView(R.id.tabSociety)
    TabLayout tabSociety;

    @BindView(R.id.tabMain)
    TabLayout tabMain;

    @BindView(R.id.rvWing)
    RecyclerView rvWing;

    @BindView(R.id.ivAddWing)
    ImageView ivAddWing;

    @BindView(R.id.edtSearch)
    EditText edtSearch;

    @BindView(R.id.tvEmptyList)
    TextView tvEmptyList;
    Intent i;

    private ArrayList<RulesModel> rulesModels = new ArrayList<>();
    private ArrayList<GatesModel> gatesModels = new ArrayList<>();
    private ArrayList<EmergencyModel> emergencyModels = new ArrayList<>();
    private ArrayList<WorkersModel> workersModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_society_tab);
        ButterKnife.bind(this);

        //assert getArguments() != null;
        //selTabPos = getArguments().getInt("selTabPos");
        //type = getArguments().getString("type");

        Intent intent = getIntent();
        type = intent.getStringExtra("type");
        selTabPos = intent.getIntExtra("selTabpos", 0);

        rvWing.setHasFixedSize(true);
        rvWing.setLayoutManager(new LinearLayoutManager(SocietyTabActivity.this));

        setupTabs();

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                }
                return false;
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

        ivAddWing.setOnClickListener(view1 -> {
            switch (type) {
                case "Society":
                    if (selTabPos == 0)
                        MainActivity.changeFragment(SocietyTabActivity.this, new AddWingFragment(), true);
                    else if (selTabPos == 1)
                        MainActivity.changeFragment(SocietyTabActivity.this, new AddRulesFragment(), true);
                    else if (selTabPos == 2)
                        MainActivity.changeFragment(SocietyTabActivity.this, new AddEmergencyContactFragment(), true);
                    else if (selTabPos == 3)
                        MainActivity.changeFragment(SocietyTabActivity.this, new AddGatesFragment(), true);
                    break;

                case "People":
                    if (selTabPos == 1)
                        MainActivity.changeFragment(SocietyTabActivity.this, new AddWorkersFragment(), true);
                    if (selTabPos == 2)
                        MainActivity.changeFragment(SocietyTabActivity.this, new AddVendorFragment(), true);
                    if (selTabPos == 4)
                        MainActivity.changeFragment(SocietyTabActivity.this, new AddStaffFragment(), true);
                    break;
                case "Home_Notice":
                    i = new Intent(SocietyTabActivity.this, AddNoticeActivity.class);
                    startActivity(i);
                  //MainActivity.changeFragment(SocietyTabActivity.this, new AddNoticeFragment(), true);
                    break;
                case "Home_Complain":
                    i = new Intent(SocietyTabActivity.this, AddComplainActivity.class);
                    startActivity(i);
                   // MainActivity.changeFragment(SocietyTabActivity.this, new AddComplainFragment(), true);
                    break;
                case "Home_Event":
                    i = new Intent(SocietyTabActivity.this, AddEventActivity.class);
                    startActivity(i);
                    //MainActivity.changeFragment(SocietyTabActivity.this, new AddEventFragment(), true);
                    break;
                case "Alarm":
                    i = new Intent(SocietyTabActivity.this, AddAlarmActivity.class);
                    startActivity(i);
                    //MainActivity.changeFragment(SocietyTabActivity.this, new AddAlarmFragment(), true);
                    break;

            }
        });

    }

    public static void changeFragment(Context context, Fragment fragment, boolean doAdd) {
        FragmentManager manager = ((MainActivity) context).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.containerFrame, fragment);
        if (doAdd)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }
    private void getDashboardData(int listFor) {
        JsonObject jObj = new JsonObject();
        new NetworkCall(SocietyTabActivity.this, Services.DASHBOARD, jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String code) {
                try {
                    String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, code, response);
                    if (returnResult.isEmpty()) {
                        return;
                    }
                    JSONObject Obj = new JSONObject(response);

                    switch (listFor) {
                        case 1: //for Notice Fragment
                            JSONObject noticeObj = Obj.getJSONObject("notices");
                            JSONArray noticeDataList = noticeObj.getJSONArray("list");
                            MyConstants.noticeData = (ArrayList<NoticeModel>) new Gson().fromJson(String.valueOf(noticeDataList), new TypeToken<ArrayList<NoticeModel>>() {
                            }.getType());
                            setNoticeList();
                            break;
                        case 2: //for Complain Fragment
                            JSONObject complainObj = Obj.getJSONObject("complaints");
                            JSONArray complainDataList = complainObj.getJSONArray("list");
                            MyConstants.complainData = (ArrayList<ComplainModel>) new Gson().fromJson(String.valueOf(complainDataList), new TypeToken<ArrayList<ComplainModel>>() {
                            }.getType());
                            setComplainList();
                            break;

                        case 3://for wings list
                            Gson gson = new Gson();
                            Type type = new TypeToken<List<WingsModel>>() {
                            }.getType();
                            List<WingsModel> list = gson.fromJson(Obj.getJSONObject("wings")
                                    .getJSONArray("list").toString(), type);
                            MyConstants.wingReqData.addAll(list);


                            break;

                        case 4://for Event list
                            JSONObject eventsObj = Obj.getJSONObject("events");
                            JSONArray eventsDataList = eventsObj.getJSONArray("list");
                            MyConstants.eventData = (ArrayList<EventModel>) new Gson().fromJson(String.valueOf(eventsDataList), new TypeToken<ArrayList<EventModel>>() {
                            }.getType());
                            setEventList();
                            break;

                        case 5://for active alarm
                            Gson gsonAlarm = new Gson();
                            Type typeAlarm = new TypeToken<List<AlarmModel>>() {
                            }.getType();
                            List<AlarmModel> listAlarm = gsonAlarm.fromJson(Obj
                                    .getJSONObject("alarms").getJSONObject("active")
                                    .getJSONArray("list").toString(), typeAlarm);

                            ArrayList<AlarmModel> arrAlarmModel = new ArrayList<>();
                            arrAlarmModel.addAll(listAlarm);

                            if (arrAlarmModel.size() == 0) {
                                tvEmptyList.setVisibility(View.VISIBLE);
                                rvWing.setVisibility(View.GONE);
                            } else {
                                tvEmptyList.setVisibility(View.GONE);
                                rvWing.setVisibility(View.VISIBLE);
                            }
                            AlarmAdapter alarmAdapter = new AlarmAdapter(SocietyTabActivity.this, arrAlarmModel, 1);
                            rvWing.setAdapter(alarmAdapter);

                            break;

                        case 6://for active alarm
                            Gson gsonClosed = new Gson();
                            Type typeClosed = new TypeToken<List<AlarmModel>>() {
                            }.getType();
                            List<AlarmModel> listClosed =
                                    gsonClosed.fromJson(Obj.getJSONObject("alarms")
                                            .getJSONObject("closed").getJSONArray("list")
                                            .toString(), typeClosed);

                            ArrayList<AlarmModel> arrAlarmClosed = new ArrayList<>();
                            arrAlarmClosed.addAll(listClosed);

                            if (arrAlarmClosed.size() == 0) {
                                tvEmptyList.setVisibility(View.VISIBLE);
                                rvWing.setVisibility(View.GONE);
                            } else {
                                tvEmptyList.setVisibility(View.GONE);
                                rvWing.setVisibility(View.VISIBLE);
                            }

                            AlarmAdapter alarmClosedAdapter = new AlarmAdapter(SocietyTabActivity.this, arrAlarmClosed, 0);
                            rvWing.setAdapter(alarmClosedAdapter);

                            break;
                        case 7://for member request
                            JSONObject member_reqObj = Obj.getJSONObject("member_req");
                            JSONArray member_reqDataList = member_reqObj.getJSONArray("list");

                            MyConstants.memberReqData = (ArrayList<MemberRequestModel>)
                                    new Gson().fromJson(String.valueOf(member_reqDataList),
                                            new TypeToken<ArrayList<MemberRequestModel>>() {
                                            }.getType());

                            setMemberRequestList();
                            break;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void setNoticeTabs() {
        tabSociety.addTab(tabSociety.newTab().setText("All"));

        setWingsTabsData();

        tabSociety.getTabAt(selTabPos).select();

        setNoticeTabData(selTabPos);
    }

    private void setNoticeTabData(int pos) {
        //getDashboardData(1);
        // getNoticeList();
        setNoticeList();
    }

    private void getNoticeList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(SocietyTabActivity.this, Services.NOTICE, jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String code) {
                try {
                    String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, code, response);
                    if (returnResult.isEmpty()) {
                        return;
                    }
                    JSONObject Obj = new JSONObject(response);
                    JSONArray noticeDataList = Obj.getJSONArray("list");
                    MyConstants.noticeData = (ArrayList<NoticeModel>) new Gson().
                            fromJson(String.valueOf(noticeDataList), new TypeToken<ArrayList<NoticeModel>>() {
                            }.getType());
                    setNoticeList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getComplainList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(SocietyTabActivity.this, Services.COMPLAIN, jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String code) {
                try {
                    String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, code, response);
                    if (returnResult.isEmpty()) {
                        return;
                    }
                    JSONObject Obj = new JSONObject(response);
                    JSONArray complainDataList = Obj.getJSONArray("list");
                    MyConstants.complainData = (ArrayList<ComplainModel>) new Gson().fromJson(String.valueOf(complainDataList), new TypeToken<ArrayList<ComplainModel>>() {
                    }.getType());
                    setComplainList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getEventList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(SocietyTabActivity.this, Services.EVENT, jObj,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String code) {
                try {
                    String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, code, response);
                    if (returnResult.isEmpty()) {
                        return;
                    }
                    JSONObject Obj = new JSONObject(response);
                    JSONArray eventsDataList = Obj.getJSONArray("list");
                    MyConstants.eventData = (ArrayList<EventModel>) new Gson().fromJson(String.valueOf(eventsDataList),
                            new TypeToken<ArrayList<EventModel>>() {
                            }.getType());
                    setEventList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setComplainTabData(int pos) {
        //  getComplainList();
        // getDashboardData(2);
        setComplainList();
    }

    private void setPeopleTabData(int pos) {
        switch (pos) {
            case 0:
                break;
            case 1:
                setWorkersList();
                break;
            case 2:
                setVendorList();
                break;
            case 3:
                break;
            case 4:
                setStaffList();
        }

    }

    private void setVisitorTabData(int pos) {
        setVisitorList();
    }

    private void setVisitorList() {
        VisitorAdapter visitorAdapter = new VisitorAdapter(SocietyTabActivity.this, new ArrayList<>());
        rvWing.setAdapter(visitorAdapter);
    }

    private void setMemberRequestTabs() {
        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();

        setMemberRequestTabsData(selTabPos);
    }

    private void setWingsTabsData() {
        //getDashboardData(3);
        for (WingsModel model : MyConstants.wingReqData) {
            tabSociety.addTab(tabSociety.newTab().setText(model.getName()));
        }
    }

    private void setPollsTabs() {
        tabMain.addTab(tabMain.newTab().setText("Active"));
        tabMain.addTab(tabMain.newTab().setText("Close"));

        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();
        setPollsTabsData(selTabPos);
    }

    private void setBookingTabs() {
        tabMain.addTab(tabMain.newTab().setText("Active"));
        tabMain.addTab(tabMain.newTab().setText("Closed"));

        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();

        tabSociety.getTabAt(selTabPos).select();

        setBookingData(selTabPos);
    }

    private void setBookingData(int pos) {
        setBookingList();
    }

    private void setBookingList() {
        ArrayList<BookingModel> datalists = new ArrayList<>();
        BookingModel bookingModel = new BookingModel();

        for (int i = 0; i < 5; i++) {
            bookingModel.setChair("Chair");
            bookingModel.setQnt("Qnt - 1");
            datalists.add(bookingModel);
        }

        BookingAdapter bookingAdapter = new BookingAdapter(SocietyTabActivity.this, datalists);
        rvWing.setAdapter(bookingAdapter);
    }

    private void setAlarm() {
        tabMain.addTab(tabMain.newTab().setText("Active"));
        tabMain.addTab(tabMain.newTab().setText("Closed"));
        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();

        setAlarmData(selTabPos);

        tabMain.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setAlarmData(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setAlarmData(int pos) {
        if (pos == 0)
            getDashboardData(5);
        else
            getDashboardData(6);
    }

    private void setPollsTabsData(int pos) {
        setPollsList();
    }

    private void setPollsList() {


    }

    private void setMemberRequestTabsData(int pos) {
         setMemberRequestList();
       // getDashboardData(7);
    }

    private void setMemberRequestList() {
        MemberRequestAdapter memberRequestAdapter = new MemberRequestAdapter(getSupportFragmentManager(),
                SocietyTabActivity.this, MyConstants.memberReqData);
        rvWing.setAdapter(memberRequestAdapter);
    }

    private void setEventList() {
        EventAdapter eventAdapter = new EventAdapter(getSupportFragmentManager(), SocietyTabActivity.this, MyConstants.eventData);
        rvWing.setAdapter(eventAdapter);
    }

    private void setEventTabData(int pos) {
        // getEventList();
       // getDashboardData(4);
        setEventList();
    }

    private void setNoticeList() {
        NoticeAdapter noticeAdapter = new NoticeAdapter(SocietyTabActivity.this, MyConstants.noticeData);
        rvWing.setAdapter(noticeAdapter);
    }

    private void setComplainList() {
        ComplainAdapter complainAdapter = new ComplainAdapter(SocietyTabActivity.this, MyConstants.complainData);
        rvWing.setAdapter(complainAdapter);
    }

    private void setWorkersList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(SocietyTabActivity.this, Services.GET_SOCIETY_PERSON_BY + "WORKER",
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray workersDataList = jsonObject.getJSONArray("list");

                        workersModels = (ArrayList<WorkersModel>)
                                new Gson().fromJson(String.valueOf(workersDataList),
                                        new TypeToken<ArrayList<WorkersModel>>() {
                                        }.getType());
                        WorkersAdapter workersAdapter = new WorkersAdapter(SocietyTabActivity.this, workersModels);
                        rvWing.setAdapter(workersAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setVendorList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(SocietyTabActivity.this, Services.GET_SOCIETY_PERSON_BY + "VENDOR",
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray vendorDataList = jsonObject.getJSONArray("list");

                        ArrayList<VendorModel> vendorModels = new ArrayList<>();
                        vendorModels = (ArrayList<VendorModel>)
                                new Gson().fromJson(String.valueOf(vendorDataList),
                                        new TypeToken<ArrayList<VendorModel>>() {
                                        }.getType());
                        VendorAdapter vendorAdapter = new VendorAdapter(SocietyTabActivity.this, vendorModels);
                        rvWing.setAdapter(vendorAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setStaffList() {
        JsonObject jObj = new JsonObject();
        new NetworkCall(SocietyTabActivity.this, Services.GET_SOCIETY_PERSON_BY + "STAFF",
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray staffDataList = jsonObject.getJSONArray("list");

                        ArrayList<StaffModel> staffModels = new ArrayList<>();
                        staffModels = (ArrayList<StaffModel>)
                                new Gson().fromJson(String.valueOf(staffDataList),
                                        new TypeToken<ArrayList<StaffModel>>() {
                                        }.getType());
                        StaffAdapter vendorAdapter = new StaffAdapter(SocietyTabActivity.this, staffModels);
                        rvWing.setAdapter(vendorAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

     void setTop(boolean isFull, String title, boolean isBack) {
        tvTitle.setText(title);
        if (isFull) {
            ivAlarm.setVisibility(View.VISIBLE);
            ivNoti.setVisibility(View.VISIBLE);
            tvNotiNo.setVisibility(View.VISIBLE);
            tvAlarmNo.setVisibility(View.VISIBLE);
            ivUserProfile.setVisibility(View.VISIBLE);
            ivSetting.setVisibility(View.VISIBLE);
            ivBack.setVisibility(View.GONE);
        } else {
            ivNoti.setVisibility(View.INVISIBLE);
            tvNotiNo.setVisibility(View.INVISIBLE);
            ivUserProfile.setVisibility(View.INVISIBLE);
            ivSetting.setVisibility(View.INVISIBLE);

            if (isBack) {
                ivBack.setVisibility(View.VISIBLE);
                ivAlarm.setVisibility(View.GONE);
                tvAlarmNo.setVisibility(View.GONE);
            } else {
                ivBack.setVisibility(View.GONE);
                ivAlarm.setVisibility(View.INVISIBLE);
                tvAlarmNo.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void setupTabs() {
        switch (type) {
            case "Society":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "Society", true);
                MainActivity.setBottomPos(SocietyTabActivity.this, -1);
                setSocietyTabs();
                break;
            case "Home_Notice":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "Notice", true);
                setNoticeTabs();
                break;
            case "Home_Complain":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "Complain", true);
                setComplainTabs();
                break;
            case "People":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "People", true);
                MainActivity.setBottomPos(SocietyTabActivity.this, -1);
                setPeopleTabs();
                break;
            case "Visitor":
                ivAddWing.setVisibility(View.GONE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search by name");
                setTop(false, "Visitor", true);
                MainActivity.setBottomPos(SocietyTabActivity.this, 3);
                setVisitorTabs();
                break;
            case "Home_Event":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by wing name");
                setTop(false, "Event", true);
                setEventTabs();
                break;
            case "Home_Member_Request":
                ivAddWing.setVisibility(View.GONE);
                tabMain.setVisibility(View.GONE);
                edtSearch.setHint("Search by name");
                setTop(false, "Member Request", true);
                setMemberRequestTabs();
                break;
            case "Home_Polls":
                ivAddWing.setVisibility(View.GONE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search wing by name");
                setTop(false, "Polls", true);
                setPollsTabs();
                break;
            case "Home_Booking":
                ivAddWing.setVisibility(View.GONE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search wing by name");
                setTop(false, "Booking", true);
                setBookingTabs();
                break;
            case "Alarm":
                ivAddWing.setVisibility(View.VISIBLE);
                tabMain.setVisibility(View.VISIBLE);
                edtSearch.setHint("Search by name");
                setTop(false, "Alarm", true);
                setAlarm();
                break;
        }

        setTabLister();
    }

    private void setVisitorTabs() {
        tabMain.addTab(tabMain.newTab().setText("All"));
        tabMain.addTab(tabMain.newTab().setText("My"));

        tabSociety.addTab(tabSociety.newTab().setText("All"));

        setWingsTabsData();

        tabSociety.getTabAt(selTabPos).select();

        tabMain.getTabAt(0).select();

        setVisitorTabData(selTabPos);

        setTabMainLister();
    }

    private void setEventTabs() {
        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();

        setEventTabData(selTabPos);

    }

    private void setComplainTabs() {
        tabMain.addTab(tabMain.newTab().setText("Pending"));
        tabMain.addTab(tabMain.newTab().setText("In Progress"));
        tabMain.addTab(tabMain.newTab().setText("Resolved"));

        tabSociety.addTab(tabSociety.newTab().setText("All"));
        setWingsTabsData();
        tabSociety.getTabAt(selTabPos).select();

        tabMain.getTabAt(0).select();

        setComplainTabData(selTabPos);

        setTabMainLister();
    }

    private void setSocietyTabs() {
        tabSociety.addTab(tabSociety.newTab().setText("Wings"));
        tabSociety.addTab(tabSociety.newTab().setText("Rules"));
        tabSociety.addTab(tabSociety.newTab().setText("Emergency Contact"));
        tabSociety.addTab(tabSociety.newTab().setText("Entry Gates"));

        tabSociety.getTabAt(selTabPos).select();

        setSocietyTabData(selTabPos);
    }

    private void setPeopleTabs() {
        tabSociety.addTab(tabSociety.newTab().setText("Member"));
        tabSociety.addTab(tabSociety.newTab().setText("Worker"));
        tabSociety.addTab(tabSociety.newTab().setText("Vendor"));
        tabSociety.addTab(tabSociety.newTab().setText("Security"));
        tabSociety.addTab(tabSociety.newTab().setText("Staff"));

        tabSociety.getTabAt(selTabPos).select();

        setPeopleTabData(selTabPos);
    }

    private void setTabLister() {
        tabSociety.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selTabPos = tab.getPosition();
                switch (type) {
                    case "Society":
                        setSocietyTabData(selTabPos);
                        break;
                    case "Home_Notice":
                        setNoticeTabData(selTabPos);
                        break;
                    case "People":
                        setPeopleTabData(selTabPos);
                        break;
                    case "Visitor":
                        setVisitorTabData(selTabPos);
                        break;
                    case "Home_Complain":
                        setComplainTabData(selTabPos);
                        break;
                    case "Home_Event":
                        setEventTabData(selTabPos);
                        break;
                    case "Home_Member_Request":
                        setMemberRequestTabsData(selTabPos);
                        break;
                    case "Alarm":
                        setAlarmData(selTabPos);
                        break;
                    case "Home_Booking":
                        setBookingData(selTabPos);
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setTabMainLister() {
        tabMain.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selTabPos = tab.getPosition();
                switch (type) {
                    case "Visitor":

                        break;
                    case "":

                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setSocietyTabData(int pos) {
        switch (pos) {
            case 0:
                setWings();
                break;
            case 1:
                setRules();
                break;
            case 2:
                setEmergency();
                break;
            case 3:
                setGates();
                break;

        }
    }

    private void setWings() {
        JsonObject jObj = new JsonObject();

        new NetworkCall(SocietyTabActivity.this, Services.DASHBOARD,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, requestCode, response);
                Log.e("code-->", requestCode);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        Gson gson = new Gson();
                        Type type = new TypeToken<List<WingsModel>>() {
                        }.getType();
                        List<WingsModel> list = gson.fromJson(jsonObject.getJSONObject("wings").
                                getJSONArray("list").toString(), type);

                        ArrayList<WingsModel> arrModel = new ArrayList<>();
                        ArrayList<WingsModel> arrTmpModel = new ArrayList<>();
                        arrModel.addAll(list);
                        arrTmpModel.addAll(list);

                        getBlocksForWings(arrModel, arrTmpModel);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                if (!returnResult.isEmpty()) {
                    return;
                } else {
                    WingsAdapter wingsAdapter = new WingsAdapter(SocietyTabActivity.this, new ArrayList<>());
                    rvWing.setAdapter(wingsAdapter);
                }
            }
        });
    }

    private void getBlocksForWings(ArrayList<WingsModel> arrModel, ArrayList<WingsModel> arrTmpModel) {

        rvWing.setHasFixedSize(true);
        rvWing.setLayoutManager(new LinearLayoutManager(SocietyTabActivity.this));
        rvWing.setNestedScrollingEnabled(false);

        WingsAdapter wingsAdapter = new WingsAdapter(SocietyTabActivity.this, arrModel);
        rvWing.setAdapter(wingsAdapter);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                arrModel.clear();
                arrModel.addAll(findWingsByName(s.toString(), arrTmpModel));
                wingsAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        for (int i = 0; i < arrModel.size(); i++) {

            JsonObject jObj = new JsonObject();

            int finalI = i;
            new NetworkCall(SocietyTabActivity.this, Services.WINGS + arrModel.get(i).getId(),
                    jObj, true, NetworkCall.REQ_GET,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response, String requestCode) {
                    String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, requestCode, response);
                    Log.e("codeWing-->", returnResult);
                    if (requestCode.equals("200")) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            Gson gson = new Gson();
                            Type type = new TypeToken<List<WingsModel.FloorsModel>>() {
                            }.getType();
                            List<WingsModel.FloorsModel> list = gson.fromJson(jsonObject.getJSONObject("wing").
                                    getJSONArray("floors").toString(), type);
                            ArrayList<WingsModel.FloorsModel> arrFloors = new ArrayList<>();
                            arrFloors.addAll(list);

                            arrModel.get(finalI).setFloors(arrFloors);
                            arrTmpModel.get(finalI).setFloors(arrFloors);
                            wingsAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
        }
    }

    public ArrayList<WingsModel> findWingsByName(String name, ArrayList<WingsModel> listArray) {
        ArrayList<WingsModel> matches = new ArrayList<WingsModel>();
        // go through list of members and compare name with given name
        for (WingsModel list : listArray) {
            if (list.getName().toLowerCase().contains(name.toLowerCase())) {
                matches.add(list); // adds matching member to the return list
            }
        }
        return matches; // return the matches, which is empty when no member with the given name was found
    }

    private void setAssets() {
        AssetsAdapter assetsAdapter = new AssetsAdapter(SocietyTabActivity.this, new ArrayList<>());
        rvWing.setAdapter(assetsAdapter);
    }

    private void setRules() {
        JsonObject jObj = new JsonObject();

        new NetworkCall(SocietyTabActivity.this, Services.SOCIETY_RULES,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray rulesDataList = jsonObject.getJSONArray("list");

                        rulesModels = (ArrayList<RulesModel>)
                                new Gson().fromJson(String.valueOf(rulesDataList),
                                        new TypeToken<ArrayList<RulesModel>>() {
                                        }.getType());
                        RulesAdapter rulesAdapter = new RulesAdapter(SocietyTabActivity.this, rulesModels);
                        rvWing.setAdapter(rulesAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setEmergency() {
        JsonObject jObj = new JsonObject();

        new NetworkCall(SocietyTabActivity.this, Services.SOCIETY_EMERGENCY,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray emergencyDataList = jsonObject.getJSONArray("list");

                        emergencyModels = (ArrayList<EmergencyModel>)
                                new Gson().fromJson(String.valueOf(emergencyDataList),
                                        new TypeToken<ArrayList<EmergencyModel>>() {
                                        }.getType());
                        EmergencyAdapter emergencyAdapter = new EmergencyAdapter(SocietyTabActivity.this, emergencyModels);
                        rvWing.setAdapter(emergencyAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }

    private void setGates() {
        JsonObject jObj = new JsonObject();

        new NetworkCall(SocietyTabActivity.this, Services.SOCIETY_GATES,
                jObj, true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response, String requestCode) {
                String returnResult = Utils.handleAPIResponceCode(SocietyTabActivity.this, requestCode, response);
                Log.e("code-->", returnResult);
                if (requestCode.equals("200")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray rulesDataList = jsonObject.getJSONArray("list");

                        gatesModels = (ArrayList<GatesModel>)
                                new Gson().fromJson(String.valueOf(rulesDataList),
                                        new TypeToken<ArrayList<GatesModel>>() {
                                        }.getType());
                        GatesAdapter gatesAdapter = new GatesAdapter(SocietyTabActivity.this, gatesModels);
                        rvWing.setAdapter(gatesAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!returnResult.isEmpty()) {
                    return;
                }
            }
        });
    }
}
