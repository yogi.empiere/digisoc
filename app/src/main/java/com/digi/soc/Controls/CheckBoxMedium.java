package com.digi.soc.Controls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;

public class CheckBoxMedium extends AppCompatCheckBox {

    public CheckBoxMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CheckBoxMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CheckBoxMedium(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/avenirltstd_medium.ttf");
        setTypeface(tf);
    }

}