package com.digi.soc.Controls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class TextLightOblique extends AppCompatTextView {

    public TextLightOblique(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextLightOblique(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextLightOblique(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/avenirltstd_lightoblique.ttf");
        setTypeface(tf);
    }

}