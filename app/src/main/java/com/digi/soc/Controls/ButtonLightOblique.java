package com.digi.soc.Controls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

public class ButtonLightOblique extends AppCompatButton {

    public ButtonLightOblique(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ButtonLightOblique(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonLightOblique(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/avenirltstd_lightoblique.ttf");
        setTypeface(tf);
    }

}