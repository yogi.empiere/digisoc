package com.digi.soc.Controls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class TextLight extends AppCompatTextView {

    public TextLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextLight(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/avenirltstd_light.ttf");
        setTypeface(tf);
    }

}