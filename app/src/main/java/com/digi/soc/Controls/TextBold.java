package com.digi.soc.Controls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class TextBold extends AppCompatTextView {

    public TextBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/metropolis_bold.otf");
        setTypeface(tf);
    }

}