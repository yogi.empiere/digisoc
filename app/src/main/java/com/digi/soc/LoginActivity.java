package com.digi.soc;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.digi.soc.Controls.EditMedium;
import com.digi.soc.Helpers.MySharedPref;
import com.digi.soc.Helpers.Utils;
import com.digi.soc.WS.NetworkCall;
import com.digi.soc.WS.Services;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.tvSignup)
    TextView tvSignup;

    @BindView(R.id.llLogin)
    LinearLayout llLogin;

    @BindView(R.id.etEmail)
    EditMedium etEmail;

    @BindView(R.id.etPassword)
    EditMedium etPassword;

    boolean isShowText = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

        if (new MySharedPref(LoginActivity.this).isLogin()) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        bindWidget();

    }

    public void onClickPasswordShowHide(View view){
        if(isShowText){
            isShowText = false;
            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }else{
            isShowText = true;
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    private void bindWidget() {
        llLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin();

            }
        });

        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(i);
            }
        });
    }

    private void doLogin() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            etEmail.requestFocus();
            etEmail.setError(getResources().getString(R.string.invalid_email));
        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError(getResources().getString(R.string.invalid_password));
        } else {

            JsonObject jObj = new JsonObject();
            jObj.addProperty("contact_no", email);
            jObj.addProperty("password", password);

            new NetworkCall(this, Services.LOGIN,
                    jObj, true, NetworkCall.REQ_POST,
                    false, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response, String requestCode) {
                    String returnResult = Utils.handleAPIResponceCode(LoginActivity.this, requestCode, response);
                    Log.e("code-->", requestCode);
                    if (requestCode.equals("200")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String access_token = jsonObject.getString("access_token");
                            String token_type = jsonObject.getString("token_type");

                            JSONObject userObj = jsonObject.getJSONObject("user");
                            String userId = userObj.getString("id");

                            MySharedPref myShared = new MySharedPref(LoginActivity.this);
                            myShared.setString("Update", "Yes");
                            myShared.setString(MySharedPref.USER_ID, userId);
                            myShared.setString(MySharedPref.USER_AUTH, access_token);
                            myShared.setString(MySharedPref.USER_TOKEN_TYPE, token_type);

                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            ActivityCompat.finishAffinity(LoginActivity.this);
                            finish();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    if (!returnResult.isEmpty()) {
                        return;
                    }
                }
            });
        }
    }

    public void onResume(Bundle savedInstanceState){
        super.onResume();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
}
